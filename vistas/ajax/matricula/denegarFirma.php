<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';

$instancia = ControlMatricula::singleton_matricula();
$datos     = $instancia->denegarFirmaAcudienteControl($_POST['id'], $_POST['estado']);

if ($datos == true) {
    $mensaje = 'ok';
} else {
    $mensaje = 'error';
}

echo json_encode(['mensaje' => $mensaje]);