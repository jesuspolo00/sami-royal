<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'extra' . DS . 'ControlExtra.php';

$instancia = ControlExtra::singleton_extra();
$datos     = $instancia->datosExtraCurricularControl($_POST['id_extra']);
echo json_encode($datos);
