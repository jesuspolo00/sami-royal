<!DOCTYPE html>
<html lang="en">

<head>
    <title>S.A.M.I</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <link rel="manifest" href="%PUBLIC_URL%/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff">
    <link rel="icon" type="image/x-icon" href="<?=PUBLIC_PATH?>img/engranajes.gif">
    <meta name="theme-color" content="#ffffff">
    <!-- Custom fonts for this template-->
    <link href="<?=PUBLIC_PATH?>vendor/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="<?=PUBLIC_PATH?>css/bootstrapClockPicker.css">
    <link rel="stylesheet" type="text/css" href="<?=PUBLIC_PATH?>css/bootstrap-select.css">
    <!-- Custom styles for this template-->
    <link href="<?=PUBLIC_PATH?>css/sb-admin-2.css" rel="stylesheet">
    <link href="<?=PUBLIC_PATH?>css/main.css" rel="stylesheet">
    <link href="<?=PUBLIC_PATH?>css/fileinput.css" rel="stylesheet">
    <link href="<?=PUBLIC_PATH?>css/select.css" rel="stylesheet">
    <link href="<?=PUBLIC_PATH?>css/calendar.css" rel="stylesheet">
</head>

<body class="bg-image">
    <div class="loader"></div>
    <div id="ohsnap"></div>
    <div id="wrapper">