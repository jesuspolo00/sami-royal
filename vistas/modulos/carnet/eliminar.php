<div class="modal fade" id="eliminar_fotos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-primary" id="exampleModalLabel">Eliminacion de fotos</h5>
      </div>
      <div class="modal-body">
        <div class="row p-2">
          <div class="col-lg-12 form-group text-center">
            <h6>Estas seguro de eliminar todas las fotos de carnet ?</h6>
          </div>
          <div class="col-lg-12 form-group mt-2 text-right">
            <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
              <i class="fa fa-times"></i>
              &nbsp;
              No
            </button>
            <button class="btn btn-primary btn-sm eliminar_fotos_si" type="button">
              <i class="fa fa-check"></i>
              &nbsp;
              Si
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
