<?php
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once LIB_PATH . 'bardcode' . DS . 'vendor' . DS . 'autoload.php';

$instancia        = ControlUsuarios::singleton_usuarios();
$instancia_perfil = ControlPerfil::singleton_perfil();

$generator = new Picqer\Barcode\BarcodeGeneratorPNG();

if (isset($_GET['usuario'])) {

    $id_user = base64_decode($_GET['usuario']);

    $datos_usuario = $instancia_perfil->mostrarDatosPerfilControl($id_user);

    $fondo_carnet = 'fondo_carnet.png';

    $cargo = (empty($datos_usuario['nom_curso'])) ? $datos_usuario['nom_nivel'] : $datos_usuario['nom_curso'];

    $nombre    = mb_strtoupper($datos_usuario['apellido'] . ' ' . $datos_usuario['nombre']);
    $documento = $datos_usuario['documento'];
    $curso     = mb_strtoupper($cargo);

    $img = '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($documento, $generator::TYPE_CODE_128, 1.8, 35)) . '" class="codigo">';
?>    <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Carnet</title>
</head>
<body>
    <div class="bordes">
        <div class="contenido">
            <center>
                <img src="<?=PUBLIC_PATH?>img/<?=$fondo_carnet?>" class="fondo" alt="">
                <p class="nombre"><?=$nombre?></p>
                <p class="cargo"><?=$curso?></p>
                <p class="documento"><?=$documento?></p>
                <div class="circular--portrait">
                    <img src="<?=PUBLIC_PATH?>upload/<?=$datos_usuario['foto_carnet']?>" alt="">
                </div>
                <?=$img?>
            </center>
        </div>
    </div>
</body>
</html>
<style type="text/css">
    *{
        font-family: 'Helvetica', sans-serif;
    }
    body{
        outline: 0;
        padding: 0;
        margin: 0;
    }
    .codigo{
        margin-top: 30%;
    }
    .fondo{
        width: 100%;
        height: 331.8px;
        border:  1px solid rgb(255,255,255, 0);
    }
    .nombre{
        width: 80%;
        position: relative;
        margin-top: -55%;
        font-size: 0.70em;
        color: #302f59;
        font-weight: bold;
    }
    .documento{
        position: relative;
        margin-top: -1%;
        font-size: 0.65em;
        color: #302f59;
        font-weight: bold;
    }
    .cargo{
        position: relative;
        margin-top: -3%;
        font-size: 0.60em;
        color: #302f59;
        font-weight: bold;
    }

    .circular--portrait {
        position: relative;
        width: 108px;
        height: 115px;
        margin-right: -8px;
        border-radius: 50%;
        margin-top: -85.5%;
        z-index: -1000 !important;
        overflow: hidden;
    }

    .circular--portrait img {
        width: 100%;
        height: auto;
    }
</style>
<?php
include_once VISTA_PATH . 'script_and_final.php';
}
?>
<script>
    $(".loader").hide();
    window.print();
    window.addEventListener("afterprint", function(event) {
        window.close();
    });
</script>