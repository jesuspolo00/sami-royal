<?php
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once LIB_PATH . 'bardcode' . DS . 'vendor' . DS . 'autoload.php';

$instancia        = ControlUsuarios::singleton_usuarios();
$instancia_perfil = ControlPerfil::singleton_perfil();

$generator = new Picqer\Barcode\BarcodeGeneratorPNG();

if (isset($_GET['usuario'])) {
    $id_user = base64_decode($_GET['usuario']);

    $datos_usuario = $instancia_perfil->mostrarDatosPerfilControl($id_user);

    $cargo = (empty($datos_usuario['nom_curso'])) ? $datos_usuario['nom_nivel'] : $datos_usuario['nom_curso'];

    $nombre    = mb_strtoupper($datos_usuario['apellido'] . ' ' . $datos_usuario['nombre']);
    $documento = $datos_usuario['documento'];
    $curso     = mb_strtoupper($cargo);

    $img = '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($documento, $generator::TYPE_CODE_128, 1.6, 35)) . '">';
    ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Carnet</title>
    </head>
    <body>
        <div class="bordes">
            <div class="contenido">
                <center>
                    <img src="<?=PUBLIC_PATH?>img/fondo_carnet_atras.png" class="fondo" alt="">
                </center>
            </div>
        </div>
    </body>
    </html>
    <style>
        body{
            outline: 0;
            padding: 0;
            margin: 0;
        }
        .fondo{
            width: 100%;
            height: 331px;
            overflow: hidden;
        }
    </style>
    <?php
    include_once VISTA_PATH . 'script_and_final.php';
}
?>
<script>
    window.print();
    window.addEventListener("afterprint", function(event) {
        window.close();
    });
</script>