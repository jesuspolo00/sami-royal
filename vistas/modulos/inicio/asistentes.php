<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'zonas' . DS . 'ControlZonas.php';

$instancia = ControlZonas::singleton_zonas();

$datos_zona = $instancia->mostrarZonaControl();
$datos_area = $instancia->mostrarAreasZonasControl();

$permisos = $instancia_permiso->permisosUsuarioControl(36, $perfil_log);

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="col-lg-12">
	<div class="card shadow-sm mb-4">
		<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
			<h4 class="m-0 font-weight-bold text-primary">
				<!-- <a href="<?=BASE_URL?>inicio" class="text-decoration-none">
					<i class="fa fa-arrow-left text-primary"></i>
				</a>
				&nbsp; -->
				Areas
			</h4>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-lg-8 form-inline">
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<div class="input-group mb-3">
							<input type="text" class="form-control filtro" placeholder="Buscar">
							<div class="input-group-prepend">
								<span class="input-group-text rounded-right" id="basic-addon1">
									<i class="fa fa-search"></i>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="table-responsive mt-2">
				<table class="table table-hover border table-sm" width="100%" cellspacing="0">
					<thead>
						<tr class="text-center font-weight-bold">
							<th scope="col">#</th>
							<th scope="col">Area</th>
							<th scope="col">Zona</th>
						</tr>
					</thead>
					<tbody class="buscar">
						<?php
						foreach ($datos_zona as $zona) {
							$id_zona      = $zona['id'];
							$nombre       = $zona['nombre'];
							$activo       = $zona['activo'];
							$nom_area     = $zona['nom_area'];
							$id_area_zona = $zona['id_area'];
							$estado       = $zona['estado'];

							$ver_reporte   = '';
							$ver_mant      = '';
							$ver_editar    = '';
							$ver_inactivar = '';
							$span          = '';

							$ver = ($activo == 0 && $perfil_log != 1) ? 'd-none' : '';

							if ($activo == 1) {
								$icon  = '<i class="fa fa-times"></i>';
								$class = 'btn-danger inactivar_zona';
								$title = "Inactivar";
							} else {
								$icon  = '<i class="fa fa-check"></i>';
								$class = 'btn-success activar_zona';
								$title = "Activar";
							}

							if ($estado == 2) {
								$ver_reporte   = 'd-none';
								$ver_mant      = 'd-none';
								$ver_editar    = 'd-none';
								$ver_inactivar = 'd-none';
								$span          = '<span class="badge badge-success">Reportado</span>';
							}

							if ($estado == 6) {
								$ver_reporte   = 'd-none';
								$ver_mant      = 'd-none';
								$ver_editar    = 'd-none';
								$ver_inactivar = 'd-none';
								$span          = '<span class="badge badge-warning">Mantenimiento</span>';
							}

							if ($nom_nivel == $nom_area) {
								?>
								<tr class="text-center text-uppercase <?=$ver?>">
									<td><?=$id_zona?></td>
									<td>
										<a href="<?=BASE_URL?>historial/zona?id=<?=base64_encode($id_zona)?>"><?=$nombre?></a>
									</td>
									<td><?=$nom_area?></td>
									<td>
										<?=$span?>
									</td>
									<td>
										<div class="btn-group btn-group-sm" role="group">
											<button class="btn btn-success btn-sm <?=$ver_reporte?>" data-tooltip="tooltip" title="Reportar zona" data-placement="bottom" data-toggle="modal" data-target="#dano<?=$id_zona?>">
												<i class="fas fa-brush"></i>
											</button>
										<!-- <button class="btn btn-warning btn-sm <?=$ver_mant?>" data-tooltip="tooltip" data-placement="bottom" title="Mantenimiento" data-toggle="modal" data-target="#mant<?=$id_zona?>">
											<i class="fas fa-wrench"></i>
										</button> -->
									</div>
								</td>
							</tr>

							<!-- Modal -->
							<div class="modal fade" id="dano<?=$id_zona?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Reportar zona</h5>
										</div>
										<form method="POST">
											<input type="hidden" name="id_log" value="<?=$id_log?>">
											<input type="hidden" name="id_zona" value="<?=$id_zona?>">
											<input type="hidden" value="2" name="estado">
											<input type="hidden" name="fecha_mant" value="">
											<input type="hidden" name="inicio" value="1">
											<div class="modal-body">
												<div class="row p-2">
													<div class="col-lg-6 form-group">
														<label class="font-weight-bold">Zona <span class="text-danger">*</span></label>
														<input type="text" class="form-control" disabled value="<?=$nombre?>">
													</div>
													<div class="col-lg-6 form-group">
														<label class="font-weight-bold">Area <span class="text-danger">*</span></label>
														<input type="text" class="form-control" disabled value="<?=$nom_area?>">
													</div>
													<div class="col-lg-12 form-group">
														<label class="font-weight-bold">Observacion</label>
														<textarea class="form-control" rows="5" name="observacion"></textarea>
													</div>
												</div>
											</div>
											<div class="modal-footer border-0">
												<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
													<i class="fa fa-times"></i>
													&nbsp;
													Cerrar
												</button>
												<button type="submit" class="btn btn-success btn-sm">
													<i class="fa fa-save"></i>
													&nbsp;
													Guardar
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>


							<!-- Modal -->
							<!-- <div class="modal fade" id="mant<?=$id_zona?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Mantenimiento zona</h5>
										</div>
										<form method="POST">
											<input type="hidden" value="6" name="estado">
											<input type="hidden" name="id_log" value="<?=$id_log?>">
											<input type="hidden" name="id_zona" value="<?=$id_zona?>">
											<div class="modal-body">
												<div class="row p-2">
													<div class="col-lg-6 form-group">
														<label class="font-weight-bold">Zona <span class="text-danger">*</span></label>
														<input type="text" class="form-control" disabled value="<?=$nombre?>">
													</div>
													<div class="col-lg-6 form-group">
														<label class="font-weight-bold">Area <span class="text-danger">*</span></label>
														<input type="text" class="form-control" disabled value="<?=$nom_area?>">
													</div>
													<div class="col-lg-6 form-group">
														<label class="font-weight-bold">Fecha a programar <span class="text-danger">*</span></label>
														<input type="date" class="form-control" name="fecha_mant" value="<?=date('Y-m-d')?>">
													</div>
													<div class="col-lg-12 form-group">
														<label class="font-weight-bold">Observacion</label>
														<textarea class="form-control" rows="5" name="observacion"></textarea>
													</div>
												</div>
											</div>
											<div class="modal-footer border-0">
												<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
													<i class="fa fa-times"></i>
													&nbsp;
													Cerrar
												</button>
												<button type="submit" class="btn btn-success btn-sm">
													<i class="fa fa-save"></i>
													&nbsp;
													Guardar
												</button>
											</div>
										</form>
									</div>
								</div>
							</div> -->

							<?php
						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['nombre'])) {
	$instancia->guardarZonasControl();
}

if (isset($_POST['nom_area'])) {
	$instancia->agregarAreaControl();
}

if (isset($_POST['area_edit'])) {
	$instancia->editarZonaControl();
}

if (isset($_POST['estado'])) {
	$instancia->reportarZonaControl();
}
?>