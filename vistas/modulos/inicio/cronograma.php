<div class="col-lg-12">
	<div class="card shadow-sm mb-4">
		<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
			<h4 class="m-0 font-weight-bold text-primary">
				Cronograma
			</h4>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-lg-12 form-group">
					<div id="calendar">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
