<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'PhpSpreadsheet' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'extra' . DS . 'ControlExtra.php';

$instancia = ControlExtra::singleton_extra();

$datos_extra = $instancia->mostrarDatosRegistrosExtraControl();

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();

$spreadsheet->getProperties()
->setTitle('Reporte de inscripcion extracurricular')
->setDescription('Este documento fue generado por el sistema');

$sheet = $spreadsheet->setActiveSheetIndex(0);

$estilos_cabecera = [
    'font'      => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$estilos_datos = [
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$sheet->getStyle('A1:E1')->applyFromArray($estilos_cabecera);
$sheet->getStyle('A:E')->applyFromArray($estilos_datos);

foreach (range('A', 'E') as $column) {
    $sheet->getColumnDimension($column)->setAutoSize(true);
}

$sheet->setCellValue('A1', 'TIPO')
->setCellValue('B1', 'ACTIVIDAD')
->setCellValue('C1', 'ESTUDIANTE')
->setCellValue('D1', 'NIVEL')
->setCellValue('E1', 'CURSO');

$cont = 2;

foreach ($datos_extra as $extra) {
    $id_extra    = $extra['id'];
    $tipo        = ($extra['tipo'] == 1) ? 'Extracurricular' : 'Club';
    $actividad   = $extra['actividad'];
    $nom_usuario = $extra['nom_usuario'];
    $nom_nivel   = $extra['nom_nivel'];
    $nom_curso   = $extra['nom_curso'];

    $sheet->setCellValue('A' . $cont, $tipo)
    ->setCellValue('B' . $cont, $actividad)
    ->setCellValue('C' . $cont, $nom_usuario)
    ->setCellValue('D' . $cont, $nom_nivel)
    ->setCellValue('E' . $cont, $nom_curso);

    $cont++;
}

$spreadsheet->getActiveSheet()->setTitle('Hoja 1');

$fileName = "Reporte_inscripcion_" . date('Y-m-d') . ".xlsx";
$writer   = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
$writer->save('php://output');
