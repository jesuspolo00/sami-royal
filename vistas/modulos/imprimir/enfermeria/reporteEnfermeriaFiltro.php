<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'PhpSpreadsheet' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'enfermeria' . DS . 'ControlEnfermeria.php';

$instancia = ControlEnfermeria::singleton_enfermeria();

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if (isset($_GET['buscar'])) {
    $datos          = array('buscar' => $_GET['buscar'], 'fecha_hasta' => $_GET['fecha_hasta'], 'fecha_desde' => $_GET['fecha_desde']);
    $datos_atencion = $instancia->buscarAtencionMedicaControl($datos);
}

$spreadsheet = new Spreadsheet();

$spreadsheet->getProperties()
->setTitle('Reporte de atenciones medicas')
->setDescription('Este documento fue generado por el sistema');

$sheet = $spreadsheet->setActiveSheetIndex(0);

$estilos_cabecera = [
    'font'      => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$estilos_datos = [
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$sheet->getStyle('A1:I1')->applyFromArray($estilos_cabecera);
$sheet->getStyle('A:I')->applyFromArray($estilos_datos);

foreach (range('A', 'I') as $column) {
    $sheet->getColumnDimension($column)->setAutoSize(true);
}

$sheet->setCellValue('A1', 'No. ATENCION MEDICA')
->setCellValue('B1', 'DOCUMENTO')
->setCellValue('C1', 'NOMBRE')
->setCellValue('D1', 'NIVEL')
->setCellValue('E1', 'CURSO')
->setCellValue('F1', 'MOTIVO')
->setCellValue('G1', 'TRATAMIENTO')
->setCellValue('H1', 'FECHA DE ATENCION')
->setCellValue('I1', 'ESTADO DE NOTIFICACION');

$cont = 2;

foreach ($datos_atencion as $atencion) {
    $id_atencion    = $atencion['id'];
    $nom_user       = $atencion['nom_user'];
    $documento      = $atencion['documento'];
    $curso          = $atencion['nom_curso'];
    $nivel          = $atencion['nom_nivel'];
    $motivo         = $atencion['motivo_cons'];
    $tratamiento    = $atencion['tratamiento'];
    $fecha_atencion = date('Y-m-d', strtotime($atencion['fechareg']));

    if ($atencion['envio'] == 1) {
        $span_envio = 'Correo Enviado';
    }

    if ($atencion['envio'] == 2) {
        $span_envio = 'Envio de correo programado pendiente';
    }

    if ($atencion['envio'] == 2 && $atencion['enviado'] == 1) {
        $span_envio = 'Correo Programado Enviado';
    }

    $sheet->setCellValue('A' . $cont, $id_atencion)
    ->setCellValue('B' . $cont, $documento)
    ->setCellValue('C' . $cont, $nom_user)
    ->setCellValue('D' . $cont, $nivel)
    ->setCellValue('E' . $cont, $curso)
    ->setCellValue('F' . $cont, $motivo)
    ->setCellValue('G' . $cont, $tratamiento)
    ->setCellValue('H' . $cont, $fecha_atencion)
    ->setCellValue('I' . $cont, $span_envio);

    $cont++;

}

$spreadsheet->getActiveSheet()->setTitle('Hoja 1');

$fileName = "Reporte_atencion_medica_" . date('Y-m-d') . ".xlsx";
$writer   = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
$writer->save('php://output');
