<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'PhpSpreadsheet' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'zonas' . DS . 'ControlZonas.php';

$instancia = ControlZonas::singleton_zonas();

$datos_reporte = $instancia->mostrarTodosReportesZonasControl();

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();

$spreadsheet->getProperties()
->setTitle('Reportes de zonas')
->setDescription('Este documento fue generado por el sistema');

$sheet = $spreadsheet->setActiveSheetIndex(0);

$estilos_cabecera = [
    'font'      => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$estilos_datos = [
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
];

$sheet->getStyle('A1:G1')->applyFromArray($estilos_cabecera);
$sheet->getStyle('A:G')->applyFromArray($estilos_datos);

foreach (range('A', 'G') as $column) {
    $sheet->getColumnDimension($column)->setAutoSize(true);
}

$sheet->setCellValue('A1', '#')
->setCellValue('B1', 'AREA')
->setCellValue('C1', 'ZONA')
->setCellValue('D1', 'ESTADO')
->setCellValue('E1', 'FECHA PROGRAMACION')
->setCellValue('F1', 'FECHA RESPUESTA/REPROGRAMACION')
->setCellValue('G1', 'OBSERVACION');

$cont = 2;

foreach ($datos_reporte as $reporte) {

    $id_reporte                     = $reporte['id'];
    $area                           = $reporte['area'];
    $zona                           = $reporte['zona'];
    $estado                         = $reporte['estado'];
    $fecha_programacion             = $reporte['fecha_programacion'];
    $fecha_respuesta_reprogramacion = $reporte['fecha_respuesta_reprogramacion'];
    $observacion                    = $reporte['observacion'];

    $sheet->setCellValue('A' . $cont, $id_reporte)
    ->setCellValue('B' . $cont, $area)
    ->setCellValue('C' . $cont, $zona)
    ->setCellValue('D' . $cont, $estado)
    ->setCellValue('E' . $cont, $fecha_programacion)
    ->setCellValue('F' . $cont, $fecha_respuesta_reprogramacion)
    ->setCellValue('G' . $cont, $observacion);

    $cont++;
}

$spreadsheet->getActiveSheet()->setTitle('Hoja 1');

$fileName = "Reporte_zonas_" . date('Y-m-d') . ".xlsx";
$writer   = new Xlsx($spreadsheet);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="' . urlencode($fileName) . '"');
$writer->save('php://output');
