<?php
date_default_timezone_set('America/Bogota');
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once LIB_PATH . 'bardcode' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'proveedor' . DS . 'ControlProveedor.php';

$instancia = ControlProveedor::singleton_proveedor();

if (isset($_GET['proveedor'])) {

    $id_proveedor = base64_decode($_GET['proveedor']);

    $datos_proveedor     = $instancia->mostrarDatosProveedorIdControl($id_proveedor);
    $contactos_proveedor = $instancia->mostrarContactosProveedorControl($id_proveedor);
    $banco_proveedor     = $instancia->mostrarBancoProveedorControl($id_proveedor);

    class MYPDF extends TCPDF
    {

        public function setData($logo)
        {
            $this->logo = $logo;
        }

        public function Header()
        {
        }

        public function Footer()
        {
            $this->SetY(-15);
            $this->SetFillColor(127);
            $this->SetTextColor(127);
            $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
            $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
        }
    }

// create a PDF object
    $pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document (meta) information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->setData('encabezado.png');
    $pdf->SetAuthor('Jesus Polo');
    $pdf->SetTitle('Hoja de registro');
    $pdf->SetSubject('Hoja de registro');
    $pdf->SetKeywords('Hoja de registro');
    $pdf->AddPage();

    $pdf->Ln(-1);
    $pdf->Cell(2);
    $pdf->Image(PUBLIC_PATH . 'img/07fef3197d045247702d01ede3ea1c7f.png', '', '', 40, 16, '', '', 'T', false, 90, '', false, false, 1, false, false, false);
    $pdf->Ln(-3);
    $pdf->Cell(45);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->Cell(142.5, 5, 'COLEGIO REAL ROYAL SCHOOL', 'B', 0, 'C');
    $pdf->Ln(6);
    $pdf->Cell(45);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
    $pdf->Cell(142.5, 5, 'HOJA DE REGISTRO', 'B', 0, 'C');
    $pdf->Ln(6);
    $pdf->Cell(45);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->Cell(47.5, 5, 'Codigo:', 'B', 0, 'C');
    $pdf->Cell(47.5, 5, 'Version: 1', 'B', 0, 'C');
    $pdf->Cell(47.5, 5, 'Fecha Version: 2021-11-10', 'B', 0, 'C');

    $pdf->Ln(20);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
    $pdf->Cell(180, 5, 'Informacion del proveedor externo', 1, 0, 'C');

    $ln = 7;

    $pdf->Ln($ln + 3);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    //$pdf->Cell(90, 5, 'Nombre: ' . $datos_proveedor['nombre'], 0, 0, 'L');
    $pdf->MultiCell(90, 5, 'Nombre: ' . $datos_proveedor['nombre'], 0, 'L', 0, 0, '', '', true);
    $pdf->Cell(90, 5, 'Fecha de actualizacion: ' . $datos_proveedor['fechareg'], 0, 0, 'L');

    $pdf->Ln($ln);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->Cell(90, 5, 'Identificacion: ' . $datos_proveedor['identificacion'], 0, 0, 'L');
    $pdf->Cell(90, 5, 'Numero de identificacion: ' . $datos_proveedor['num_identificacion'], 0, 0, 'L');

    $pdf->Ln($ln);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->Cell(90, 5, 'Direccion: ' . $datos_proveedor['direccion'], 0, 0, 'L');
    $pdf->Cell(90, 5, 'Ciudad: ' . $datos_proveedor['ciudad'], 0, 0, 'L');

    $pdf->Ln($ln);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->Cell(90, 5, 'Departamento: ' . $datos_proveedor['departamento'], 0, 0, 'L');
    $pdf->Cell(90, 5, 'Pais: ' . $datos_proveedor['pais'], 0, 0, 'L');

    $pdf->Ln($ln);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->Cell(90, 5, 'Telefono: ' . $datos_proveedor['telefono'], 0, 0, 'L');
    $pdf->Cell(90, 5, 'Correo: ' . $datos_proveedor['correo'], 0, 0, 'L');

    $pdf->Ln($ln);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->Cell(90, 5, 'Fecha ingreso: ' . $datos_proveedor['fecha_ingreso'], 0, 0, 'L');

    /*---------------------------------------------------------*/
    $pdf->Ln(10);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
    $pdf->Cell(180, 5, 'Informacion del producto', 1, 0, 'C');

    $ln = 7;

    $pdf->Ln($ln + 3);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->Cell(90, 5, 'Tipo: ' . $datos_proveedor['tipo'], 0, 0, 'L');
    $pdf->Cell(90, 5, 'Tiempo de entrega (Dias): ' . $datos_proveedor['tiempo_entrega'], 0, 0, 'L');

    $pdf->Ln($ln);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->Cell(90, 5, 'Garantia (Dias/Meses): ' . $datos_proveedor['garantia'], 0, 0, 'L');
    $pdf->Cell(90, 5, 'Plazo de pago (Dias/Meses): ' . $datos_proveedor['plazo_pago'], 0, 0, 'L');

    $pdf->Ln($ln);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    //$pdf->Cell(90, 5, 'Detalle del producto: ' . $datos_proveedor['detalle_producto'], 0, 0, 'L');
    $pdf->MultiCell(180, 5, 'Detalle del producto: ' . $datos_proveedor['detalle_producto'], 0, 'L', 0, 0, '', '', true);

    /*--------------------------------------------------------*/

    /*---------------------------------------------------------*/
    $pdf->Ln(12);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
    $pdf->Cell(180, 5, 'Informacion del representante legal', 1, 0, 'C');

    $ln = 7;

    $pdf->Ln($ln + 3);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    //$pdf->Cell(90, 5, 'Nombre completo: ' . $datos_proveedor['nom_representante'], 0, 0, 'L');
    $pdf->MultiCell(90, 5, 'Nombre completo: ' . $datos_proveedor['nom_representante'], 0, 'L', 0, 0, '', '', true);
    $pdf->Cell(90, 5, 'Identificacion: ' . $datos_proveedor['identificacion_representante'], 0, 0, 'L');

    $pdf->Ln($ln);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->Cell(90, 5, 'Correo electronico: ' . $datos_proveedor['correo_representante'], 0, 0, 'L');
    $pdf->Cell(90, 5, 'Telefono: ' . $datos_proveedor['telefono_representante'], 0, 0, 'L');

    /*---------------------------------------------------*/

    /*---------------------------------------------------------*/
    $pdf->Ln(10);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
    $pdf->Cell(180, 5, 'Informacion tributaria', 1, 0, 'C');

    $ln = 7;

    $pdf->Ln($ln + 3);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->Cell(90, 5, 'Regimen: ' . $datos_proveedor['regimen_proveedor'], 0, 0, 'L');
    $pdf->Cell(90, 5, 'Gran contribuyente: ' . $datos_proveedor['contribuyente_proveedor'], 0, 0, 'L');

    $pdf->Ln($ln);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->Cell(90, 5, 'Autoretenedor: ' . $datos_proveedor['autoretenedor_proveedor'], 0, 0, 'L');
    $pdf->Cell(90, 5, 'Responsable industria y comercio: ' . $datos_proveedor['comercio_proveedor'], 0, 0, 'L');

    $pdf->Ln($ln);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    //$pdf->MultiCell(90, 5, 'Actividad economica: ' . $datos_proveedor['actividad_proveedor'], 0, 0, 0, 'L');
    $pdf->MultiCell(90, 5, 'Actividad economica: ' . $datos_proveedor['actividad_proveedor'], 0, 'L', 0, 0, '', '', true);
    $pdf->Cell(90, 5, 'Tarifa: ' . $datos_proveedor['tarifa_proveedor'], 0, 0, 'L');
    /*---------------------------------------------------*/

    /*---------------------------------------------------------*/
    $pdf->Ln(12);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
    $pdf->Cell(180, 5, 'Referencia comercial', 1, 0, 'C');

    $ln = 7;

    $pdf->Ln($ln + 3);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    //$pdf->MultiCell(90, 5, '', 0, 0, 0, 'L');
    $pdf->MultiCell(90, 5, 'Nombre o razon social: ' . $datos_proveedor['comercial_nombre'], 0, 'L', 0, 0, '', '', true);
    $pdf->Cell(90, 5, 'Identificacion: ' . $datos_proveedor['identificacion_comercial'], 0, 0, 'L');

    $pdf->Ln($ln + 2);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->Cell(90, 5, 'Correo electronico: ' . $datos_proveedor['correo_comercial'], 0, 0, 'L');
    $pdf->Cell(90, 5, 'Telefono: ' . $datos_proveedor['telefono_comercial'], 0, 0, 'L');

    $pdf->Ln($ln);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->Cell(90, 5, 'Direccion: ' . $datos_proveedor['direccion_comercial'], 0, 0, 'L');
    $pdf->Cell(90, 5, 'Ciudad: ' . $datos_proveedor['ciudad_comercial'], 0, 0, 'L');

    $pdf->Ln($ln);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 9);
    $pdf->Cell(90, 5, 'Departamento: ' . $datos_proveedor['departamento_comercial'], 0, 0, 'L');
    /*---------------------------------------------------*/

    /*-------------------Hardware----------------------*/
    $pdf->Ln($ln + 1);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
    $pdf->Cell(180, 5, 'Informacion de contactos', 1, 0, 'C');

    $tabla_hard = '
    <table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
    <tr style="text-align:center; font-weight:bold;">
    <th>Nombre</th>
    <th>Telefono</th>
    <th>Correo</th>
    <th>Cargo</th>
    </tr>
    ';

    foreach ($contactos_proveedor as $contacto) {
        $id_contacto = $contacto['id'];
        $nombre      = $contacto['nombre_contacto'];
        $telefono    = $contacto['telefono_contacto'];
        $correo      = $contacto['correo_contacto'];
        $cargo       = $contacto['cargo_contacto'];
        $activo      = $contacto['activo'];

        $ver = ($activo == 1) ? '' : 'display:none;';

        $tabla_hard .= '
        <tr style="text-align:center;' . $ver . '">
        <td>' . $nombre . '</td>
        <td>' . $telefono . '</td>
        <td>' . $correo . '</td>
        <td>' . $cargo . '</td>
        </tr>
        ';
    }

    $tabla_hard .= '
    </table>
    ';

    $pdf->Ln($ln);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($tabla_hard, true, false, true, false, '');
    /*--------------------------------------------------------*/

    /*-------------------Software----------------------*/
    $pdf->Ln(-2);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 9);
    $pdf->Cell(180, 5, 'Informacion basica para efectuar pagos', 1, 0, 'C');

    $tabla_soft = '
    <table border="1" cellpadding="3" style="font-size:8.5px; width:98%;">
    <tr style="text-align:center; font-weight:bold;">
    <th>Nombre</th>
    <th>Numero de cuenta</th>
    <th>Tipo de cuenta</th>
    </tr>
    ';

    foreach ($banco_proveedor as $banco) {
        $id_banco = $banco['id'];
        $nombre   = $banco['nom_banco'];
        $numero   = $banco['num_banco'];
        $tipo     = $banco['tipo_cuenta'];
        $activo   = $banco['activo'];

        $ver = ($activo == 1) ? '' : 'display:none;';

        $tabla_soft .= '
        <tr style="text-align:center;' . $ver . '">
        <td>' . $nombre . '</td>
        <td>' . $numero . '</td>
        <td>' . $tipo . '</td>
        </tr>
        ';
    }

    $tabla_soft .= '
    </table>
    ';

    $pdf->Ln($ln);
    $pdf->Cell(6);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($tabla_soft, true, false, true, false, '');
    /*-----------------------------------------------------*/

    $style = array(
        'position'     => 'C',
        'align'        => 'C',
        'stretch'      => false,
        'fitwidth'     => true,
        'cellfitalign' => '',
        'border'       => false,
        'hpadding'     => 'auto',
        'vpadding'     => 'auto',
        'fgcolor'      => array(0, 0, 0),
        'bgcolor'      => false, //array(255,255,255),
        'text'         => true,
        'font'         => 'helvetica',
        'fontsize'     => 8,
        'stretchtext'  => 4,
    );

// CODE 39
    /*$pdf->Ln($ln + 8);
    $pdf->write1DBarcode($datos_articulos['codigo'], 'C39', '', '', '', 18, 0.4, $style, 'N', 'C');*/
    $pdf->Output('hoja_vida_' . date('Y-m-d-H-i-s') . '.pdf', 'I');
}
