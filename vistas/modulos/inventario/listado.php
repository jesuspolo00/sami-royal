<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';

$instancia = ControlInventario::singleton_inventario();

if (isset($_POST['buscar'])) {
    $buscar          = $_POST['buscar'];
    $datos_articulos = $instancia->mostrarArticulosBuscarUsuarioControl($id_log, $_POST['buscar']);
} else {
    $buscar          =  '';
    $datos_articulos = $instancia->mostrarArticulosUsuarioControl($id_log);
}

$cantidades = $instancia->mostrarCantidadesControl($id_log);

$ver_confirmar = ($cantidades['cantidad_confirmada'] == $cantidades['cantidad']) ? 'd-none' : '';

$permisos = $instancia_permiso->permisosUsuarioControl(16, $perfil_log);

if (!$permisos) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        Inventario
                    </h4>
                    <a class="btn btn-success btn-sm <?=$ver_confirmar?>" href="<?=BASE_URL?>inventario/confirmar">
                        <i class="fas fa-check-double"></i>
                        &nbsp;
                        Confirmar inventario
                    </a>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <div class="row">
                            <div class="col-lg-8"></div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control filtro" placeholder="Buscar articulo..." aria-describedby="basic-addon2" name="buscar"data-tooltip="tooltip" data-trigger="focus" data-placement="top" title="Presione ENTER para buscar" value="<?=$buscar?>">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary btn-sm" type="submit">
                                                <i class="fa fa-search"></i>
                                                &nbsp;
                                                Buscar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive mt-4">
                        <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center text-uppercase bg-light">
                                    <th colspan="9">Listado inventario</th>
                                </tr>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">AREA</th>
                                    <th scope="col">DESCRIPCION</th>
                                    <th scope="col">MARCA</th>
                                    <th scope="col">CANTIDAD</th>
                                    <th scope="col">ESTADO</th>
                                    <th scope="col">OBSERVACION</th>
                                </tr>
                            </thead>
                            <tbody class="buscar text-uppercase">
                                <?php
                                foreach ($datos_articulos as $articulo) {
                                    $id_inventario = $articulo['id'];
                                    $descripcion   = $articulo['descripcion'];
                                    $marca         = $articulo['marca'];
                                    $modelo        = $articulo['modelo'];
                                    $usuario       = $articulo['usuario'];
                                    $area          = $articulo['area'];
                                    $codigo        = $articulo['codigo'];
                                    $estado        = $articulo['nombre_estado'];
                                    $id_estado     = $articulo['estado'];
                                    $observacion   = $articulo['observacion'];
                                    $id_area       = $articulo['id_area'];
                                    $id_user       = $articulo['id_user'];
                                    $cantidad      = $articulo['cantidad'];

                                    $visible_rep  = '';
                                    $visible_trab = '';
                                    $span         = '';
                                    $bg_color     = '';
                                    $ver          = '';

                                    if ($id_estado == 2) {
                                        $visible_rep  = 'd-none';
                                        $ver          = '';
                                        $span         = '<span class="badge badge-success">Reportado</span>';
                                        $visible_trab = '';
                                        $span_casa    = '';
                                        $bg_color     = '';
                                    } elseif ($id_estado == 3) {
                                        $visible_rep  = '';
                                        $ver          = '';
                                        $span         = '';
                                        $visible_trab = '';
                                        $span_casa    = '';
                                        $bg_color     = '';
                                    } elseif ($id_estado == 6) {
                                        $visible_rep  = 'd-none';
                                        $ver          = '';
                                        $span         = '<span class="badge badge-warning">Mantenimiento preventivo</span>';
                                        $visible_trab = '';
                                        $span_casa    = '';
                                        $bg_color     = '';
                                    } elseif ($id_estado == 1) {
                                        $visible_rep  = '';
                                        $ver          = '';
                                        $span         = '';
                                        $visible_trab = '';
                                        $span_casa    = '';
                                        $bg_color     = '';
                                    } elseif ($id_estado == 5) {
                                        $visible_trab = 'd-none';
                                        $visible_rep  = 'd-none';
                                        $ver          = 'd-none';
                                        $span         = '<span class="badge badge-danger">Descontinuado</span>';
                                    } else if ($id_estado == 8) {
                                        $visible_rep  = '';
                                        $ver          = 'd-none';
                                        $visible_trab = '';
                                        $span         = '';
                                        $bg_color     = '';
                                    }

                                    if ($articulo['confirmado'] == 0) {
                                        $visible_rep     = 'd-none';
                                        $visible_trab    = 'd-none';
                                        $bg_color        = '';
                                        $span_confirmado = '';
                                        $span            = '<span class="badge badge-danger">No Confirmado</span>';
                                    } else if ($articulo['confirmado'] == 1) {
                                        $span_confirmado = '<span class="badge badge-success">Confirmado</span>';
                                    } else if ($articulo['confirmado'] == 2) {
                                        $visible_rep     = 'd-none';
                                        $visible_trab    = 'd-none';
                                        $span_confirmado = '<span class="badge badge-warning">Pendiente de revision</span>';
                                    }

                                    ?>
                                    <tr class="text-center <?=$bg_color . ' ' . $ver?>">
                                        <td><?=$area?></td>
                                        <td><?=$descripcion?></td>
                                        <td><?=$marca?></td>
                                        <td><?=$cantidad?></td>
                                        <td><?=$estado?></td>
                                        <td><?=$observacion?></td>
                                        <!-- <td><?=$span_confirmado?></td> -->
                                        <td><?=$span?></td>
                                        <td>
                                            <div class="btn-group btn-group-sm" role="group">
                                                <button class="btn btn-success btn-sm <?=$visible_rep?>" data-tooltip="tooltip" data-placement="bottom" title="Reportar" data-toggle="modal" data-target="#rep_inv<?=$id_inventario?>">
                                                    <i class="fas fa-clipboard-check"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>

                                    <!-- Reportar inventario -->
                                    <div class="modal fade" id="rep_inv<?=$id_inventario?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Reportar articulo</h5>
                                                </div>
                                                <form method="POST">
                                                    <div class="modal-body border-0">
                                                        <div class="row p-2">
                                                            <input type="hidden" value="<?=$id_log?>" name="id_log_rep">
                                                            <input type="hidden" value="<?=$id_user?>" name="id_user_rep">
                                                            <input type="hidden" value="<?=$id_area?>" name="id_area_rep">
                                                            <input type="hidden" value="<?=$descripcion?>" name="nom_inventario_rep">
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Area</label>
                                                                <input type="text" class="form-control" disabled maxlength="50" minlength="1" value="<?=$area?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Descripcion</label>
                                                                <input type="text" class="form-control" disabled maxlength="50" minlength="1" value="<?=$descripcion?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Marca</label>
                                                                <input type="text" class="form-control" disabled maxlength="50" minlength="1" value="<?=$marca?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Cantidad</label>
                                                                <input type="text" class="form-control" disabled value="<?=$cantidad?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Responsable</label>
                                                                <input type="text" class="form-control" disabled maxlength="50" minlength="1" value="<?=$usuario?>">
                                                            </div>
                                                            <div class="col-lg-6 form-group">
                                                                <label class="font-weight-bold">Cantidad a Reportar</label>
                                                                <input type="number" class="form-control numeros" name="cantidad" required max="<?=$cantidad?>">
                                                            </div>
                                                            <div class="form-group col-lg-12">
                                                                <label class="font-weight-bold">Observacion</label>
                                                                <textarea name="observacion" class="form-control" maxlength="1000" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer border-0">
                                                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                                                            <i class="fa fa-times"></i>
                                                            &nbsp;
                                                            Cerrar
                                                        </button>
                                                        <button type="submit" class="btn btn-primary btn-sm">
                                                            <i class="fas fa-clipboard-check"></i>
                                                            &nbsp;
                                                            Reportar
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!------------------------------------------------------->


                                    <!-- Reportar inventario -->
                                    <div class="modal fade" id="trab_home<?=$id_inventario?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Trabajo en casa</h5>
                                                </div>
                                                <form method="POST">
                                                    <div class="modal-body border-0">
                                                        <div class="row p-2">
                                                            <input type="hidden" value="<?=$id_super_empresa?>" name="super_empresa_trab_home">
                                                            <input type="hidden" value="<?=$id_inventario?>" name="id_inventario_trab_home">
                                                            <input type="hidden" value="<?=$id_log?>" name="id_log_trab_home">
                                                            <input type="hidden" value="<?=$id_user?>" name="id_user_trab_home">
                                                            <input type="hidden" value="<?=$id_area?>" name="id_area_trab_home">
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Area</label>
                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?=$area?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Descripcion</label>
                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?=$descripcion?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Marca</label>
                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?=$marca?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Modelo</label>
                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?=$modelo?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Responsable</label>
                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?=$usuario?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">No. Serie</label>
                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?=$codigo?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer border-0">
                                                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                                                            <i class="fa fa-times"></i>
                                                            &nbsp;
                                                            Cerrar
                                                        </button>
                                                        <button type="submit" class="btn btn-success btn-sm">
                                                            <i class="fas fa-briefcase"></i>
                                                            &nbsp;
                                                            Aceptar
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!------------------------------------------------------->

                                    <?php
//}

                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="table-responsive mt-4">
                        <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center text-uppercase bg-light">
                                    <th colspan="9">Trabajo en casa</th>
                                </tr>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">AREA</th>
                                    <th scope="col">DESCRIPCION</th>
                                    <th scope="col">MARCA</th>
                                    <th scope="col">CODIGO</th>
                                    <!-- <th scope="col">ESTADO</th> -->
                                    <th scope="col">OBSERVACION</th>
                                </tr>
                            </thead>
                            <tbody class="buscar text-uppercase">
                                <?php
                                foreach ($datos_articulos as $articulo) {
                                    $id_inventario = $articulo['id'];
                                    $descripcion   = $articulo['descripcion'];
                                    $marca         = $articulo['marca'];
                                    $modelo        = $articulo['modelo'];
                                    $usuario       = $articulo['usuario'];
                                    $area          = $articulo['area'];
                                    $codigo        = $articulo['codigo'];
                                    $estado        = $articulo['nombre_estado'];
                                    $id_estado     = $articulo['estado'];
                                    $observacion   = $articulo['observacion'];
                                    $id_area       = $articulo['id_area'];
                                    $id_user       = $articulo['id_user'];

                                    if ($id_estado == 2) {
                                        $ver          = '';
                                        $visible_rep  = 'd-none';
                                        $span         = '<span class="badge badge-success">Reportado</span>';
                                        $visible_trab = '';
                                        $span_casa    = '';
                                        $bg_color     = '';
                                    }

                                    if ($id_estado == 8) {
                                        $ver          = '';
                                        $visible_rep  = '';
                                        $visible_trab = 'd-none';
                                        $span         = '<span class="badge badge-primary">Trabajo en casa</span>';
                                    }

                                    if ($id_estado != 8) {
                                        $ver = 'd-none';
                                    }

                                    if ($articulo['confirmado'] == 0) {
                                        $visible_rep     = 'd-none';
                                        $visible_trab    = 'd-none';
                                        $bg_color        = '';
                                        $span_confirmado = '';
                                        $span            = '<span class="badge badge-danger">No Confirmado</span>';
                                    } else if ($articulo['confirmado'] == 1) {
                                        $span_confirmado = '<span class="badge badge-success">Confirmado</span>';
                                    } else if ($articulo['confirmado'] == 2) {
                                        $visible_rep     = 'd-none';
                                        $visible_trab    = 'd-none';
                                        $span_confirmado = '<span class="badge badge-warning">Pendiente de revision</span>';
                                    }

                                    ?>
                                    <tr class="text-center <?=$ver?>">
                                        <td><?=$area?></td>
                                        <td><?=$descripcion?></td>
                                        <td><?=$marca?></td>
                                        <td><?=$codigo?></td>
                                        <!-- <td><?=$estado?></td> -->
                                        <td><?=$observacion?></td>
                                        <td><?=$span_confirmado?></td>
                                        <td><?=$span?></td>
                                        <td>
                                            <div class="btn-group btn-group-sm" role="group">
                                                <button class="btn btn-success btn-sm <?=$visible_rep?>" data-tooltip="tooltip" data-placement="bottom" title="Reportar" data-toggle="modal" data-target="#rep_inv<?=$id_inventario?>">
                                                    <i class="fas fa-clipboard-check"></i>
                                                </button>
                                                <a href="<?=BASE_URL?>listado/historial?inventario=<?=base64_encode($id_inventario)?>" class="btn btn-info btn-sm" data-tooltip="tooltip" title="Ver mas">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>

                                    <!-- Reportar inventario -->
                                    <div class="modal fade" id="rep_inv<?=$id_inventario?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-md" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Reportar articulo</h5>
                                                </div>
                                                <form method="POST">
                                                    <div class="modal-body border-0">
                                                        <div class="row p-2">
                                                            <input type="hidden" value="<?=$id_log?>" name="id_log_rep">
                                                            <input type="hidden" value="<?=$id_user?>" name="id_user_rep">
                                                            <input type="hidden" value="<?=$id_area?>" name="id_area_rep">
                                                            <input type="hidden" value="<?=$descripcion?>" name="nom_inventario_rep">
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Area</label>
                                                                <input type="text" class="form-control" disabled maxlength="50" minlength="1" value="<?=$area?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Descripcion</label>
                                                                <input type="text" class="form-control" disabled maxlength="50" minlength="1" value="<?=$descripcion?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Marca</label>
                                                                <input type="text" class="form-control" disabled maxlength="50" minlength="1" value="<?=$marca?>">
                                                            </div>
                                                            <div class="col-lg-6 form-group">
                                                                <label class="font-weight-bold">Cantidad a Reportar</label>
                                                                <input type="number" class="form-control numeros" name="cantidad" required max="<?=$cantidad?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Responsable</label>
                                                                <input type="text" class="form-control" disabled maxlength="50" minlength="1" value="<?=$usuario?>">
                                                            </div>
                                                            <div class="form-group col-lg-12">
                                                                <label class="font-weight-bold">Observacion</label>
                                                                <textarea name="observacion" class="form-control" maxlength="1000" cols="30" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer border-0">
                                                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                                                            <i class="fa fa-times"></i>
                                                            &nbsp;
                                                            Cerrar
                                                        </button>
                                                        <button type="submit" class="btn btn-success btn-sm">
                                                            <i class="fas fa-clipboard-check"></i>
                                                            &nbsp;
                                                            Reportar
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!------------------------------------------------------->


                                    <!-- Trabajo en casa -->
                                    <div class="modal fade" id="trab_home<?=$id_inventario?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-md" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Trabajo en casa</h5>
                                                </div>
                                                <form method="POST">
                                                    <div class="modal-body border-0">
                                                        <div class="row p-2">
                                                            <input type="hidden" value="<?=$id_super_empresa?>" name="super_empresa_trab_home">
                                                            <input type="hidden" value="<?=$id_inventario?>" name="id_inventario_trab_home">
                                                            <input type="hidden" value="<?=$id_log?>" name="id_log_trab_home">
                                                            <input type="hidden" value="<?=$id_user?>" name="id_user_trab_home">
                                                            <input type="hidden" value="<?=$id_area?>" name="id_area_trab_home">
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Area</label>
                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?=$area?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Descripcion</label>
                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?=$descripcion?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Marca</label>
                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?=$marca?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Modelo</label>
                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?=$modelo?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">Responsable</label>
                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?=$usuario?>">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label class="font-weight-bold">No. Serie</label>
                                                                <input type="text" class="form-control letras" disabled maxlength="50" minlength="1" value="<?=$codigo?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer border-0">
                                                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                                                            <i class="fa fa-times"></i>
                                                            &nbsp;
                                                            Cerrar
                                                        </button>
                                                        <button type="submit" class="btn btn-success btn-sm">
                                                            <i class="fas fa-briefcase"></i>
                                                            &nbsp;
                                                            Aceptar
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!------------------------------------------------------->

                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['nom_inventario_rep'])) {
    $instancia->reportarArticuloListadoControl();
}

if (isset($_POST['id_inventario_trab_home'])) {
    $instancia->trabajoCasaListadoArticuloControl();
}
