<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'solicitud' . DS . 'ControlSolicitud.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia       = ControlSolicitud::singleton_solicitud();
$instancia_areas = ControlAreas::singleton_areas();

$datos_solicitud = $instancia->mostrarSolicitudesControl();

$permisos = $instancia_permiso->permisosUsuarioControl(47, $perfil_log);
if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						<a href="<?=BASE_URL?>compras/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-primary"></i>
						</a>
						&nbsp;
						Listado de solicitudes
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-dark"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
							<div class="dropdown-header">Acciones:</div>
							<a class="dropdown-item" href="<?=BASE_URL?>solicitud/index">Registrar Solicitud</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8 form-inline">
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar">
									<div class="input-group-prepend">
										<span class="input-group-text rounded-right" id="basic-addon1">
											<i class="fa fa-search"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No. solicitud</th>
									<th scope="col">Area</th>
									<th scope="col">Usuario</th>
									<th scope="col">Justificacion</th>
									<th scope="col">Fecha solicitado / aplazado</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_solicitud as $solicitud) {
									$id_solicitud  = $solicitud['id'];
									$id_area       = $solicitud['id_area'];
									$id_user       = $solicitud['id_user'];
									$nom_user      = $solicitud['nom_usuario'];
									$nom_area      = $solicitud['area_nom'];
									$estado        = $solicitud['estado'];
									$justificacion = $solicitud['justificacion'];
									$activo        = $solicitud['activo'];
									$motivo        = $solicitud['motivo'];

									$texto = ($activo == 0) ? $motivo : $justificacion;

									$fechareg = ($estado == 3 || $estado == 4) ? $solicitud['fecha_aplazado'] : $solicitud['fecha_solicitud'];

									$ver_boton        = '';
									$ver_imprimir     = 'd-none';
									$span             = '';
									$ver_verificacion = 'd-none';
									$ver_anular       = '';
									$editar           = 'd-none';

									if ($estado == 1) {
										$span             = '<span class="badge badge-success">Aprobada</span>';
										$ver_boton        = 'd-none';
										$ver_imprimir     = '';
										$ver_verificacion = '';
										$editar           = '';
									}

									if ($estado == 2) {
										$span             = '<span class="badge badge-danger">Rechazada</span>';
										$ver_boton        = 'd-none';
										$ver_imprimir     = 'd-none';
										$ver_verificacion = 'd-none';
										$editar           = 'd-none';
									}

									if ($estado == 3) {
										$span             = '<span class="badge badge-warning">Aplazada</span>';
										$ver_boton        = 'd-none';
										$ver_imprimir     = 'd-none';
										$ver_verificacion = 'd-none';
										$editar           = 'd-none';
									}

									if ($estado == 4) {
										$span             = '<span class="badge badge-secondary">Aprobada - pendiente</span>';
										$ver_boton        = 'd-none';
										$ver_imprimir     = 'd-none';
										$ver_verificacion = 'd-none';
										$editar           = '';
									}

									$datos_veriifcacion = $instancia->mostrarDatosVerificacionControl($id_solicitud);

									if ($datos_veriifcacion['id'] == '' && $estado == 1) {
										$ver_verificacion = '';
									} else {
										$ver_verificacion = 'd-none';
									}

									if ($estado == 0) {
										$ver_verificacion = 'd-none';
									}

									if ($fechareg == date('Y-m-d') && $estado == 3) {
										$ver_boton = '';
									}

									if ($fechareg == date('Y-m-d') && $estado == 4) {
										$ver_boton = '';
									}

									if ($activo == 0) {
										$span             = '<span class="badge badge-danger">Anulada</span>';
										$ver_boton        = 'd-none';
										$ver_imprimir     = 'd-none';
										$ver_verificacion = 'd-none';
										$ver_anular       = 'd-none';
										$editar           = 'd-none';
									}

									?>
									<tr class="text-center">
										<td><?=$id_solicitud?></td>
										<td><?=$nom_area?></td>
										<td><?=$nom_user?></td>
										<td><?=$texto?></td>
										<td><?=date('Y-m-d', strtotime($fechareg))?></td>
										<td><?=$span?></td>
										<td>
											<div class="btn-group btn-group-sm" role="group">
												<a class="btn btn-success btn-sm <?=$ver_boton?>" href="<?=BASE_URL?>solicitud/confirmar?solicitud=<?=base64_encode($id_solicitud)?>" data-tooltip="tooltip" data-placement="bottom" data-trigger="hover" title="Confirmar solicitud">
													<i class="fas fa-check-double"></i>
												</a>
												<a href="<?=BASE_URL?>solicitud/editar?solicitud=<?=base64_encode($id_solicitud)?>" class="btn btn-secondary btn-sm <?=$editar?>" data-tooltip="tooltip" data-placement="bottom" data-trigger="hover" title="Editar solicitud">
													<i class="fa fa-edit"></i>
												</a>
												<a class="btn btn-info btn-sm <?=$ver_verificacion?>" href="<?=BASE_URL?>solicitud/verificar?solicitud=<?=base64_encode($id_solicitud)?>" data-tooltip="tooltip" data-placement="bottom" data-trigger="hover" title="Verificar productos">
													<i class="fas fa-tasks"></i>
												</a>
												<a href="<?=BASE_URL?>imprimir/solicitud?solicitud=<?=base64_encode($id_solicitud)?>" class="btn btn-blue btn-sm <?=$ver_imprimir?>" data-tooltip="tooltip" data-placement="bottom" data-trigger="hover" title="Hoja de solicitud" target="_blank">
													<i class="fas fa-file-pdf"></i>
												</a>
												<a href="<?=BASE_URL?>imprimir/solicitudInicial?solicitud=<?=base64_encode($id_solicitud)?>" target="_blank" class="btn btn-primary btn-sm" data-tooltip="tooltip" data-placement="bottom"data-trigger="hover" title="Solicitud inicial">
													<i class="far fa-file-pdf"></i>
												</a>
											</div>
										</td>
										<td class="<?=$ver_anular?>">
											<button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#anular<?=$id_solicitud?>" data-tooltip="tooltip" data-placement="bottom"data-trigger="hover" title="Anular solicitud">
												<i class="fa fa-times"></i>
											</button>
										</td>
									</tr>



									<!-- Modal -->
									<div class="modal fade" id="anular<?=$id_solicitud?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Anular solicitud (#<?=$id_solicitud?>)</h5>
												</div>
												<form method="POST">
													<input type="hidden" name="id_solicitud" value="<?=$id_solicitud?>">
													<input type="hidden" name="id_log" value="<?=$id_log?>">
													<div class="modal-body">
														<div class="row">
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Motivo de anulacion</label>
																<textarea maxlength="1300" required rows="5" class="form-control" cols="4" name="motivo"></textarea>
															</div>
														</div>
													</div>
													<div class="modal-footer border-0">
														<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
															<i class="fa fa-times"></i>
															&nbsp;
															Cerrar
														</button>
														<button type="submit" class="btn btn-primary btn-sm">
															<i class="fa fa-save"></i>
															&nbsp;
															Enviar
														</button>
													</div>
												</form>
											</div>
										</div>
									</div>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['motivo'])) {
	$instancia->anularSolicitudControl();
}
?>