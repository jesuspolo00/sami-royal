<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';

$instancia           = ControlUsuarios::singleton_usuarios();
$instancia_matricula = ControlMatricula::singleton_matricula();

if (isset($_GET['id'])) {

    $id_acudiente     = base64_decode($_GET['id']);
    $datos_estudiante = $instancia->mostrarAcudientesEstudiantesControl($id_acudiente);
}

$permisos = $instancia_permiso->permisosUsuarioControl(73, $perfil_log);
if (!$permisos) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?=BASE_URL?>inicio" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Acudientes
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <div class="row">
                            <div class="col-lg-8 form-group">
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control filtro" placeholder="Buscar" aria-describedby="basic-addon2" name="buscar"data-tooltip="tooltip" data-trigger="focus" data-placement="top" title="Presione ENTER para buscar">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary btn-sm" type="button">
                                            <i class="fa fa-search"></i>
                                            &nbsp;
                                            Buscar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">Documento</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Correo</th>
                                    <th scope="col">Telefono</th>
                                </tr>
                            </thead>
                            <tbody class="buscar">
                                <?php
                                foreach ($datos_estudiante as $usuario) {
                                    $id_user          = $usuario['id_user'];
                                    $nombre           = $usuario['nombre'] . ' ' . $usuario['apellido'];
                                    $documento        = $usuario['documento'];
                                    $correo           = $usuario['correo'];
                                    $telefono         = $usuario['telefono'];
                                    $lugar_expedicion = $usuario['lugar_expedicion'];
                                    $direccion        = $usuario['direccion'];
                                    $ciudad           = $usuario['ciudad'];
                                    $celular          = $usuario['celular'];
                                    $firma_ant        = $usuario['firma'];

                                    $terminos = ($usuario['terminos'] == 0) ? '' : 'checked';

                                    $responsable_si = ($usuario['responsable'] == 1) ? 'selected' : '';
                                    $responsable_no = ($usuario['responsable'] == 0) ? 'selected' : '';

                                    ?>
                                    <tr class="text-center">
                                        <td><?=$documento?></td>
                                        <td class="text-uppercase"><?=$nombre?></td>
                                        <td><?=$correo?></td>
                                        <td><?=$telefono?></td>
                                        <td>
                                            <div class="btn-group" role="group">
                                                <button type="button" data-toggle="modal" data-target="#info_<?=$id_user?>" class="btn btn-primary btn-sm" data-tooltip="tooltip" title="Actualizar informacion">
                                                    <i class="fas fa-user-edit"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>

                                    <div class="modal fade" id="info_<?=$id_user?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title font-weight-bold text-primary" id="exampleModalLabel">Actualizar Informacion</h5>
                                            </div>
                                            <div class="modal-body">
                                                <form method="POST" enctype="multipart/form-data">
                                                    <input type="hidden" name="id_log" value="<?=$id_log?>">
                                                    <input type="hidden" name="id_user" value="<?=$id_user?>">
                                                    <input type="hidden" name="firma_ant" value="<?=$firma_ant?>">
                                                    <input type="hidden" name="firma_digital" value="<?=$usuario['firma_digital']?>">
                                                    <div class="row p-2">
                                                        <div class="col-lg-6 form-group">
                                                            <label class="font-weight-bold">Documento</label>
                                                            <input type="text" class="form-control" value="<?=$documento?>" disabled>
                                                        </div>
                                                        <div class="col-lg-6 form-group">
                                                            <label class="font-weight-bold">Nombre Completo</label>
                                                            <input type="text" class="form-control" value="<?=$nombre?>" disabled>
                                                        </div>
                                                        <div class="col-lg-6 form-group">
                                                            <label class="font-weight-bold">Telefono de contacto</label>
                                                            <input type="text" class="form-control numeros" name="telefono" value="<?=$telefono?>">
                                                        </div>
                                                        <div class="col-lg-6 form-group">
                                                            <label class="font-weight-bold">Correo <span class="text-danger">*</span></label>
                                                            <input type="email" class="form-control" name="correo" value="<?=$correo?>" required>
                                                        </div>
                                                        <div class="col-lg-6 form-group">
                                                            <label class="font-weight-bold">Lugar de expedicion del documento <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" name="lugar_expedicion" value="<?=$lugar_expedicion?>" required>
                                                        </div>
                                                        <div class="col-lg-6 form-group">
                                                            <label class="font-weight-bold">Direccion de domicilio <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" value="<?=$direccion?>" name="direccion" required>
                                                        </div>
                                                        <div class="col-lg-6 form-group">
                                                            <label class="font-weight-bold">Ciudad de domicilio <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" value="<?=$ciudad?>" name="ciudad" required>
                                                        </div>
                                                        <div class="col-lg-6 form-group">
                                                            <label class="font-weight-bold">Celular <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control numeros" name="celular" value="<?=$celular?>" required>
                                                        </div>
                                                        <div class="col-lg-6 form-group">
                                                            <label class="font-weight-bold">Responsable Economico <span class="text-danger">*</span></label>
                                                            <select name="responsable" class="form-control" required>
                                                                <option value="0" <?=$responsable_no?>>No</option>
                                                                <option value="1" <?=$responsable_si?>>Si</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-6 form-group">
                                                            <label class="font-weight-bold">Firma Digital <span class="text-danger">*</span></label>
                                                            <div class="custom-file pmd-custom-file-filled">
                                                                <input type="file" class="custom-file-input file_input" id="<?=$id_user?>" name="foto" accept=".png, .jpg, .jpeg">
                                                                <label class="custom-file-label file_label_<?=$id_user?>" for="customfilledFile"></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 form-group text-left mt-2">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="checkbox" name="terminos" <?=$terminos?> value="1" id="flexCheckDefault" required />
                                                                <label class="form-check-label" for="flexCheckDefault">Acepto el uso de firma digital para la firma de documentos de la instituci&oacute;n</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 form-group mt-4 text-right">
                                                            <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
                                                                <i class="fa fa-times"></i>
                                                                &nbsp;
                                                                Cancelar
                                                            </button>
                                                            <button class="btn btn-primary btn-sm" type="submit">
                                                                <i class="fa fa-save"></i>
                                                                &nbsp;
                                                                Guardar
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
    $instancia_matricula->informacionAcudienteControl();
}