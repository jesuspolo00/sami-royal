<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';

$instancia         = ControlInventario::singleton_inventario();
$instancia_usuario = ControlUsuarios::singleton_usuarios();
$instancia_areas   = ControlAreas::singleton_areas();

$datos_usuario   = $instancia_usuario->mostrarTodosUsuariosControl();
$datos_areas     = $instancia_areas->mostrarAreasControl($id_super_empresa);
$datos_categoria = $instancia->mostrarCategoriasControl($id_super_empresa);

if (isset($_POST['area_buscar'])) {

    $area     = $_POST['area_buscar'];
    $usuario  = $_POST['usuario_buscar'];
    $articulo = $_POST['articulo'];

    $datos = array(
        'area'     => $area,
        'usuario'  => $usuario,
        'articulo' => $_POST['articulo'],
    );

    $datos_articulo = $instancia->buscarInventarioDetalleControl($datos);
} else {

    $area     = '';
    $usuario  = '';
    $articulo = '';

    $datos_articulo = $instancia->mostrarInventarioDetalleControl();
}

$permisos = $instancia_permiso->permisosUsuarioControl(12, $perfil_log);
if (!$permisos) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?=BASE_URL?>inicio" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Listado
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <div class="row">
                            <div class="col-lg-4 form-group">
                                <select name="area_buscar" id="" class="form-control select2" data-tooltip="tooltip" title="Area">
                                    <option value="" selected>Seleccione un area...</option>
                                    <?php
                                    foreach ($datos_areas as $areas) {
                                        $id_area     = $areas['id'];
                                        $nombre_area = $areas['nombre'];

                                        $ver      = ($areas['activo'] == 0) ? 'd-none' : '';
                                        $selected = ($id_area == $area) ? 'selected' : '';
                                        ?>
                                        <option value="<?=$id_area?>" class="<?=$ver?> text-uppercase" <?=$selected?>><?=$nombre_area?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-4 form-group">
                                <select name="usuario_buscar" id="" class="form-control select2" data-tooltip="tooltip" title="Usuario">
                                    <option value="" selected>Seleccione una opcion...</option>
                                    <?php
                                    foreach ($datos_usuario as $usuarios) {
                                        $id_usuario  = $usuarios['id_user'];
                                        $nombre_user = $usuarios['nom_user'];

                                        $ver      = ($areas['estado'] == 'inactivo') ? 'd-none' : '';
                                        $selected = ($id_usuario == $usuario) ? 'selected' : '';
                                        ?>
                                        <option value="<?=$id_usuario?>" class="<?=$ver?> text-uppercase" <?=$selected?>><?=$nombre_user?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control filtro" placeholder="Buscar" aria-describedby="basic-addon2" name="articulo" value="<?=$articulo?>">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary btn-sm" type="submit">
                                            <i class="fa fa-search"></i>
                                            &nbsp;
                                            Buscar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">USUARIO</th>
                                    <th scope="col">AREA</th>
                                    <th scope="col">DESCRIPCION</th>
                                    <th scope="col">MARCA</th>
                                    <th scope="col">CANTIDAD</th>
                                </tr>
                            </thead>
                            <tbody class="buscar text-uppercase">
                                <?php
                                $url = base64_encode('1');
                                foreach ($datos_articulo as $inventario) {
                                    $id_inventario = $inventario['id'];
                                    $descripcion   = $inventario['descripcion'];
                                    $marca         = $inventario['marca'];
                                    $modelo        = $inventario['modelo'];
                                    $usuario       = $inventario['nom_user'];
                                    $area          = $inventario['nom_area'];
                                    $estado        = $inventario['estado'];
                                    $observacion   = $inventario['observacion'];
                                    $id_user       = $inventario['id_user'];
                                    $id_area       = $inventario['id_area'];
                                    $precio        = $inventario['precio'];
                                    $cantidad      = $inventario['cantidad'];

                                    $codigo = ($inventario['codigo'] == '') ? $inventario['id'] : $inventario['codigo'];

                                    if ($inventario['id_categoria'] == 1) {
                                        $hoja_vida = '<a href="' . BASE_URL . 'hoja_vida/index?inventario=' . base64_encode($id_inventario) . '">' . $descripcion . '</a>';
                                    } else {
                                        $hoja_vida = $descripcion;
                                    }

                                    $ver             = ($estado == 5 || $inventario['confirmado'] == 2) ? 'd-none' : '';
                                    $span_confirmado = ($inventario['confirmado'] == 0) ? '<span class="badge badge-danger">No confirmado</span>' : '<span class="badge badge-success">Confirmado</span>';

                                    ?>
                                    <tr class="text-center <?=$ver?>">
                                        <td><?=$usuario?></td>
                                        <td><?=$area?></td>
                                        <td><?=$hoja_vida?></td>
                                        <td><?=$marca?></td>
                                        <td><?=$cantidad?></td>
                                        <td><?=$span_confirmado?></td>
                                        <td>
                                            <div class="btn-group btn-group-sm" role="group">
                                                <a href="<?=BASE_URL?>listado/historial?inventario=<?=base64_encode($id_inventario)?>" class="btn btn-warning btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Ver historial">
                                                    <i class="fas fa-eye"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>

                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';