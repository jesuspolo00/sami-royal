<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';
require_once CONTROL_PATH . 'areas' . DS . 'ControlAreas.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia          = ControlInventario::singleton_inventario();
$instancia_areas    = ControlAreas::singleton_areas();
$instancia_usuarios = ControlUsuarios::singleton_usuarios();

$datos_areas   = $instancia_areas->mostrarAreasControl($id_super_empresa);
$datos_usuario = $instancia_usuarios->mostrarTodosUsuariosControl();

if (isset($_POST['buscar'])) {
    $datos            = array('area' => $_POST['area'], 'usuario' => $_POST['usuario'], 'buscar' => $_POST['buscar']);
    $datos_inventario = $instancia->mostrarDatosEquipoComputoControl($datos);
} else {
    $datos_inventario = $instancia->mostrarEquipoComputoControl();
}

$permisos = $instancia_permiso->permisosUsuarioControl(10, $perfil_log);

if (!$permisos) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?=BASE_URL?>inicio" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Mantenimientos preventivos
                    </h4>
                    <div class="btn-group">
                        <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#programar_mant">
                            <i class="fas fa-calendar-alt"></i>
                            &nbsp;
                            Programar Mantenimientos
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <div class="row">
                            <div class="col-lg-4 form-group">
                                <select class="form-control" name="area" data-tooltip="tooltip" title="Area" data-trigger="hover" data-placement="bottom">
                                    <option value="" selected>Seleccione una area...</option>
                                    <?php
                                    foreach ($datos_areas as $areas) {
                                        $id_area  = $areas['id'];
                                        $nom_area = $areas['nombre'];
                                        ?>
                                        <option value="<?=$id_area?>"><?=$nom_area?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-4 form-group">
                                <select class="form-control" name="usuario" data-tooltip="tooltip" title="Usuario" data-trigger="hover" data-placement="bottom">
                                    <option value="" selected>Seleccione un usuario...</option>
                                    <?php
                                    foreach ($datos_usuario as $usuario) {
                                        $id_usuario  = $usuario['id_user'];
                                        $nom_usuario = $usuario['nombre'] . ' ' . $usuario['apellido'];
                                        ?>
                                        <option value="<?=$id_usuario?>"><?=$nom_usuario?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="input-group mb-3">
                                  <input type="text" class="form-control filtro" name="buscar" placeholder="Buscar..." aria-describedby="basic-addon2">
                                  <div class="input-group-append">
                                    <button class="btn btn-primary btn-sm" type="submit">
                                        <i class="fa fa-search"></i>
                                        &nbsp;
                                        Buscar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="table-responsive mt-2">
                    <table class="table table-hover text-uppercase border table-sm" width="100%" cellspacing="0">
                        <thead>
                            <tr class="text-center font-weight-bold">
                                <th scope="col">ID</th>
                                <th scope="col">Usuario</th>
                                <th scope="col">Area</th>
                                <th scope="col">Descripcion</th>
                                <th scope="col">Marca</th>
                                <th scope="col">Modelo</th>
                                <th scope="col">Frecuencia mantenimiento</th>
                                <th scope="col">Frecuencia copia de seguridad</th>
                                <th scope="col">Fecha proximo mantenimiento</th>
                            </tr>
                        </thead>
                        <tbody class="buscar text-uppercase">
                            <?php
                            foreach ($datos_inventario as $inventario) {
                                $id_inventario    = $inventario['id'];
                                $descripcion      = $inventario['descripcion'];
                                $marca            = $inventario['marca'];
                                $modelo           = $inventario['modelo'];
                                $codigo           = ($inventario['codigo'] == '') ? $id_inventario : $inventario['codigo'];
                                $usuario          = $inventario['usuario'];
                                $area             = $inventario['nom_area'];
                                $estado           = $inventario['estado'];
                                $observacion      = $inventario['observacion'];
                                $id_user          = $inventario['id_user'];
                                $id_area          = $inventario['id_area'];
                                $id_categoria     = $inventario['id_categoria'];
                                $frecuencia       = $inventario['frecuencia'];
                                $frecuencia_copia = $inventario['frecuencia_copia'];

                                $fecha      = date('Y-m', strtotime($inventario['ultimo_mant']));
                                $nuevafecha = strtotime('+' . $frecuencia . ' month', strtotime($fecha));
                                $nuevafecha = date('Y-m', $nuevafecha);

                                if ($id_categoria == 1) {
                                    $hoja_vida = '<a href="' . BASE_URL . 'hoja_vida/index?inventario=' . base64_encode($id_inventario) . '">' . $descripcion . '</a>';
                                } else {
                                    $hoja_vida = $descripcion;
                                }

                                if ($estado == 3 || $estado == 1) {
                                    $visible_descargar = 'd-none';
                                    $visible_mant      = '';
                                } else {
                                    $visible_mant      = 'd-none';
                                    $visible_descargar = '';
                                }

                                if ($estado != 5 && $estado != 4) {
                                    ?>
                                    <tr class="text-center">
                                        <td><?=$id_inventario?></td>
                                        <td><?=$usuario?></td>
                                        <td><?=$area?></td>
                                        <td><?=$hoja_vida?></td>
                                        <td><?=$marca?></td>
                                        <td><?=$modelo?></td>
                                        <td><?=$frecuencia?> Meses</td>
                                        <td><?=$frecuencia_copia?> Meses</td>
                                        <td><?=$nuevafecha?></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'mantenimientos' . DS . 'programarMant.php';

if (isset($_POST['id_log'])) {
    $instancia->programarMantenimientosControl();
}