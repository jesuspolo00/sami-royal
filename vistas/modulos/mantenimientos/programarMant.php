<div class="modal fade" id="programar_mant" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-primary" id="exampleModalLabel">Programar Mantenimiento</h5>
      </div>
      <div class="modal-body">
        <form method="POST">
          <input type="hidden" name="id_log" value="<?=$id_log?>">
          <div class="row p-2">
            <div class="col-lg-12 form-group">
              <h6 class="text-danger"><span class="font-weight-bold">Nota:</span> Este accion solo creara mantenimientos a los articulos que esten en estado NUEVO o ARREGLADO. ( Omitira los que tengan algun reporte y no este solucionado hasta la fecha)</h6>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Fecha desde <span class="text-danger">*</span></label>
              <input type="date" class="form-control" name="fecha_desde" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Fecha hasta <span class="text-danger">*</span></label>
              <input type="date" class="form-control" name="fecha_hasta" required>
            </div>
            <div class="col-lg-12 form-group text-right mt-2">
              <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
                <i class="fa fa-times"></i>
                &nbsp;
                Cancelar
              </button>
              <button class="btn btn-primary btn-sm" type="submit">
                <i class="fa fa-save"></i>
                &nbsp;
                Guardar
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
