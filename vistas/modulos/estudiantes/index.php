<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';

$instancia           = ControlUsuarios::singleton_usuarios();
$instancia_matricula = ControlMatricula::singleton_matricula();

if (isset($_GET['id'])) {

    $id_acudiente     = base64_decode($_GET['id']);
    $datos_estudiante = $instancia->mostrarEstudiantesAcudienteControl($id_acudiente);
}

$permisos = $instancia_permiso->permisosUsuarioControl(73, $perfil_log);
if (!$permisos) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}

$url = 1;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?=BASE_URL?>inicio" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Estudiantes
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <div class="row">
                            <div class="col-lg-8 form-group">
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control filtro" placeholder="Buscar" aria-describedby="basic-addon2" name="buscar"data-tooltip="tooltip" data-trigger="focus" data-placement="top" title="Presione ENTER para buscar">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary btn-sm" type="button">
                                            <i class="fa fa-search"></i>
                                            &nbsp;
                                            Buscar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">Documento</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Correo</th>
                                    <th scope="col">Telefono</th>
                                    <th scope="col">Nivel</th>
                                    <th scope="col">Curso Actual</th>
                                    <th scope="col">Curso Proximo</th>
                                </tr>
                            </thead>
                            <tbody class="buscar">
                                <?php
                                foreach ($datos_estudiante as $usuario) {
                                    $id_user       = $usuario['id_user'];
                                    $id_estudiante = $usuario['id_user'];
                                    $nombre        = $usuario['nombre'] . ' ' . $usuario['apellido'];
                                    $documento     = $usuario['documento'];
                                    $correo        = $usuario['correo'];
                                    $telefono      = $usuario['telefono'];
                                    $nom_curso     = $usuario['nom_curso'];
                                    $nom_nivel     = $usuario['nom_nivel'];
                                    $curso_proximo = (empty($usuario['curso_proximo_est'])) ? $usuario['curso_proximo'] : $usuario['curso_proximo_est'];

                                    $datos_contrato   = $instancia_matricula->mostrarContratoGeneradoControl($id_user, date('Y'));
                                    $datos_documentos = $instancia_matricula->mostrarDocumentosEstudianteControl(date('Y'), $id_estudiante);

                                    $ver_subir         = (empty($datos_documentos['id'])) ? '' : 'd-none';
                                    $ver_contrato_prev = ($datos_contrato['estado'] == 2 || empty($datos_contrato['id'])) ? '' : 'd-none';

                                    $ver_contrato = (empty($datos_contrato['id'])) ? 'd-none' : '';
                                    $ver_contrato = ($datos_contrato['estado'] == 0) ? 'd-none' : $ver_contrato;
                                    $ver_contrato = ($datos_contrato['estado'] == 1) ? '' : $ver_contrato;
                                    $ver_contrato = ($datos_contrato['estado'] == 2) ? 'd-none' : $ver_contrato;

                                    if (empty($datos_contrato['estado']) && empty($datos_contrato['id'])) {
                                        $span_contrato = '';
                                    }

                                    if ($datos_contrato['estado'] == 0 && !empty($datos_contrato['id'])) {
                                        $span_contrato = '<span class="badge badge-warning">Pendiente</span>';
                                    }

                                    if ($datos_contrato['estado'] == 1 && !empty($datos_contrato['id'])) {
                                        $span_contrato = '<span class="badge badge-success">Aprobado</span>';
                                    }

                                    if ($datos_contrato['estado'] == 2 && !empty($datos_contrato['id'])) {
                                        $span_contrato = '<span class="badge badge-danger">Denegado</span>';
                                    }

                                    ?>
                                    <tr class="text-center">
                                        <td><?=$documento?></td>
                                        <td class="text-uppercase"><?=$nombre?></td>
                                        <td><?=$correo?></td>
                                        <td><?=$telefono?></td>
                                        <td><?=$nom_nivel?></td>
                                        <td><?=$nom_curso?></td>
                                        <td><?=$curso_proximo?></td>
                                        <td>
                                            <?=$span_contrato?>
                                        </td>
                                        <td>
                                            <div class="btn-group" role="group">
                                                <a href="<?=BASE_URL?>imprimir/matricula/contratoPrev?estudiante=<?=base64_encode($id_user)?>" target="_blank" class="btn btn-info btn-sm float-left <?=$ver_contrato_prev?>" data-tooltip="tooltip" title="Previsualizar Contrato" data-placement="bottom">
                                                    <i class="fas fa-file-contract"></i>
                                                </a>
                                                <a href="<?=BASE_URL?>estudiantes/informacion?estudiante=<?=base64_encode($id_user)?>" class="btn btn-primary btn-sm <?=$ver_contrato_prev?>" data-tooltip="tooltip" title="Editar informacion" data-placement="bottom">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <button class="btn btn-secondary btn-sm <?=$ver_subir?>" type="button" data-toggle="modal" data-target="#subir_documentos" data-tooltip="tooltip" title="Subir Documentos" data-placement="bottom">
                                                    <i class="fa fa-upload"></i>
                                                </button>
                                                <a href="<?=BASE_URL?>matricula/historial/index?estudiante=<?=base64_encode($id_user)?>&enlace=1" class="btn btn-info btn-sm <?=$ver_contrato?>" data-tooltip="tooltip" data-placement="bottom" title="Historial de contratos">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a href="<?=BASE_URL?>imprimir/matricula/contratoEstudiante?contrato=<?=base64_encode($datos_contrato['id'])?>" target="_blank" class="btn btn-primary btn-sm <?=$ver_contrato?>" data-tooltip="tooltip" title="Ultimo Contrato" data-placement="bottom">
                                                    <i class="fas fa-file-contract"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'estudiantes' . DS . 'documentos.php';

if (isset($_POST['id_estudiante_documentos'])) {
    $instancia_matricula->subirDocumentosEstudianteControl();
}