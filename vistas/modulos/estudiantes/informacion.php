<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia           = ControlUsuarios::singleton_usuarios();
$instancia_matricula = ControlMatricula::singleton_matricula();
$instancia_perfil    = ControlPerfil::singleton_perfil();

if (isset($_GET['estudiante'])) {

    $id_estudiante = base64_decode($_GET['estudiante']);

    $datos_estudiante      = $instancia_perfil->mostrarDatosPerfilControl($id_estudiante);
    $datos_info_estudiante = $instancia_matricula->mostrarInformacionEstudianteControl($id_estudiante);
    $datos_clinicos        = $instancia_matricula->mostrarHistoriaClinicaControl($id_estudiante);

    $datos_tipo_doc        = $instancia_matricula->mostrarTiposDocumentoControl();
    $datos_estrato         = $instancia_matricula->mostrarEstratosControl();
    $datos_tipo_vivienda   = $instancia_matricula->mostrarDatosTipoViviendaControl();
    $datos_tipo_sangre     = $instancia_matricula->mostrarDatosTipoSangreControl();
    $datos_religion        = $instancia_matricula->mostrarDatosReligionControl();
    $datos_estado          = $instancia_matricula->mostrarDatosEstadoPadresControl();
    $datos_contacto        = $instancia_matricula->mostrarContactoEmergenciaControl();
    $datos_eps             = $instancia_matricula->mostrarDatosEpsControl();
    $datos_documentos      = $instancia_matricula->mostrarDocumentosEstudianteControl(date('Y'), $id_estudiante);
    $datos_contrato        = $instancia_matricula->mostrarContratoGeneradoControl($id_estudiante, date('Y'));
    $datos_contrato_ultimo = $instancia_matricula->datosUltimoContratoNivelControl($datos_estudiante['id_nivel']);

    $ver_subir     = (empty($datos_documentos['id'])) ? '' : 'd-none';
    $ver_documento = (!empty($datos_documentos['id'])) ? '' : 'd-none';

    $ver_contrato_prev = (empty($datos_contrato['id'])) ? 'd-none' : '';
    $ver_contrato_prev = ($datos_contrato['estado'] == 0) ? 'd-none' : $ver_contrato_prev;
    $ver_contrato_prev = ($datos_contrato['estado'] == 1) ? 'd-none' : $ver_contrato_prev;
    $ver_contrato_prev = ($datos_contrato['estado'] == 2) ? '' : $ver_contrato_prev;

    $url = 2;
}

$permisos = $instancia_permiso->permisosUsuarioControl(73, $perfil_log);
if (!$permisos) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?=BASE_URL?>estudiantes/index?id=<?=base64_encode($id_log)?>" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Informacion estudiante
                    </h4>
                    <div class="btn-group">
                        <button class="btn btn-secondary btn-sm <?=$ver_subir?>" type="button" data-toggle="modal" data-target="#subir_documentos">
                            <i class="fa fa-upload"></i>
                            &nbsp;
                            Subir Documentos
                        </button>
                        <a target="_blank" href="<?=PUBLIC_PATH?>upload/<?=$datos_documentos['documento_estudiante']?>" class="btn btn-info btn-sm <?=$ver_documento?>">
                            <i class="fa fa-eye"></i>
                            &nbsp;
                            Documento Estudiante
                        </a>
                        &nbsp;
                        <a target="_blank" href="<?=PUBLIC_PATH?>upload/<?=$datos_documentos['certificado']?>" class="btn btn-info btn-sm <?=$ver_documento?>">
                            <i class="fa fa-eye"></i>
                            &nbsp;
                            Certificado Medico
                        </a>
                        &nbsp;
                        <a target="_blank" href="<?=PUBLIC_PATH?>upload/<?=$datos_documentos['documento_acudiente']?>" class="btn btn-info btn-sm <?=$ver_documento?>">
                            <i class="fa fa-eye"></i>
                            &nbsp;
                            Documento Responsable Financiero
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <input type="hidden" name="id_estudiante" value="<?=$id_estudiante?>">
                        <input type="hidden" name="id_log" value="<?=$id_log?>">
                        <input type="hidden" name="id_contrato" value="<?=$datos_contrato_ultimo['id']?>">
                        <input type="hidden" name="anio" value="<?=$datos_contrato_ultimo['anio']?>">
                        <input type="hidden" name="curso_proximo" value="<?=$datos_estudiante['curso_proximo_user']?>">
                        <div class="row p-2">
                            <div class="col-lg-4 form-group mt-4">
                                <div class="circular--portrait">
                                    <img src="<?=PUBLIC_PATH?>upload/<?=$datos_estudiante['foto_carnet']?>" alt="">
                                </div>
                            </div>
                            <div class="col-lg-8 form-group mt-2">
                                <div class="row">
                                    <div class="col-lg-6 form-group">
                                        <label class="font-weight-bold">Numero de documento <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="documento" value="<?=$datos_estudiante['documento']?>" required>
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        <label class="font-weight-bold">Tipo de documento <span class="text-danger">*</span></label>
                                        <select name="tipo_documento" class="form-control" required>
                                            <option value="" selected>Seleccione una opcion...</option>
                                            <?php
                                            foreach ($datos_tipo_doc as $tipo_doc) {
                                                $id_tipo_doc = $tipo_doc['id'];
                                                $nom_tipo    = $tipo_doc['nombre'];

                                                $select_tipo = ($datos_info_estudiante['tipo_documento'] == $id_tipo_doc) ? 'selected' : '';

                                                ?>
                                                <option value="<?=$id_tipo_doc?>" <?=$select_tipo?>><?=$nom_tipo?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        <label class="font-weight-bold">Lugar de expidicion <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="lugar_expedicion" value="<?=$datos_info_estudiante['lugar_expedicion']?>" required>
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        <label class="font-weight-bold">Genero <span class="text-danger">*</span></label>
                                        <select name="genero" class="form-control" required>
                                            <option value="" selected>Seleccione una opcion...</option>
                                            <?php
                                            for ($i = 1; $i < 3; $i++) {
                                                $valor = ($i == 1) ? 'Masculino' : 'Femenino';

                                                $select_genero = ($datos_info_estudiante['genero'] == $i) ? 'selected' : '';
                                                ?>
                                                <option value="<?=$i?>" <?=$select_genero?>><?=$valor?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        <label class="font-weight-bold">Nombres <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="nombre" value="<?=$datos_estudiante['nombre']?>" required>
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        <label class="font-weight-bold">Apellidos <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="apellido" value="<?=$datos_estudiante['apellido']?>" required>
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        <label class="font-weight-bold">Telefono <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control numeros" name="telefono" value="<?=$datos_estudiante['telefono']?>" required>
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        <label class="font-weight-bold">Fecha de nacimiento <span class="text-danger">*</span></label>
                                        <input type="date" class="form-control" name="fecha_nacimiento" value="<?=$datos_info_estudiante['fecha_nacimiento']?>" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 form-group mt-2">
                                <h5 class="font-weight-bold text-primary text-uppercase">Lugar de nacimiento</h5>
                                <hr>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Pais <span class="text-danger">*</span></label>
                                <input type="text" name="pais_nac" class="form-control" value="<?=$datos_info_estudiante['pais_nac']?>" required>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Departamento <span class="text-danger">*</span></label>
                                <input type="text" name="departamento_nac" class="form-control" value="<?=$datos_info_estudiante['departamento_nac']?>" required>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Ciudad <span class="text-danger">*</span></label>
                                <input type="text" name="ciudad_nac" class="form-control" value="<?=$datos_info_estudiante['ciudad_nac']?>" required>
                            </div>
                            <div class="col-lg-12 form-group mt-2">
                                <h5 class="font-weight-bold text-primary text-uppercase">Lugar de residencia</h5>
                                <hr>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Pais <span class="text-danger">*</span></label>
                                <input type="text" name="pais_resi" class="form-control" value="<?=$datos_info_estudiante['pais_resi']?>" required>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Departamento <span class="text-danger">*</span></label>
                                <input type="text" name="departamento_resi" class="form-control" value="<?=$datos_info_estudiante['departamento_resi']?>" required>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Ciudad <span class="text-danger">*</span></label>
                                <input type="text" name="ciudad_resi" class="form-control" value="<?=$datos_info_estudiante['ciudad_resi']?>" required>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Direccion <span class="text-danger">*</span></label>
                                <input type="text" name="direccion" class="form-control" value="<?=$datos_info_estudiante['direccion']?>" required>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Barrio <span class="text-danger">*</span></label>
                                <input type="text" name="barrio" class="form-control" value="<?=$datos_info_estudiante['barrio']?>" required>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Estrato <span class="text-danger">*</span></label>
                                <select name="estrato" class="form-control" required>
                                    <option value="" selected>Seleccione una opcion...</option>
                                    <?php
                                    foreach ($datos_estrato as $estrato) {
                                        $id_estrato  = $estrato['id'];
                                        $nom_estrato = $estrato['nombre'];

                                        $select_estrato = ($datos_info_estudiante['estrato'] == $id_estrato) ? 'selected' : '';
                                        ?>
                                        <option value="<?=$id_estrato?>" <?=$select_estrato?>><?=$nom_estrato?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Tipo de vivienda <span class="text-danger">*</span></label>
                                <select name="tipo_vivienda" class="form-control" required>
                                    <option value="" selected>Seleccione una opcion...</option>
                                    <?php
                                    foreach ($datos_tipo_vivienda as $tipo_vivienda) {
                                        $id_tipo_vivienda  = $tipo_vivienda['id'];
                                        $nom_tipo_vivienda = $tipo_vivienda['nombre'];

                                        $select_tipo_vivienda = ($datos_info_estudiante['tipo_vivienda'] == $id_tipo_vivienda) ? 'selected' : '';
                                        ?>
                                        <option value="<?=$id_tipo_vivienda?>" <?=$select_tipo_vivienda?>><?=$nom_tipo_vivienda?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Telefono <span class="text-danger">*</span></label>
                                <input type="text" name="telefono_resi" class="form-control numeros" value="<?=$datos_info_estudiante['telefono_resi']?>" required>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Celular <span class="text-danger">*</span></label>
                                <input type="text" name="celular_resi" class="form-control numeros" value="<?=$datos_info_estudiante['celular_resi']?>" required>
                            </div>
                            <div class="col-lg-12 form-group mt-2">
                                <hr>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Tiene Hermanos ? <span class="text-danger">*</span></label>
                                <select name="hermanos" class="form-control" required>
                                    <?php
                                    for ($i = 1; $i < 3; $i++) {
                                        $valor  = ($i == 1) ? 'Si' : 'No';
                                        $select = ($i == $datos_info_estudiante['hermanos']) ? 'selected' : '';
                                        ?>
                                        <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Hijo Mayor ? <span class="text-danger">*</span></label>
                                <select name="hijo_mayor" class="form-control" required>
                                    <?php
                                    for ($i = 1; $i < 3; $i++) {
                                        $valor  = ($i == 1) ? 'Si' : 'No';
                                        $select = ($i == $datos_info_estudiante['hijo_mayor']) ? 'selected' : '';
                                        ?>
                                        <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Correo electronico <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="correo" value="<?=$datos_estudiante['correo']?>" required>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Correo electronico institucional <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="correo_institucional" value="<?=$datos_info_estudiante['correo_institucional']?>" required>
                            </div>
                            <div class="col-lg-4 form-group">
                                <label class="font-weight-bold">Estatura <span class="text-danger">*</span></label>
                                <div class="input-group">
                                  <input type="text" class="form-control numeros" name="estatura" value="<?=$datos_info_estudiante['estatura']?>" required>
                                  <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">Cm</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 form-group">
                            <label class="font-weight-bold">Peso <span class="text-danger">*</span></label>
                            <div class="input-group">
                              <input type="text" class="form-control numeros" name="peso" value="<?=$datos_info_estudiante['peso']?>" required>
                              <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2">Kg</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Tipo de sangre <span class="text-danger">*</span></label>
                        <select name="tipo_sangre" class="form-control" required>
                            <option value="" selected>Seleccione una opcion...</option>
                            <?php
                            foreach ($datos_tipo_sangre as $tipo_sangre) {
                                $id_tipo_sangre  = $tipo_sangre['id'];
                                $nom_tipo_sangre = $tipo_sangre['nombre'];

                                $select_tipo_sangre = ($id_tipo_sangre == $datos_info_estudiante['tipo_sangre']) ? 'selected' : '';
                                ?>
                                <option value="<?=$id_tipo_sangre?>" <?=$select_tipo_sangre?>><?=$nom_tipo_sangre?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Religion <span class="text-danger">*</span></label>
                        <select name="religion" class="form-control" required>
                            <option value="" selected>Seleccione una opcion...</option>
                            <?php
                            foreach ($datos_religion as $religion) {
                                $id_religion  = $religion['id'];
                                $nom_religion = $religion['nombre'];

                                $select_religion = ($id_religion == $datos_info_estudiante['religion']) ? 'selected' : '';
                                ?>
                                <option value="<?=$id_religion?>" <?=$select_religion?>><?=$nom_religion?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Estado civil de los padres <span class="text-danger">*</span></label>
                        <select name="estado_civil_padres" class="form-control" required>
                            <option value="" selected>Seleccione una opcion...</option>
                            <?php
                            foreach ($datos_estado as $estado_civil) {
                                $id_estado_civil  = $estado_civil['id'];
                                $nom_estado_civil = $estado_civil['nombre'];

                                $select_estado_civil = ($datos_info_estudiante['estado_civil_padres'] == $id_estado_civil) ? 'selected' : '';
                                ?>
                                <option value="<?=$id_estado_civil?>" <?=$select_estado_civil?>><?=$nom_estado_civil?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">En caso de emergencia contactar a <span class="text-danger">*</span></label>
                        <select name="contactar" class="form-control" required>
                            <option value="" selected>Seleccione una opcion...</option>
                            <?php
                            foreach ($datos_contacto as $contacto) {
                                $id_contacto  = $contacto['id'];
                                $nom_contacto = $contacto['nombre'];

                                $select_contacto = ($id_contacto == $datos_info_estudiante['contactar']) ? 'selected' : '';
                                ?>
                                <option value="<?=$id_contacto?>" <?=$select_contacto?>><?=$nom_contacto?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Establecimiento de donde procede</label>
                        <input type="text" class="form-control" name="establecimiento" value="<?=$datos_info_estudiante['establecimiento']?>">
                    </div>
                    <div class="col-lg-12 form-group mt-2">
                        <h5 class="font-weight-bold text-center text-uppercase mt-4 mb-3">AUTORIZACIÓN PARA USO DE REGISTRO FOTOGRÁFICO Y VIDEOS</h5>
                        <center>
                            <p class="text-justify w-75">Solicitamos a los Padres de Familia autorizar al Colegio para publicar, distribuir, grabar y producir registro fotográfico y de video para mi hijo(a), con el fin de que pueda ser utilizado exclusivamente para fines de la institución como es el uso de la página Web , las redes sociales oficiales y la realización de cualquier publicidad del colegio.</p>
                            <p class="text-justify w-75">Todo esto con el buen uso y respetando la imagen de los estudiantes, el derecho a su intimidad personal y su honor. </p>
                            <div class="col-lg-4 form-group form-inline">
                                <label class="font-weight-bold text-danger">Autorizo el uso de la imagen de mi hijo(a)</label>
                                &nbsp;
                                <select name="autorizo_imagen" class="form-control" required>
                                    <?php
                                    for ($i = 1; $i < 3; $i++) {
                                        $valor  = ($i == 1) ? 'Si' : 'No';
                                        $select = ($i == $datos_info_estudiante['autorizo_imagen']) ? 'selected' : '';
                                        ?>
                                        <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </center>
                    </div>
                    <!------------------->
                    <div class="col-lg-12 form-group mt-3">
                        <h5 class="font-weight-bold text-primary text-uppercase text-center">Datos Medicos</h5>
                        <hr>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="font-weight-bold">Utiliza diariamente o en casos de urgencia medicamentos preescritos <span class="text-danger">*</span></label>
                        <select name="medicamentos_pree" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['medicamentos_pree']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="font-weight-bold">Cual</label>
                        <input type="text" class="form-control" name="cual_medicamento" value="<?=$datos_clinicos['cual_medicamento']?>">
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Dosis</label>
                        <input type="text" class="form-control" name="dosis_medicamento" value="<?=$datos_clinicos['dosis_medicamento']?>">
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Presenta alergias <span class="text-danger">*</span></label>
                        <select name="alergias" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['alergias']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Especifica a que</label>
                        <input type="text" class="form-control" name="alergia_cual" value="<?=$datos_clinicos['alergia_cual']?>">
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Tratamiento para las alergias</label>
                        <input type="text" class="form-control" name="alergia_tratamiento" value="<?=$datos_clinicos['alergia_tratamiento']?>">
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Alergia a algun alimento <span class="text-danger">*</span></label>
                        <select name="alergia_alimento" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['alergia_alimento']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Especifique cuales</label>
                        <input type="text" class="form-control" name="alergia_alimento_cual" value="<?=$datos_clinicos['alergia_alimento_cual']?>">
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Dieta normal <span class="text-danger">*</span></label>
                        <select name="dieta" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['dieta']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Cual ?</label>
                        <input type="text" class="form-control" name="dieta_cual" value="<?=$datos_clinicos['dieta_cual']?>">
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Seguro Accidente <span class="text-danger">*</span></label>
                        <select name="seguro" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['seguro']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <!------------------->
                    <div class="col-lg-12 form-group mt-2">
                        <h5 class="font-weight-bold text-primary text-uppercase">Medicinas que el colegio puede suministrar </h5>
                        <hr>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">ANTIALERGICOS (alergias): LORATADINA <span class="text-danger">*</span></label>
                        <select name="loratadina" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['loratadina']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">ANALGESICOS (dolor y fiebre): DOLEX <span class="text-danger">*</span></label>
                        <select name="dolex" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['dolex']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">ANTIACIDO : MILANTA <span class="text-danger">*</span></label>
                        <select name="milanta" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['milanta']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="font-weight-bold">ANTIESPASMODICO:(cólicos mestruales):BUSCAPINA <span class="text-danger">*</span></label>
                        <select name="buscapina" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['buscapina']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="font-weight-bold">Seguro o EPS afiliada  <span class="text-danger">*</span></label>
                        <select name="eps" class="form-control" required>
                            <option value="" selected>Seleccione una opcion...</option>
                            <?php
                            foreach ($datos_eps as $eps) {
                                $id_eps  = $eps['id'];
                                $nom_eps = $eps['nombre'];

                                $select_eps = ($id_eps == $datos_clinicos['eps']) ? 'selected' : '';
                                ?>
                                <option value="<?=$id_eps?>" <?=$select_eps?>><?=$nom_eps?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Nombre del medico</label>
                        <input type="text" class="form-control" name="nombre_medico" value="<?=$datos_clinicos['nombre_medico']?>">
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Telefono del medico</label>
                        <input type="text" class="form-control" name="telefono_medico" value="<?=$datos_clinicos['telefono_medico']?>">
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Clinica de preferencia</label>
                        <input type="text" class="form-control" name="clinica" value="<?=$datos_clinicos['clinica']?>">
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Recomendaciones Medicas</label>
                        <input type="text" class="form-control" name="recomendacion_medica" value="<?=$datos_clinicos['recomendacion_medica']?>">
                    </div>
                    <!------------------->
                    <div class="col-lg-12 form-group mt-2">
                        <h5 class="font-weight-bold text-primary text-uppercase">Antecedentes Personales</h5>
                        <hr>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Enfermedad en los ojos <span class="text-danger">*</span></label>
                        <select name="enfermedad_ojos" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['enfermedad_ojos']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Lentes permanentes <span class="text-danger">*</span></label>
                        <select name="lentes" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['lentes']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Asma <span class="text-danger">*</span></label>
                        <select name="asma" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['asma']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Enfermedad Respiratoria <span class="text-danger">*</span></label>
                        <select name="enfermedad_respiratoria" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['enfermedad_respiratoria']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Enfermedad Cardiaca <span class="text-danger">*</span></label>
                        <select name="enfermedad_cardiaca" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['enfermedad_cardiaca']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Enfermedad Gastricas <span class="text-danger">*</span></label>
                        <select name="enfermedad_gastricas" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['enfermedad_gastricas']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Diabetes <span class="text-danger">*</span></label>
                        <select name="enfermedad_diabetes" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['enfermedad_diabetes']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Convulsiones <span class="text-danger">*</span></label>
                        <select name="convulsiones" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['convulsiones']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Celiaquismo <span class="text-danger">*</span></label>
                        <select name="celiaquismo" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['celiaquismo']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Problemas psiquiatricos <span class="text-danger">*</span></label>
                        <select name="psiquiatra" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['psiquiatra']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Hepatitis <span class="text-danger">*</span></label>
                        <select name="hepatitis" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['hepatitis']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Anemia <span class="text-danger">*</span></label>
                        <select name="anemia" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['anemia']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Hipertension arterial <span class="text-danger">*</span></label>
                        <select name="hipertension_arterial" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['hipertension_arterial']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Hipotension arterial <span class="text-danger">*</span></label>
                        <select name="hipotension_arterial" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['hipotension_arterial']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Epilepsia <span class="text-danger">*</span></label>
                        <select name="epilepsia" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['epilepsia']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Hernias <span class="text-danger">*</span></label>
                        <select name="hernias" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['hernias']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Migra&ntilde;as <span class="text-danger">*</span></label>
                        <select name="migrana" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['migrana']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Fractura / Traumas <span class="text-danger">*</span></label>
                        <select name="fractura_trauma" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['fractura_trauma']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Accidente cerebrovascular <span class="text-danger">*</span></label>
                        <select name="accidente_cardio" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['accidente_cardio']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Artritis reumatoidea <span class="text-danger">*</span></label>
                        <select name="artritis" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['artritis']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Desnutricion <span class="text-danger">*</span></label>
                        <select name="desnutricion" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['desnutricion']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Obesidad <span class="text-danger">*</span></label>
                        <select name="obesidad" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['obesidad']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Cancer <span class="text-danger">*</span></label>
                        <select name="cancer" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['cancer']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">VIH <span class="text-danger">*</span></label>
                        <select name="vih" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['vih']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="font-weight-bold">Enfermedad renal crónica (no incluye infecciones urinarias) <span class="text-danger">*</span></label>
                        <select name="enfermedad_renal" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['enfermedad_renal']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="font-weight-bold">Otros</label>
                        <input type="text" class="form-control" name="otros" value="<?=$datos_clinicos['otros']?>">
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="font-weight-bold">Prótesis</label>
                        <input type="text" class="form-control" name="protesis" value="<?=$datos_clinicos['protesis']?>">
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="font-weight-bold">Cirugías</label>
                        <input type="text" class="form-control" name="cirugias" value="<?=$datos_clinicos['cirugias']?>">
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="font-weight-bold">El estudiantes ha convulsionado o perdido el conocimiento?</label>
                        <input type="text" class="form-control" name="perdido_conocimiento" value="<?=$datos_clinicos['perdido_conocimiento']?>">
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="font-weight-bold">Enfermedad Actual</label>
                        <input type="text" class="form-control" name="enfermedad_actual" value="<?=$datos_clinicos['enfermedad_actual']?>">
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="font-weight-bold">Medicamentos que no puede recibir</label>
                        <input type="text" class="form-control" name="medicamentos_no" value="<?=$datos_clinicos['medicamentos_no']?>">
                    </div>
                    <div class="col-lg-6 form-group">
                        <label class="font-weight-bold">Condiciones especiales de salud</label>
                        <input type="text" class="form-control" name="condicion_salud" value="<?=$datos_clinicos['condicion_salud']?>">
                    </div>
                    <!------------------->
                    <div class="col-lg-12 form-group mt-2">
                        <h5 class="font-weight-bold text-primary text-uppercase">Antecedentes Familiares</h5>
                        <hr>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Diabetes <span class="text-danger">*</span></label>
                        <select name="diabetes_familiar" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['diabetes_familiar']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Cancer <span class="text-danger">*</span></label>
                        <select name="cancer_familiar" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['cancer_familiar']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Hipertension <span class="text-danger">*</span></label>
                        <select name="hipertension" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['hipertension']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Enfermedad Cardiovascular <span class="text-danger">*</span></label>
                        <select name="enfermedad_cardiovascular" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['enfermedad_cardiovascular']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">¿El estudiante ha sido diagnosticado con covid-19? <span class="text-danger">*</span></label>
                        <select name="covid" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['covid']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Etnia</label>
                        <input type="text" name="etnia" class="form-control" value="<?=$datos_clinicos['etnia']?>">
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Inclusion <span class="text-danger">*</span></label>
                        <select name="inclusion" class="form-control" required>
                            <?php
                            for ($i = 1; $i < 3; $i++) {
                                $valor  = ($i == 1) ? 'Si' : 'No';
                                $select = ($i == $datos_clinicos['inclusion']) ? 'selected' : '';
                                ?>
                                <option value="<?=$i?>" <?=$select?>><?=$valor?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label class="font-weight-bold">Otros</label>
                        <input type="text" class="form-control" name="otros_familiar" value="<?=$datos_clinicos['otros_familiar']?>">
                    </div>
                    <div class="col-lg-12 form-group mt-2">
                        <a href="<?=BASE_URL?>imprimir/matricula/contratoPrev?estudiante=<?=base64_encode($id_estudiante)?>" target="_blank" class="btn btn-info btn-sm float-left <?=$ver_contrato_prev?>" data-tooltip="tooltip" title="Previsualizar Contrato" data-placement="bottom">
                            <i class="fas fa-file-contract"></i>
                            &nbsp;
                            Previsualizar Contrato
                        </a>
                        <button class="btn btn-primary btn-sm float-right" type="submit">
                            <i class="fa fa-sync"></i>
                            &nbsp;
                            Actualizar y firmar contrato
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'estudiantes' . DS . 'documentos.php';

if (isset($_POST['id_estudiante'])) {
    $instancia_matricula->guardarInformacionEstudianteControl();
}

if (isset($_POST['id_estudiante_documentos'])) {
    $instancia_matricula->subirDocumentosEstudianteControl();
}