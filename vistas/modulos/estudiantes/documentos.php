<div class="modal fade" id="subir_documentos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-primary" id="exampleModalLabel">Subir Documentos</h5>
      </div>
      <div class="modal-body">
        <form method="POST" enctype="multipart/form-data">
          <input type="hidden" name="id_estudiante_documentos" value="<?=$id_estudiante?>">
          <input type="hidden" name="id_log" value="<?=$id_log?>">
          <input type="hidden" name="url" value="<?=$url?>">
          <div class="row p-2">
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Fotocopia del documento (Estudiante) <span class="text-danger">*</span></label>
              <div class="custom-file pmd-custom-file-filled">
                <input type="file" class="custom-file-input file_input" id="documento_est" name="documento_est" required accept=".png, .jpg, .jpeg, .pdf">
                <label class="custom-file-label file_label_documento_est" for="customfilledFile"></label>
              </div>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Certificado medico <span class="text-danger">*</span></label>
              <div class="custom-file pmd-custom-file-filled">
                <input type="file" class="custom-file-input file_input" id="certificado" name="certificado" required accept=".png, .jpg, .jpeg, .pdf">
                <label class="custom-file-label file_label_certificado" for="customfilledFile"></label>
              </div>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Fotocopia del documento (Responsable Financiero) <span class="text-danger">*</span></label>
              <div class="custom-file pmd-custom-file-filled">
                <input type="file" class="custom-file-input file_input" id="documento_acu" name="documento_acudiente" required accept=".png, .jpg, .jpeg, .pdf">
                <label class="custom-file-label file_label_documento_acu" for="customfilledFile"></label>
              </div>
            </div>
            <div class="col-lg-12 form-group mt-2 text-right">
              <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
                <i class="fa fa-times"></i>
                &nbsp;
                Cancelar
              </button>
              <button class="btn btn-primary btn-sm" type="submit">
                <i class="fas fa-file-upload"></i>
                &nbsp;
                Subir
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
