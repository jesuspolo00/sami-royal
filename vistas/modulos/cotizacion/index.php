<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'solicitud' . DS . 'ControlSolicitud.php';

$instancia = ControlSolicitud::singleton_solicitud();

if (isset($_POST['buscar'])) {
	$datos            = array('fecha' => $_POST['fecha'], 'buscar' => $_POST['buscar']);
	$datos_cotizacion = $instancia->buscarCotizacionesControl($datos);
} else {
	$datos_cotizacion = $instancia->mostrarCotizacionesControl();
}

$permisos = $instancia_permiso->permisosUsuarioControl(47, $perfil_log);
if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						<a href="<?=BASE_URL?>compras/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-primary"></i>
						</a>
						&nbsp;
						Cotizaciones Sistemas
					</h4>
					<div class="btn-group">
						<button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#agregar_cot">
							<i class="fa fa-plus"></i>
							&nbsp;
							Agregar Cotizacion
						</button>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
							</div>
							<div class="col-lg-4 form-group">
								<input type="date" name="fecha" class="form-control" data-tooltip="tooltip" title="Fecha" data-placement="top">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group">
									<input type="text" class="form-control filtro" placeholder="Buscar" aria-describedby="basic-addon2" name="buscar"data-tooltip="tooltip" data-trigger="focus" data-placement="top" title="Presione ENTER para buscar">
									<div class="input-group-append">
										<button class="btn btn-primary btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No. Cotizacion</th>
									<th scope="col">Concepto</th>
									<th scope="col">Fecha cotizacion</th>
									<th scope="col">Observacilon</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_cotizacion as $cotizacion) {
									$id_cotizacion = $cotizacion['id'];
									$concepto      = $cotizacion['concepto'];
									$fecha         = $cotizacion['fecha'];
									$observacion   = $cotizacion['observacion'];

									$ver_subir_orden = (empty($cotizacion['orden_compra'])) ? '' : 'd-none';
									$ver_orden       = (!empty($cotizacion['orden_compra'])) ? '' : 'd-none';

									$ver_subir   = (!empty($cotizacion['orden_compra']) && empty($cotizacion['factura'])) ? '' : 'd-none';
									$ver_factura = (!empty($cotizacion['factura'])) ? '' : 'd-none';

									?>
									<tr class="text-center">
										<td><?=$id_cotizacion?></td>
										<td><?=$concepto?></td>
										<td><?=$fecha?></td>
										<td><?=$observacion?></td>
										<td>
											<div class="btn-group">
												<a href="<?=PUBLIC_PATH?>upload/<?=$cotizacion['cotizacion']?>" class="btn btn-info btn-sm" target="_blank" data-tooltip="tooltip" title="Ver Cotizacion" data-placement="bottom">
													<i class="fas fa-money-check-alt"></i>
												</a>
												<a href="<?=PUBLIC_PATH?>upload/<?=$cotizacion['orden_compra']?>" class="btn btn-dark btn-sm <?=$ver_orden?>" target="_blank" data-tooltip="tooltip" title="Ver Orden de Compra" data-placement="bottom">
													<i class="fas fa-money-check"></i>
												</a>
												<a href="<?=PUBLIC_PATH?>upload/<?=$cotizacion['factura']?>" class="btn btn-success btn-sm <?=$ver_factura?>" target="_blank" data-tooltip="tooltip" title="Ver Factura" data-placement="bottom">
													<i class="fas fa-receipt"></i>
												</a>
												<button class="btn btn-secondary btn-sm <?=$ver_subir?>" type="button" data-toggle="modal" data-target="#subir<?=$id_cotizacion?>" data-tooltip="tooltip" title="Subir Factura" data-placement="bottom">
													<i class="fa fa-upload"></i>
												</button>
												<button class="btn btn-secondary btn-sm <?=$ver_subir_orden?>" type="button" data-toggle="modal" data-target="#orden<?=$id_cotizacion?>" data-tooltip="tooltip" title="Subir Orden de compra" data-placement="bottom">
													<i class="fa fa-upload"></i>
												</button>
											</div>
										</td>
									</tr>

									<div class="modal fade" id="subir<?=$id_cotizacion?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title font-weight-bold text-primary" id="exampleModalLabel">Subir Factura Cotizacion No. <?=$id_cotizacion?></h5>
												</div>
												<div class="modal-body">
													<form method="POST" enctype="multipart/form-data">
														<input type="hidden" name="id_log" value="<?=$id_log?>">
														<input type="hidden" name="id_cotizacion" value="<?=$id_cotizacion?>">
														<div class="rows p-2">
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Factura <span class="text-danger">*</span></label>
																<div class="custom-file pmd-custom-file-filled">
																	<input type="file" class="custom-file-input file_input" id="<?=$id_cotizacion?>" name="factura" required accept=".png, .jpg, .jpeg, .pdf">
																	<label class="custom-file-label file_label_<?=$id_cotizacion?>" for="customfilledFile"></label>
																</div>
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Observacion</label>
																<textarea class="form-control" rows="5" name="observacion"><?=$observacion?></textarea>
															</div>
															<div class="col-lg-12 form-group mt-2 text-right">
																<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
																	<i class="fa fa-times"></i>
																	&nbsp;
																	Cancelar
																</button>
																<button class="btn btn-primary btn-sm" type="submit">
																	<i class="fa fa-upload"></i>
																	&nbsp;
																	Subir
																</button>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>


									<div class="modal fade" id="orden<?=$id_cotizacion?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title font-weight-bold text-primary" id="exampleModalLabel">Subir Orden de Compra Cotizacion No. <?=$id_cotizacion?></h5>
												</div>
												<div class="modal-body">
													<form method="POST" enctype="multipart/form-data">
														<input type="hidden" name="id_log" value="<?=$id_log?>">
														<input type="hidden" name="id_cotizacion_compra" value="<?=$id_cotizacion?>">
														<div class="rows p-2">
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Orden de compra <span class="text-danger">*</span></label>
																<!-- <input type="file" class="form-control" name="orden" required accept=".png, .jpg, .jpeg, .pdf"> -->
																<div class="custom-file pmd-custom-file-filled">
																	<input type="file" class="custom-file-input file_input" id="<?=$id_cotizacion?>" name="orden" required accept=".png, .jpg, .jpeg, .pdf">
																	<label class="custom-file-label file_label_<?=$id_cotizacion?>" for="customfilledFile"></label>
																</div>
															</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Observacion</label>
																<textarea class="form-control" rows="5" name="observacion"><?=$observacion?></textarea>
															</div>
															<div class="col-lg-12 form-group mt-2 text-right">
																<button class="btn btn-danger btn-sm" type="reset" data-dismiss="modal">
																	<i class="fa fa-times"></i>
																	&nbsp;
																	Cancelar
																</button>
																<button class="btn btn-primary btn-sm" type="submit">
																	<i class="fa fa-upload"></i>
																	&nbsp;
																	Subir
																</button>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>

								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'cotizacion' . DS . 'agregarCotizacion.php';

if (isset($_POST['concepto'])) {
	$instancia->subirCotizacionControl();
}

if (isset($_POST['id_cotizacion'])) {
	$instancia->subirFacturaControl();
}

if (isset($_POST['id_cotizacion_compra'])) {
	$instancia->subirOrdenControl();
}
?>