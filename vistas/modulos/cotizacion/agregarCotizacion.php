<div class="modal fade" id="agregar_cot" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-primary" id="exampleModalLabel">Agregar Cotizacion</h5>
      </div>
      <div class="modal-body">
        <form method="POST" enctype="multipart/form-data">
          <input type="hidden" name="id_log" value="<?=$id_log?>">
          <div class="row p-2">
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Concepto <span class="text-danger">*</span></label>
              <input type="text" name="concepto" class="form-control" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Fecha cotizacion <span class="text-danger">*</span></label>
              <input type="date" name="fecha" class="form-control" required>
            </div>
            <div class="col-lg-12 form-group">
              <label class="font-weight-bold">Cotizacion a subir <span class="text-danger">*</span></label>
              <!-- <input type="file" class="file" name="cotizacion" required accept=".png, .jpg, .jpeg, .pdf"> -->
              <div class="custom-file pmd-custom-file-filled">
                <input type="file" class="custom-file-input file_input" name="cotizacion" id="" required accept=".png, .jpg, .jpeg, .pdf">
                <label class="custom-file-label file_label" for="customfilledFile"></label>
              </div>
            </div>
            <div class="col-lg-12 form-group">
              <label class="font-weight-bold">Observacion</label>
              <textarea class="form-control" rows="5" name="observacion"></textarea>
            </div>
            <div class="col-lg-12 form-group mt-2 text-right">
              <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
                <i class="fa fa-times"></i>
                &nbsp;
                Cancelar
              </button>
              <button class="btn btn-primary btn-sm" type="submit">
                <i class="fa fa-save"></i>
                &nbsp;
                Guardar
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
