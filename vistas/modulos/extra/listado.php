<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'extra' . DS . 'ControlExtra.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia          = ControlExtra::singleton_extra();
$instancia_usuarios = ControlUsuarios::singleton_usuarios();

$datos_usuarios = $instancia_usuarios->mostrarTodosUsuariosControl();
$datos_extras   = $instancia->mostrarTodosExtraControl();

if (isset($_POST['buscar'])) {
    $datos       = array('buscar' => $_POST['buscar'], 'tipo' => $_POST['tipo'], 'actividad' => $_POST['actividad']);
    $datos_extra = $instancia->buscarDatosRegistrosExtraControl($datos);
} else {
    $datos_extra = $instancia->mostrarDatosRegistrosExtraLimiteControl();
}

$permisos = $instancia_permiso->permisosUsuarioControl(39, $perfil_log);
if (!$permisos) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?=BASE_URL?>inicio" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Listado de inscripci&oacute;n
                    </h4>
                    <div class="btn-group">
                        <a href="<?=BASE_URL?>imprimir/extra/listadoCompleto" target="_blank" class="btn btn-success btn-sm">
                            <i class="fa fa-file-excel"></i>
                            &nbsp;
                            Descargar Listado Completo
                        </a>
                        <a href="<?=BASE_URL?>extra/index" class="btn btn-secondary btn-sm">
                            <i class="fa fa-stopwatch"></i>
                            &nbsp;
                            Extracurriculares / Clubes
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <div class="row">
                            <div class="col-lg-4 form-group">
                                <select name="tipo" class="form-control" data-tooltip="tooltip" title="Tipo" data-placement="top">
                                    <option value="" selected>Seleccione un tipo...</option>
                                    <option value="1">Extracurricular</option>
                                    <option value="2">Club</option>
                                </select>
                            </div>
                            <div class="col-lg-4 form-group">
                                <select name="actividad" class="form-control actividad">
                                    <option value="" selected>Seleccione una actividad...</option>
                                    <?php
                                    foreach ($datos_extras as $extra) {
                                        $id_extra  = $extra['id'];
                                        $nom_extra = $extra['actividad'];
                                        $grado     = $extra['grado'];

                                        if ($extra['activo'] == 1) {
                                            ?>
                                            <option value="<?=$id_extra?>"><?=$nom_extra?> (<?=$grado?>)</option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-4 form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control filtro" placeholder="Buscar" aria-describedby="basic-addon2" name="buscar"data-tooltip="tooltip" data-trigger="focus" data-placement="top" title="Presione ENTER para buscar">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary btn-sm" type="submit">
                                            <i class="fa fa-search"></i>
                                            &nbsp;
                                            Buscar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <?php
                    if (isset($_POST['buscar'])) {
                        ?>
                        <div class="row">
                            <div class="col-lg-12 form-group mt-2 text-right">
                                <a href="<?=BASE_URL?>imprimir/extra/listadoFiltro?buscar=<?=$_POST['buscar']?>&tipo=<?=$_POST['tipo']?>&actividad=<?=$_POST['actividad']?>" target="_blank" class="btn btn-success btn-sm">
                                 <i class="fa fa-file-excel"></i>
                                 &nbsp;
                                 Descargar Filtro
                             </a>
                         </div>
                     </div>
                 <?php }?>
                 <div class="table-responsive mt-2">
                    <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                        <thead>
                            <tr class="text-center font-weight-bold">
                                <th scope="col">Tipo</th>
                                <th scope="col">Actividad</th>
                                <th scope="col">Grado Actividad</th>
                                <th scope="col">Dia Actividad</th>
                                <th scope="col">Estudiante</th>
                                <th scope="col">Nivel</th>
                                <th scope="col">Curso</th>
                            </tr>
                        </thead>
                        <tbody class="buscar">
                            <?php
                            foreach ($datos_extra as $extra) {
                                $id_extra    = $extra['id'];
                                $tipo        = ($extra['tipo'] == 1) ? 'Extracurricular' : 'Club';
                                $actividad   = $extra['actividad'];
                                $nom_usuario = $extra['nom_usuario'];
                                $nom_nivel   = $extra['nom_nivel'];
                                $nom_curso   = $extra['nom_curso'];
                                $grado       = $extra['grado'];
                                $dia         = $extra['dias'];
                                ?>
                                <tr class="text-center">
                                    <td><?=$tipo?></td>
                                    <td><?=$actividad?></td>
                                    <td><?=$grado?></td>
                                    <td><?=$dia?></td>
                                    <td><?=$nom_usuario?></td>
                                    <td><?=$nom_nivel?></td>
                                    <td><?=$nom_curso?></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>