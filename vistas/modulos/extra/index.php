<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'extra' . DS . 'ControlExtra.php';

$instancia = ControlExtra::singleton_extra();

$datos_extra = $instancia->mostrarTodosExtraControl();

$permisos = $instancia_permiso->permisosUsuarioControl(39, $perfil_log);
if (!$permisos) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?=BASE_URL?>extra/listado" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Extracurriculares / Clubes
                    </h4>
                    <div class="btn-group">
                        <a href="<?=BASE_URL?>extra/listado" class="btn btn-info btn-sm">
                            <i class="fa fa-list-ul"></i>
                            &nbsp;
                            Listado Inscripci&oacute;n
                        </a>
                        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#agregar_extra">
                            <i class="fa fa-plus"></i>
                            &nbsp;
                            Agregar Extra
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 form-inline">
                        </div>
                        <div class="col-lg-4 form-group">
                            <div class="input-group">
                                <input type="text" class="form-control filtro" placeholder="Buscar" aria-describedby="basic-addon2" name="buscar"data-tooltip="tooltip" data-trigger="focus" data-placement="top" title="Presione ENTER para buscar">
                                <div class="input-group-append">
                                    <button class="btn btn-primary btn-sm" type="submit">
                                        <i class="fa fa-search"></i>
                                        &nbsp;
                                        Buscar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Actividad</th>
                                    <th scope="col">Grados</th>
                                    <th scope="col">Cupos Disponibles</th>
                                    <th scope="col">Docente a cargo</th>
                                    <th scope="col">Dias</th>
                                    <th scope="col">Horas</th>
                                </tr>
                            </thead>
                            <tbody class="buscar">
                                <?php
                                foreach ($datos_extra as $extra) {
                                    $id_extra    = $extra['id'];
                                    $tipo        = ($extra['tipo'] == 1) ? 'Extracurricular' : 'Club';
                                    $actividad   = $extra['actividad'];
                                    $grado       = $extra['grado'];
                                    $cupo        = $extra['cupo'];
                                    $docente     = $extra['docente'];
                                    $dias        = $extra['dias'];
                                    $hora_inicio = date('h:i', strtotime($extra['hora_inicio']));
                                    $hora_fin    = date('h:i A', strtotime($extra['hora_fin']));
                                    ?>
                                    <tr class="text-center">
                                        <td><?=$tipo?></td>
                                        <td><?=$actividad?></td>
                                        <td><?=$grado?></td>
                                        <td><?=$cupo?></td>
                                        <td><?=$docente?></td>
                                        <td><?=$dias?></td>
                                        <td><?=$hora_inicio . ' - ' . $hora_fin?></td>
                                    </tr>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
require_once VISTA_PATH . 'modulos' . DS . 'extra' . DS . 'agregarExtra.php';

if (isset($_POST['id_log'])) {
    $instancia->agregarExtraControl();
}
?>