<div class="modal fade" id="agregar_extra" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-primary">Agregar Extra</h5>
      </div>
      <div class="modal-body">
        <form method="POST">
          <input type="hidden" name="id_log" value="<?=$id_log?>">
          <div class="row p-2">
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Tipo <span class="text-danger">*</span></label>
              <select name="tipo" class="form-control" required>
                <option value="" selected>Seleccione una opcion...</option>
                <option value="1">Extracurricular</option>
                <option value="2">Club</option>
              </select>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Actividad <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="actividad" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Grados <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="grado" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Cupo <span class="text-danger">*</span></label>
              <input type="text" class="form-control numeros" name="cupo" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Docente a cargo </label>
              <input type="text" class="form-control" name="docente">
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Dias <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="dias" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Hora Inicio <span class="text-danger">*</span></label>
              <input type="time" class="form-control" name="hora_inicio" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Hora Fin <span class="text-danger">*</span></label>
              <input type="time" class="form-control" name="hora_fin" required>
            </div>
            <div class="col-lg-12 form-group mt-2 text-right">
              <button class="btn btn-danger btn-sm" data-dismiss="modal" type="button">
                <i class="fa fa-times"></i>
                &nbsp;
                Cancelar
              </button>
              <button class="btn btn-primary btn-sm" type="submit">
                <i class="fa fa-save"></i>
                &nbsp;
                Guardar
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
