<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once VISTA_PATH . 'cabeza.php';
require_once CONTROL_PATH . 'extra' . DS . 'ControlExtra.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia          = ControlExtra::singleton_extra();
$instancia_usuarios = ControlUsuarios::singleton_usuarios();

if (isset($_GET['extra'])) {

	$id_extra = base64_decode($_GET['extra']);

	$datos_extra    = $instancia->informacionExtraControl($id_extra);
	$datos_usuarios = $instancia_usuarios->mostrarTodosUsuariosControl();

	$tipo        = ($datos_extra['tipo'] == 1) ? 'Extracurricular' : 'Club';
	$hora_inicio = date('h:i', strtotime($datos_extra['hora_inicio']));
	$hora_fin    = date('h:i A', strtotime($datos_extra['hora_fin']));

	?>
	<div class="container-fluid">
		<div class="row mt-2">
			<div class="col-lg-12">
				<div class="card shadow-sm mb-4">
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-primary">
							<a href="<?=BASE_URL?>extra/extra" class="text-decoration-none text-primary">
								<i class="fa fa-arrow-left"></i>
							</a>
							&nbsp;
							<?=$tipo?> - <?=$datos_extra['actividad']?>
						</h4>
					</div>
					<div class="card-body">
						<form method="POST">
							<input type="hidden" name="id_extra" value="<?=$id_extra?>">
							<div class="row p-2">
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Actividad <span class="text-danger"></span></label>
									<input type="text" class="form-control" value="<?=$datos_extra['actividad']?>" disabled>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Grado <span class="text-danger"></span></label>
									<input type="text" class="form-control" value="<?=$datos_extra['grado']?>" disabled>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Docente a cargo <span class="text-danger"></span></label>
									<input type="text" class="form-control" value="<?=$datos_extra['docente']?>" disabled>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Dias <span class="text-danger"></span></label>
									<input type="text" class="form-control" value="<?=$datos_extra['dias']?>" disabled>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Horas <span class="text-danger"></span></label>
									<input type="text" class="form-control" value="<?=$hora_inicio . ' - ' . $hora_fin?>" disabled>
								</div>
								<div class="col-lg-12 form-group">
									<hr>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Estudiante <span class="text-danger">*</span></label>
									<select name="estudiante" class="form-control estudiante" required>
										<option value="" selected>Seleccione una opcion...</option>
										<?php
										foreach ($datos_usuarios as $usuario) {
											$id_user   = $usuario['id_user'];
											$nombre    = $usuario['nombre'] . ' ' . $usuario['apellido'];
											$documento = $usuario['documento'];

											if ($usuario['perfil'] == 16 && $usuario['estado'] == 'activo') {
												?>
												<option value="<?=$id_user?>"><?=$documento?> - <?=$nombre?></option>
												<?php
											}
										}
										?>
									</select>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Correo <span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="correo" disabled>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Nivel <span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="nivel" disabled>
								</div>
								<div class="col-lg-4 form-group">
									<label class="font-weight-bold">Curso <span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="curso" disabled>
								</div>
								<div class="col-lg-12 form-group mt-2 text-right">
									<button class="btn btn-primary btn-sm" type="submit">
										<i class="fa fa-save"></i>
										&nbsp;
										Registrar
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';

	if (isset($_POST['id_extra'])) {
		$instancia->registrarUsuarioExtraControl();
	}
}
?>
<script src="<?=PUBLIC_PATH?>js/extra/funcionesExtra.js"></script>