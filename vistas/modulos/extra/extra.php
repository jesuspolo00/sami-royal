<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once VISTA_PATH . 'cabeza.php';
require_once CONTROL_PATH . 'extra' . DS . 'ControlExtra.php';

$instancia = ControlExtra::singleton_extra();

$datos_extra = $instancia->mostrarTodosExtraControl();
?>
<div class="container-fluid">
	<div class="row mt-2">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-primary">
						Extracurriculares
					</h4>
				</div>
				<div class="card-body">
					<div class="row p-2 informacion" id="informacion">
						<?php
						$array_id = '';
						foreach ($datos_extra as $extra) {
							$id_extra  = $extra['id'];
							$actividad = $extra['actividad'];
							$grado     = $extra['grado'];
							$cupo      = $extra['cupo'] - $extra['cupo_disponible'];
							$tipo      = ($extra['tipo'] == 1) ? 'Extracurricular' : 'Club';

							$array_id .= $id_extra . ',';

							if ($cupo != 0) {
								?>
								<div class="col-lg-3 form-group extra_<?=$id_extra?>">
									<div class="card shadow-sm rounded mb-2">
										<a href="<?=BASE_URL?>extra/registro?extra=<?=base64_encode($id_extra)?>" class="enlace text-decoration-none">
											<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger text-white" id="cupo_<?=$id_extra?>">
												<?=$cupo?>
											</span>
											<div class="card-header">
												<h5 class="font-weight-bold text-center"><?=$tipo?></h5>
											</div>
											<div class="card-body text-center">
												<img src="https://media.istockphoto.com/vectors/extracurricular-activities-black-glyph-icon-vector-id1269946333?k=20&m=1269946333&s=170667a&w=0&h=LhcoingFJSPP1RPBseaFoPNfoNiAXJPhmwYktTzjE54=" class="card-img-top" alt="" style="width: 60%;">
											</div>
											<div class="card-footer">
												<h5 class="text-center font-weight-bold"><?=$actividad?> (<?=$grado?>)</h5>
											</div>
										</a>
									</div>
								</div>
								<?php
							}
						}
						?>
						<button class="btn btn-primary btn-sm d-none actividad" type="button" id="<?=$array_id?>"></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?=PUBLIC_PATH?>js/extra/funcionesExtra.js"></script>