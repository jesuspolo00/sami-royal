<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia        = ControlMatricula::singleton_matricula();
$instancia_perfil = ControlPerfil::singleton_perfil();

$permisos = $instancia_permiso->permisosUsuarioControl(75, $perfil_log);
if (!$permisos) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}

if (isset($_GET['estudiante'])) {
    $id_estudiante = base64_decode($_GET['estudiante']);

    $datos_estudiante = $instancia_perfil->mostrarDatosPerfilControl($id_estudiante);
    $datos_contratos  = $instancia->mostrarTodosContratosControl($id_estudiante);

    $foto          = (empty($datos_estudiante['imagen'])) ? $datos_estudiante['foto_carnet'] : $datos_estudiante['imagen'];
    $curso_proximo = (empty($datos_estudiante['curso_proximo'])) ? $datos_estudiante['curso_proximo_anio'] : $datos_estudiante['curso_proximo'];

    $url = ($_GET['enlace'] == 0) ? BASE_URL . 'matricula/contratos/index' : BASE_URL . 'estudiantes/index?id=' . base64_encode($id_log);
}
?>
<div class="col-lg-12">
    <div class="card shadow-sm mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h4 class="m-0 font-weight-bold text-primary">
                <a href="<?=$url?>" class="text-decoration-none">
                    <i class="fa fa-arrow-left text-primary"></i>
                </a>
                &nbsp;
                Historial de contratos
            </h4>
        </div>
        <div class="card-body">
            <div class="row p-2">
                <div class="col-lg-12 form-group">
                    <div class="row">
                        <div class="col-lg-4 form-group mt-4">
                            <div class="circular--portrait">
                                <img src="<?=PUBLIC_PATH?>upload/<?=$foto?>" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 form-group mt-2">
                            <div class="row">
                                <div class="col-lg-12 form-group">
                                    <label class="font-weight-bold">Numero de documento <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="<?=$datos_estudiante['documento']?>" disabled>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="<?=$datos_estudiante['nombre']?>" disabled>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label class="font-weight-bold">Apellido <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="<?=$datos_estudiante['apellido']?>" disabled>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label class="font-weight-bold">Correo <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="<?=$datos_estudiante['correo']?>" disabled>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label class="font-weight-bold">Telefono <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="<?=$datos_estudiante['telefono']?>" disabled>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label class="font-weight-bold">Curso <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="<?=$datos_estudiante['curso_actual']?>" disabled>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label class="font-weight-bold">Curso Proximo <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="<?=$curso_proximo?>" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive mt-2">
                <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-center font-weight-bold">
                            <th colspan="4">Historial de contratos</th>
                        </tr>
                        <tr class="text-center font-weight-bold">
                            <th scope="col">A&ntilde;o Escolar</th>
                            <th scope="col">Nivel Contrato</th>
                            <th scope="col">Curso</th>
                        </tr>
                    </thead>
                    <tbody class="buscar">
                        <?php
                        foreach ($datos_contratos as $contratos) {
                            $id_contrato = $contratos['id'];
                            $nivel       = $contratos['nom_nivel'];
                            $curso       = $contratos['nom_curso'];
                            ?>
                            <tr class="text-center">
                                <td><?=$contratos['anio']?></td>
                                <td><?=$nivel?></td>
                                <td><?=$curso?></td>
                                <td>
                                    <div class="btn-group" role="group">
                                        <a href="<?=BASE_URL?>imprimir/matricula/contratoEstudiante?contrato=<?=base64_encode($id_contrato)?>" target="_blank" class="btn btn-primary btn-sm" data-tooltip="tooltip" title="Ver Contrato" data-placement="bottom">
                                            <i class="fas fa-file-contract"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';