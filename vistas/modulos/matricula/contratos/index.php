<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia         = ControlMatricula::singleton_matricula();
$instancia_usuario = ControlUsuarios::singleton_usuarios();

$datos_curso = $instancia_usuario->mostrarCursosUsuarioControl();
$datos_nivel = $instancia_usuario->mostrarNivelesUsuarioControl();

if (isset($_POST['buscar'])) {
    $datos             = array('curso' => $_POST['curso'], 'nivel' => $_POST['nivel'], 'buscar' => $_POST['buscar']);
    $datos_estudiantes = $instancia_usuario->buscarEstudiantesControl($datos);

    $post_curso  = $_POST['curso'];
    $post_nivel  = $_POST['nivel'];
    $post_buscar = $_POST['buscar'];
} else {
    $datos_estudiantes = $instancia_usuario->mostrarEstudiantesControl();

    $post_curso  = '';
    $post_nivel  = '';
    $post_buscar = '';
}

$permisos = $instancia_permiso->permisosUsuarioControl(72, $perfil_log);
if (!$permisos) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}

?>
<div class="col-lg-12">
    <div class="card shadow-sm mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h4 class="m-0 font-weight-bold text-primary">
                <a href="<?=BASE_URL?>matricula/index" class="text-decoration-none">
                    <i class="fa fa-arrow-left text-primary"></i>
                </a>
                &nbsp;
                Contratos
            </h4>
            <div class="btn-group">
                <a class="btn btn-secondary btn-sm" href="<?=BASE_URL?>matricula/contratos/contrato">
                    <i class="fas fa-funnel-dollar"></i>
                    &nbsp;
                    Valores del Contrato
                </a>
            </div>
        </div>
        <div class="card-body">
            <form method="POST">
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <select name="nivel" class="form-control">
                            <option value="" selected>Seleccione un nivel...</option>
                            <?php
                            foreach ($datos_nivel as $nivel) {
                                $id_nivel  = $nivel['id'];
                                $nom_nivel = $nivel['nombre'];

                                $select_nivel = ($id_nivel == $post_nivel) ? 'selected' : '';
                                ?>
                                <option value="<?=$id_nivel?>" <?=$select_nivel?>><?=$nom_nivel?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <select name="curso" class="form-control">
                            <option value="" selected>Seleccione un curso...</option>
                            <?php
                            foreach ($datos_curso as $curso) {
                                $id_curso  = $curso['id'];
                                $nom_curso = $curso['nombre'];

                                $select_curso = ($id_curso == $post_curso) ? 'selected' : '';
                                ?>
                                <option value="<?=$id_curso?>" <?=$select_curso?>><?=$nom_curso?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <div class="input-group">
                            <input type="text" class="form-control filtro" placeholder="Buscar" aria-describedby="basic-addon2" name="buscar"data-tooltip="tooltip" data-trigger="focus" data-placement="top" title="Presione ENTER para buscar" value="<?=$post_buscar?>">
                            <div class="input-group-append">
                                <button class="btn btn-primary btn-sm" type="submit">
                                    <i class="fa fa-search"></i>
                                    &nbsp;
                                    Buscar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="table-responsive mt-2">
                <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-center font-weight-bold">
                            <th scope="col">Documento</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Nivel</th>
                            <th scope="col">Curso</th>
                            <th scope="col">Curso Proximo</th>
                        </tr>
                    </thead>
                    <tbody class="buscar">
                        <?php
                        foreach ($datos_estudiantes as $usuario) {
                            $id_user       = $usuario['id_user'];
                            $nombre        = $usuario['nombre'] . ' ' . $usuario['apellido'];
                            $documento     = $usuario['documento'];
                            $correo        = $usuario['correo'];
                            $telefono      = $usuario['telefono'];
                            $asignatura    = $usuario['asignatura'];
                            $user          = $usuario['user'];
                            $estado        = $usuario['estado'];
                            $id_perfil     = $usuario['perfil'];
                            $perfil        = $usuario['nom_perfil'];
                            $nom_curso     = $usuario['nom_curso'];
                            $nom_nivel     = $usuario['nom_nivel'];
                            $id_nivel      = $usuario['id_nivel'];
                            $curso_proximo = (empty($usuario['curso_proximo_est'])) ? $usuario['curso_proximo'] : $usuario['curso_proximo_est'];

                            $datos_contrato     = $instancia->datosUltimoContratoNivelControl($id_nivel);
                            $datos_contrato_est = $instancia->mostrarContratoGeneradoControl($id_user, date('Y'));

                            $ver_contrato = ($datos_contrato_est['estado'] == 1) ? '' : 'd-none';
                            $ver_boton    = ($datos_contrato_est['estado'] == 0 || $datos_contrato_est['estado'] == 2) ? '' : 'd-none';

                            if ($usuario['curso_proximo_user'] == $usuario['id_curso_proximo'] || $usuario['curso_proximo_user'] != $usuario['id_curso_proximo']) {
                                $id_curso      = $usuario['curso_proximo_user'];
                                $nom_curso     = $usuario['curso_proximo_est'];
                                $curso_proximo = '';
                            }

                            if ($usuario['id_curso'] == $usuario['curso_proximo_user'] || empty($usuario['curso_proximo_user'])) {
                                $id_curso      = $usuario['id_curso'];
                                $nom_curso     = $usuario['nom_curso'];
                                $curso_proximo = $usuario['curso_proximo'];
                            }

                            ?>
                            <tr class="text-center">
                                <td><?=$documento?></td>
                                <td class="text-uppercase"><?=$nombre?></td>
                                <td><?=$nom_nivel?></td>
                                <td><?=$nom_curso?></td>
                                <td><?=$curso_proximo?></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="<?=BASE_URL?>matricula/contratos/firmas?id=<?=base64_encode($id_user)?>" class="btn btn-secondary btn-sm <?=$ver_boton?>" data-tooltip="tooltip" title="Ver firmas acudientes" data-placement="bottom">
                                            <i class="fa fa-file-upload"></i>
                                        </a>
                                        <button class="btn btn-success btn-sm <?=$ver_boton?>" type="button" data-tooltip="tooltip" title="Descuentos" data-placement="bottom" data-toggle="modal" data-target="#descuento_<?=$id_user?>">
                                            <i class="fas fa-money-check-alt"></i>
                                        </button>
                                        <a href="<?=BASE_URL?>imprimir/matricula/contratoEstudiante?contrato=<?=base64_encode($datos_contrato_est['id'])?>" target="_blank" class="btn btn-primary btn-sm <?=$ver_contrato?>" data-tooltip="tooltip" title="Ultimo Contrato" data-placement="bottom">
                                            <i class="fas fa-file-contract"></i>
                                        </a>
                                        <a href="<?=BASE_URL?>matricula/historial/index?estudiante=<?=base64_encode($id_user)?>&enlace=0" class="btn btn-info btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Historial de contratos">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </div>
                                </td>

                                <div class="modal fade" id="descuento_<?=$id_user?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title font-weight-bold text-primary" id="exampleModalLabel">Descuentos del contrato</h5>
                                    </div>
                                    <div class="modal-body">
                                        <form method="POST">
                                            <input type="hidden" name="id_contrato" value="<?=$datos_contrato['id']?>">
                                            <input type="hidden" name="id_user" value="<?=$id_user?>" id="id_user">
                                            <input type="hidden" name="id_log" value="<?=$id_log?>" id="id_log">
                                            <div class="row p-2">
                                                <div class="col-lg-6 form-group">
                                                    <label class="font-weight-bold">Estudiante <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" disabled value="<?=$nombre?>">
                                                </div>
                                                <div class="col-lg-6 form-group">
                                                    <label class="font-weight-bold">A&ntilde;o del contrato a realizar descuento <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" disabled value="<?=$datos_contrato['anio']?>">
                                                </div>
                                                <div class="col-lg-6 form-group">
                                                    <label class="font-weight-bold">Pension del contrato a realizar descuento <span class="text-danger">*</span></label>
                                                    <div class="input-group mb-3">
                                                      <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1">$</span>
                                                    </div>
                                                    <input type="text" class="form-control pension_valor_<?=$id_user?>" disabled value="<?=number_format($datos_contrato['pension'], 0, ',', '.')?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                <label class="font-weight-bold">Matricula del contrato a realizar descuento <span class="text-danger">*</span></label>
                                                <div class="input-group mb-3">
                                                  <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">$</span>
                                                </div>
                                                <input type="text" class="form-control matricula_valor_<?=$id_user?>" disabled value="<?=number_format($datos_contrato['matricula'], 0, ',', '.')?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 form-group">
                                            <label class="font-weight-bold">Descuento de pension a realizar</label>
                                            <div class="input-group mb-3">
                                              <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">%</span>
                                            </div>
                                            <input type="text" class="form-control numeros" maxlength="2" minlength="1" name="descuento_pension" value="0">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 form-group">
                                        <label class="font-weight-bold">Descuento de matricula a realizar</label>
                                        <div class="input-group mb-3">
                                          <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">%</span>
                                        </div>
                                        <input type="text" class="form-control numeros" maxlength="2" minlength="1" name="descuento_matricula" value="0">
                                    </div>
                                </div>
                                <div class="col-lg-12 form-group mt-4 text-right">
                                    <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
                                        <i class="fa fa-times"></i>
                                        &nbsp;
                                        Cancelar
                                    </button>
                                    <button class="btn btn-primary btn-sm" type="submit">
                                        <i class="fa fa-save"></i>
                                        &nbsp;
                                        Guardar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }
    ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_contrato'])) {
    $instancia->guardarDescuentoContratoControl();
}