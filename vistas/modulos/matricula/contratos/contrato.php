<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
  $er    = '2';
  $error = base64_encode($er);
  $salir = new Session;
  $salir->iniciar();
  $salir->outsession();
  header('Location:../login?er=' . $error);
  exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia          = ControlMatricula::singleton_matricula();
$instancia_usuarios = ControlUsuarios::singleton_usuarios();

$datos_contrato = $instancia->mostrarContratosConfiguradosControl();
$datos_nivel    = $instancia_usuarios->mostrarNivelesUsuarioControl();

$permisos = $instancia_permiso->permisosUsuarioControl(72, $perfil_log);
if (!$permisos) {
  include_once VISTA_PATH . 'modulos' . DS . '403.php';
  exit();
}

?>
<div class="col-lg-12">
  <div class="card shadow-sm mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
      <h4 class="m-0 font-weight-bold text-primary">
        <a href="<?=BASE_URL?>matricula/contratos/index" class="text-decoration-none">
          <i class="fa fa-arrow-left text-primary"></i>
        </a>
        &nbsp;
        Valores de contrato
      </h4>
      <div class="btn-group">
        <a href="<?=BASE_URL?>imprimir/matricula/contrato" class="btn btn-secondary btn-sm" target="_blank">
          <i class="fa fa-file-pdf"></i>
          &nbsp;
          Pre-visualizar Contrato
        </a>
      </div>
    </div>
    <div class="card-body">
      <form method="POST">
        <input type="hidden" name="id_log" value="<?=$id_log?>">
        <div class="row p-2">
          <div class="col-lg-3 form-group">
            <label class="font-weight-bold">A&ntilde;o <span class="text-danger">*</span></label>
            <select name="anio" class="form-control">
              <option value="" selected>Seleccione una opcion...</option>
              <?php
              for ($i = date('Y'); $i <= 2050; $i++) {
                $nom_anio = ($i - 1) . ' - ' . $i;
                ?>
                <option value="<?=$nom_anio?>"><?=$nom_anio?></option>
                <?php
              }
              ?>
            </select>
          </div>
          <div class="col-lg-3 form-group">
            <label class="font-weight-bold">Nivel <span class="text-danger">*</span></label>
            <select name="nivel" class="form-control">
              <option value="" selected>Seleccione una opcion...</option>
              <?php
              foreach ($datos_nivel as $nivel) {
                $id_nivel  = $nivel['id'];
                $nom_nivel = $nivel['nombre'];
                ?>
                <option value="<?=$id_nivel?>"><?=$nom_nivel?></option>
              <?php }?>
            </select>
          </div>
          <div class="col-lg-3 form-group">
            <label class="font-weight-bold">Valor de pension <span class="text-danger">*</span></label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm">$</span>
              </div>
              <input type="text" class="form-control precio numeros" name="pension" required>
            </div>
          </div>
          <div class="col-lg-3 form-group">
            <label class="font-weight-bold">Valor de matricula <span class="text-danger">*</span></label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroup-sizing-sm">$</span>
              </div>
              <input type="text" class="form-control precio numeros" name="matricula" required>
            </div>
          </div>
          <div class="col-lg-12 form-group mt-4 text-right">
            <button class="btn btn-primary btn-sm" type="submit">
              <i class="fa fa-save"></i>
              &nbsp;
              Guardar
            </button>
          </div>
        </div>
      </form>
      <div class="table-responsive mt-2">
        <table class="table table-hover border table-sm" width="100%" cellspacing="0">
          <thead>
            <tr class="text-center font-weight-bold">
              <th scope="col">A&ntilde;o</th>
              <th scope="col">Nivel</th>
              <th scope="col">Pension</th>
              <th scope="col">Matricula</th>
            </tr>
          </thead>
          <tbody class="buscar">
            <?php
            foreach ($datos_contrato as $contrato) {
              $id_contrato = $contrato['id'];
              $anio        = $contrato['anio'];
              $nom_nivel   = $contrato['nom_nivel'];
              $pension     = number_format($contrato['pension'], 0, ',', '.');
              $matricula   = number_format($contrato['matricula'], 0, ',', '.');
              ?>
              <tr class="text-center">
                <td><?=$anio?></td>
                <td><?=$nom_nivel?></td>
                <td>$<?=$pension?></td>
                <td>$<?=$matricula?></td>
                <td>
                  <div class="btn-group">
                    <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#editar_<?=$id_contrato?>" data-tooltip="tooltip" title="Editar Contrato" data-placement="bottom">
                      <i class="fa fa-edit"></i>
                    </button>
                  </div>
                </td>
              </tr>

              <div class="modal fade" id="editar_<?=$id_contrato?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title font-weight-bold text-primary" id="exampleModalLabel">Editar Valores del contrato</h5>
                    </div>
                    <div class="modal-body">
                      <form method="POST">
                        <input type="hidden" name="id_contrato" value="<?=$id_contrato?>">
                        <input type="hidden" name="id_log" value="<?=$id_log?>">
                        <div class="row p-2">
                          <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">A&ntilde;o <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" disabled value="<?=$anio?>">
                          </div>
                          <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Nivel <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" disabled value="<?=$nom_nivel?>">
                          </div>
                          <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Valor de pension <span class="text-danger">*</span></label>
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-sm">$</span>
                              </div>
                              <input type="text" class="form-control precio numeros" name="pension_edit" value="<?=$pension?>" required>
                            </div>
                          </div>
                          <div class="col-lg-6 form-group">
                            <label class="font-weight-bold">Valor de matricula <span class="text-danger">*</span></label>
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-sm">$</span>
                              </div>
                              <input type="text" class="form-control precio numeros" name="matricula_edit" value="<?=$matricula?>" required>
                            </div>
                          </div>
                          <div class="col-lg-12 form-group mt-2 text-right">
                            <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
                              <i class="fa fa-times"></i>
                              &nbsp;
                              Cancelar
                            </button>
                            <button class="btn btn-primary btn-sm" type="submit">
                              <i class="fa fa-save"></i>
                              &nbsp;
                              Guardar
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

              <?php
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['pension'])) {
  $instancia->contratoMatriculaControl();
}

if (isset($_POST['pension_edit'])) {
  $instancia->editarContratoControlMatriculaControl();
}