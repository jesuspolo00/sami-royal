<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia         = ControlMatricula::singleton_matricula();
$instancia_usuario = ControlUsuarios::singleton_usuarios();

if (isset($_GET['id'])) {

    $id_estudiante = base64_decode($_GET['id']);

    $datos_estudiante   = $instancia->mostrarDatosEstudianteControl($id_estudiante);
    $datos_acudientes   = $instancia->mostrarDatosAcudienteControl($id_estudiante);
    $datos_contrato     = $instancia->datosUltimoContratoNivelControl($datos_estudiante['id_nivel']);
    $datos_documentos   = $instancia->mostrarDocumentosEstudianteControl(date('Y'), $id_estudiante);
    $datos_contrato_est = $instancia->mostrarContratoGeneradoControl($id_estudiante, date('Y'));

    $ver_documento = (!empty($datos_documentos['id'])) ? '' : 'd-none';
}

$permisos = $instancia_permiso->permisosUsuarioControl(72, $perfil_log);
if (!$permisos) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="col-lg-12">
    <div class="card shadow-sm mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h4 class="m-0 font-weight-bold text-primary">
                <a href="<?=BASE_URL?>matricula/contratos/index" class="text-decoration-none">
                    <i class="fa fa-arrow-left text-primary"></i>
                </a>
                &nbsp;
                Firmas acudientes - Estudiante (<?=$datos_estudiante['nombre'] . ' ' . $datos_estudiante['apellido']?>)
            </h4>
        </div>
        <div class="card-body">
            <div class="row p-2">
                <div class="col-lg-4 form-group">
                    <label class="font-weight-bold">Documento <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" value="<?=$datos_estudiante['documento']?>" disabled>
                </div>
                <div class="col-lg-4 form-group">
                    <label class="font-weight-bold">Nombre Completo <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" value="<?=$datos_estudiante['nombre'] . ' ' . $datos_estudiante['apellido']?>" disabled>
                </div>
                <div class="col-lg-4 form-group">
                    <label class="font-weight-bold">Curso Actual <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" value="<?=$datos_estudiante['nom_curso']?>" disabled>
                </div>
                <div class="col-lg-4 form-group">
                    <label class="font-weight-bold">Curso Proximo <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" value="<?=$datos_estudiante['curso_proximo']?>" disabled>
                </div>
            </div>
            <div class="table-responsive mt-2">
                <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                    <thead class="text-center font-weight-bold">
                        <tr>
                            <th>Documento</th>
                            <th>Nombre Completo</th>
                            <th>Correo</th>
                            <th>Firma</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        <?php
                        $cantidad_padres = 0;
                        $cantidad_firmas = 0;

                        foreach ($datos_acudientes as $acudientes) {

                            $id_acudiente  = $acudientes['id_user'];
                            $documento     = $acudientes['documento'];
                            $nom_acudiente = $acudientes['nombre'] . ' ' . $acudientes['apellido'];
                            $correo        = $acudientes['correo'];
                            $firma         = $acudientes['firma'];
                            $firma_estado  = $acudientes['firma_digital'];

                            $ver_aprobar = (empty($firma) || $firma_estado == 1) ? 'd-none' : '';

                            $span_aprobar = ($firma_estado == 2) ? '<span class="badge badge-danger">Denegada</span>' : '<span class="badge badge-success">Aprobada</span>';
                            $span_aprobar = ($firma_estado == 0) ? '<span class="badge badge-warning">Pendiente</span>' : $span_aprobar;

                            $ver_firma = (empty($firma)) ? '<span class="badge badge-danger">Falta subir</span>' : '<a target="_blank" href="' . PUBLIC_PATH . 'upload/' . $firma . '" class="btn btn-info btn-sm">
                            <i class="fa fa-eye"></i>
                            &nbsp;
                            Ver firma
                            </a>';

                            $cantidad_padres++;
                            $cantidad_firmas = ($firma_estado == 1) ? $cantidad_firmas + 1 : $cantidad_firmas + 0;

                            $responsable = ($acudientes['responsable'] == 1) ? '<span class="badge badge-success">Responsable Financiero</span>' : '';

                            ?>
                            <tr>
                                <td><?=$documento?></td>
                                <td><?=$nom_acudiente?></td>
                                <td><?=$correo?></td>
                                <td><?=$ver_firma?></td>
                                <td>
                                    <?=$responsable?>
                                </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-12 form-group mt-4">
                <h5 class="text-primary font-weight-bold text-center text-uppercase">Documentos del estudiante</h5>
                <hr>
                <div class="btn-group mt-2">
                    <a target="_blank" href="<?=PUBLIC_PATH?>upload/<?=$datos_documentos['documento_estudiante']?>" class="btn btn-info btn-sm <?=$ver_documento?>">
                        <i class="fa fa-eye"></i>
                        &nbsp;
                        Documento Estudiante
                    </a>
                    &nbsp;
                    <a target="_blank" href="<?=PUBLIC_PATH?>upload/<?=$datos_documentos['certificado']?>" class="btn btn-info btn-sm <?=$ver_documento?>">
                        <i class="fa fa-eye"></i>
                        &nbsp;
                        Certificado Medico
                    </a>
                    &nbsp;
                    <a target="_blank" href="<?=PUBLIC_PATH?>upload/<?=$datos_documentos['documento_acudiente']?>" class="btn btn-info btn-sm <?=$ver_documento?>">
                        <i class="fa fa-eye"></i>
                        &nbsp;
                        Documento Responsable Financiero
                    </a>
                </div>
            </div>
            <form method="POST">
                <input type="hidden" name="id_estudiante" value="<?=$id_estudiante?>">
                <input type="hidden" name="id_contrato" value="<?=$datos_contrato_est['id']?>">
                <input type="hidden" name="id_log" value="<?=$id_log?>">
                <div class="row">
                    <div class="col-lg-12 form-group text-right mt-2">
                        <button class="btn btn-danger btn-sm" type="button" data-toggle="modal" data-target="#denegar">
                            <i class="fa fa-times"></i>
                            &nbsp;
                            Rechazar/Notificar
                        </button>
                        <button class="btn btn-success btn-sm" type="submit">
                            <i class="fas fa-check"></i>
                            &nbsp;
                            Aprobar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="denegar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-primary" id="exampleModalLabel">Rechazar/Notificar</h5>
    </div>
    <div class="modal-body">
        <form method="POST">
            <input type="hidden" name="id_estudiante" value="<?=$id_estudiante?>">
            <input type="hidden" name="id_contrato_denegar" value="<?=$datos_contrato_est['id']?>">
            <input type="hidden" name="id_log" value="<?=$id_log?>">
            <div class="row p-2">
                <div class="col-lg-12 form-group">
                    <label class="font-weight-bold">Motivo del rechazo del contrato <span class="text-danger">*</span></label>
                    <textarea name="motivo" class="form-control" required rows="5"></textarea>
                </div>
                <div class="col-lg-12 form-group text-right">
                    <button class="btn btn-danger btn-sm" type="submit" data-dismiss="modal">
                        <i class="fa fa-times"></i>
                        &nbsp;
                        Cancelar
                    </button>
                    <button class="btn btn-primary btn-sm">
                        <i class="fas fa-paper-plane"></i>
                        &nbsp;
                        Enviar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_contrato'])) {
    $instancia->aprobarContratoEstudianteControl();
}

if (isset($_POST['id_contrato_denegar'])) {
    $instancia->denegarContratoEstudianteControl();
}