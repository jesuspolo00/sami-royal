<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia         = ControlMatricula::singleton_matricula();
$instancia_usuario = ControlUsuarios::singleton_usuarios();

$datos_curso = $instancia_usuario->mostrarCursosUsuarioControl();
$datos_nivel = $instancia_usuario->mostrarNivelesUsuarioControl();

if (isset($_POST['buscar'])) {

    $datos             = array('curso' => $_POST['curso'], 'nivel' => $_POST['nivel'], 'buscar' => $_POST['buscar']);
    $datos_estudiantes = $instancia_usuario->buscarEstudiantesControl($datos);

    $post_curso  = $_POST['curso'];
    $post_nivel  = $_POST['nivel'];
    $post_buscar = $_POST['buscar'];
} else {
    
    $datos_estudiantes = $instancia_usuario->mostrarEstudiantesControl();

    $post_curso  = '';
    $post_nivel  = '';
    $post_buscar = '';
}

$permisos = $instancia_permiso->permisosUsuarioControl(74, $perfil_log);
if (!$permisos) {
    include_once VISTA_PATH . 'modulos' . DS . '403.php';
    exit();
}

?>
<div class="col-lg-12">
    <div class="card shadow-sm mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h4 class="m-0 font-weight-bold text-primary">
                <a href="<?=BASE_URL?>matricula/index" class="text-decoration-none">
                    <i class="fa fa-arrow-left text-primary"></i>
                </a>
                &nbsp;
                Promover Estudiantes
            </h4>
            <div class="btn-group">
                <button class="btn btn-secondary btn-sm" type="button" data-toggle="modal" data-target="#promover">
                    <i class="fas fa-graduation-cap"></i>
                    &nbsp;
                    Promover Cursos Estudiantes
                </button>
            </div>
        </div>
        <div class="card-body">
            <form method="POST">
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <select name="nivel" class="form-control">
                            <option value="" selected>Seleccione un nivel...</option>
                            <?php
                            foreach ($datos_nivel as $nivel) {
                                $id_nivel  = $nivel['id'];
                                $nom_nivel = $nivel['nombre'];

                                $select_nivel = ($id_nivel == $post_nivel) ? 'selected' : '';
                                ?>
                                <option value="<?=$id_nivel?>" <?=$select_nivel?>><?=$nom_nivel?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <select name="curso" class="form-control">
                            <option value="" selected>Seleccione un curso...</option>
                            <?php
                            foreach ($datos_curso as $curso) {
                                $id_curso  = $curso['id'];
                                $nom_curso = $curso['nombre'];

                                $select_curso = ($id_curso == $post_curso) ? 'selected' : '';
                                ?>
                                <option value="<?=$id_curso?>" <?=$select_curso?>><?=$nom_curso?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="col-lg-4 form-group">
                        <div class="input-group">
                            <input type="text" class="form-control filtro" placeholder="Buscar" aria-describedby="basic-addon2" name="buscar"data-tooltip="tooltip" data-trigger="focus" data-placement="top" title="Presione ENTER para buscar" value="<?=$post_buscar?>">
                            <div class="input-group-append">
                                <button class="btn btn-primary btn-sm" type="submit">
                                    <i class="fa fa-search"></i>
                                    &nbsp;
                                    Buscar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="table-responsive mt-2">
                <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-center font-weight-bold">
                            <th scope="col">Documento</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Nivel</th>
                            <th scope="col">Curso</th>
                            <th scope="col">Curso Proximo</th>
                        </tr>
                    </thead>
                    <tbody class="buscar">
                        <?php
                        foreach ($datos_estudiantes as $usuario) {

                            $id_user       = $usuario['id_user'];
                            $nombre        = $usuario['nombre'] . ' ' . $usuario['apellido'];
                            $documento     = $usuario['documento'];
                            $correo        = $usuario['correo'];
                            $telefono      = $usuario['telefono'];
                            $asignatura    = $usuario['asignatura'];
                            $user          = $usuario['user'];
                            $estado        = $usuario['estado'];
                            $id_perfil     = $usuario['perfil'];
                            $perfil        = $usuario['nom_perfil'];
                            $nom_curso     = $usuario['nom_curso'];
                            $curso_proximo = $usuario['curso_proximo'];
                            $nom_nivel     = $usuario['nom_nivel'];
                            $id_nivel      = $usuario['id_nivel'];

                            if ($usuario['curso_proximo_user'] == $usuario['id_curso_proximo'] || $usuario['curso_proximo_user'] != $usuario['id_curso_proximo']) {
                                $id_curso      = $usuario['curso_proximo_user'];
                                $nom_curso     = $usuario['curso_proximo_est'];
                                $curso_proximo = '';
                            }

                            if ($usuario['id_curso'] == $usuario['curso_proximo_user'] || empty($usuario['curso_proximo_user'])) {
                                $id_curso      = $usuario['id_curso'];
                                $nom_curso     = $usuario['nom_curso'];
                                $curso_proximo = $usuario['curso_proximo'];
                            }

                            $ver_editar = ($usuario['curso_proximo_user'] == 0) ? 'd-none' : '';

                            ?>
                            <tr class="text-center">
                                <td><?=$documento?></td>
                                <td class="text-uppercase"><?=$nombre?></td>
                                <td><?=$nom_nivel?></td>
                                <td><?=$nom_curso?></td>
                                <td><?=$curso_proximo?></td>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-primary btn-sm <?=$ver_editar?>" type="button" data-tooltip="tooltip" data-placement="bottom" title="Editar Curso" data-toggle="modal" data-target="#editar_<?=$id_user?>">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>

                            <div class="modal fade" id="editar_<?=$id_user?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Actualizar Curso Estudiante</h5>
                                </div>
                                <div class="modal-body">
                                    <form method="POST">
                                        <input type="hidden" name="id_estudiante" value="<?=$id_user?>">
                                        <div class="row p-2">
                                            <div class="col-lg-6 form-group">
                                                <label class="font-weight-bold">Documento <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" disabled value="<?=$documento?>">
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                <label class="font-weight-bold">Nombre Completo <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" disabled value="<?=$nombre?>">
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                <label class="font-weight-bold">Nivel <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" disabled value="<?=$nom_nivel?>">
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                <label class="font-weight-bold">Curso Actual <span class="text-danger">*</span></label>
                                                <select name="curso" class="form-control" required>
                                                    <?php
                                                    foreach ($datos_curso as $curso) {
                                                        $id_curso_sel  = $curso['id'];
                                                        $nom_curso_sel = $curso['nombre'];

                                                        $select_curso = ($id_curso_sel == $id_curso) ? 'selected' : '';
                                                        ?>
                                                        <option value="<?=$id_curso_sel?>" <?=$select_curso?>><?=$nom_curso_sel?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            <div class="col-lg-12 form-group text-right mt-2">
                                                <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
                                                    <i class="fa fa-times"></i>
                                                    &nbsp;
                                                    Cancelar
                                                </button>
                                                <button class="btn btn-primary btn-sm" type="submit">
                                                    <i class="fa fa-save"></i>
                                                    &nbsp;
                                                    Guardar
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
</div>

<div class="modal fade" id="promover" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <form method="POST">
            <input type="hidden" name="id_log" value="<?=$id_log?>">
            <div class="row p-2">
                <div class="col-lg-12 form-group">
                    <h4 class="text-center">Promovera los cursos de todos los estudiantes del año activo.</h4>
                    <h6 class="text-center text-danger">Esta accion no se puede deshacer.</h6>
                </div>
                <div class="col-lg-12 form-group text-center mt-2">
                    <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
                        <i class="fa fa-times"></i>
                        &nbsp;
                        Cancelar
                    </button>
                    <button class="btn btn-primary btn-sm" type="submit">
                        <i class="fa fa-arrow-right"></i>
                        &nbsp;
                        Continuar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>


<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_log'])) {
    $instancia->promoverCursosTodosControl();
}

if (isset($_POST['id_estudiante'])) {
    $instancia->actualizarCursoProximoControl();
}