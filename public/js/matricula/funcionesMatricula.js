$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*------------------------------------------------*/
    $(".aprobar").click(function() {
        var id = $(this).attr('id');
        var estuduiante = $(this).attr('data-id');
        aprobarFirma(id, 1, estuduiante);
    });
    /*----------------------------------*/
    $(".denegar").click(function() {
        var id = $(this).attr('id');
        var estuduiante = $(this).attr('data-id');
        denegarFirma(id, 2, estuduiante);
    });
    /*----------------------------------*/
    function aprobarFirma(id, estado, estuduiante) {
        try {
            $.ajax({
                url: '../../vistas/ajax/matricula/aprobarFirma.php',
                method: "POST",
                data: {
                    'id': id,
                    'estado': estado
                },
                cache: false,
                dataType: 'json',
                success: function(r) {
                    if (r.mensaje == 'ok') {
                        ohSnap("Firma Aprobada!", {
                            color: "green",
                            "duration": "1000"
                        });
                        $(".aprobar_" + id).hide();
                        $(".denegar_" + id).hide();
                        setTimeout(recargarPagina(estuduiante), 1050);
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*----------------------------------*/
    function denegarFirma(id, estado, estuduiante) {
        try {
            $.ajax({
                url: '../../vistas/ajax/matricula/denegarFirma.php',
                method: "POST",
                data: {
                    'id': id,
                    'estado': estado
                },
                cache: false,
                dataType: 'json',
                success: function(r) {
                    if (r.mensaje == 'ok') {
                        ohSnap("Firma Denegada!", {
                            color: "red",
                            "duration": "1000"
                        });
                        $(".aprobar_" + id).hide();
                        $(".denegar_" + id).hide();
                        setTimeout(recargarPagina(estuduiante), 1050);
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina(id) {
        window.location.replace('firmas?id=' + $.base64.btoa(id));
    }
});