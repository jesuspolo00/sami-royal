$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*------------------------------------*/
    $(".eliminar_fotos_si").on(tipoEvento, function() {
        eliminarFotos();
    });
    /*-------------------------*/
    function eliminarFotos() {
        try {
            $.ajax({
                url: '../../vistas/ajax/carnet/eliminarFotos.php',
                method: 'POST',
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        ohSnap("Eliminado correctamente!", {
                            color: "green",
                            "duration": "1000"
                        });
                        setTimeout(recargarPagina, 1050);
                    }else{
                        ohSnap("Error!", {
                            color: "red",
                            "duration": "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        window.location.replace("index");
    }
});