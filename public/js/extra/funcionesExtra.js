$(document).ready(function() {
	/*----------------------*/
	$(".estudiante").change(function() {
		var id = $(this).val();
		if (id != '') {
			informacionUsuario(id);
		}
	});
	/*----------------------*/
	function informacionUsuario(id) {
		$.ajax({
			url: "../vistas/ajax/usuarios/usuario.php",
			method: "POST",
			data: {
				'id': id
			},
			dataType: "json",
			success: function(data) {
				var correo = (data.correo == '' || data.correo == '.') ? 'No registrado' : data.correo;
				$("#nivel").val(data.nom_nivel);
				$("#curso").val(data.nom_curso);
				$("#correo").val(correo);
			}
		});
	}
	/*----------------------*/
	function autoclick() {
		var id_extra;
		$(".actividad").click();
		id_extra = $(".actividad").attr('id');
		contarCupo(id_extra);
	}
	/*----------------------*/
	function contarCupo(id_extra) {
		$.ajax({
			url: "../vistas/ajax/extra/contar.php",
			method: "POST",
			data: {
				'id_extra': id_extra
			},
			dataType: "json",
			success: function(data) {
				var array = data;
				array.forEach(function(datos) {
					if (datos.cupo_ocupado == 0) {
						$(".extra_" + datos.id).css('display', 'none');
					} else {
						$(".extra_" + datos.id).css('display', 'block');
						$("#cupo_" + datos.id).text(datos.cupo_ocupado);
					}
				});
			}
		});
	}
	setInterval(function() {
		autoclick();
	}, 2000);
	/*----------------------*/
});