<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloInventario extends conexion
{

    public static function guardarInventarioModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT HIGH_PRIORITY INTO " . $tabla . "
        (descripcion,
            marca,
            modelo,
            precio,
            estado,
            activo,
            fecha_compra,
            observacion,
            id_user,
            id_area,
            id_categoria,
            user_log,
            id_super_empresa,
            codigo,
            fechareg)
        SELECT descripcion,
        marca,
        modelo,
        precio,
        estado,
        activo,
        fecha_compra,
        observacion,
        id_user,
        id_area,
        id_categoria,
        user_log,
        id_super_empresa,
        codigo,
        fechareg
        FROM inventario_temp it
        WHERE it.estado = 1 AND it.activo = 1 AND it.user_log = :il AND it.id_super_empresa = :ids;
        INSERT INTO hoja_vida (id_inventario, id_super_empresa, fecha_update) SELECT it.id, it.id_super_empresa, it.fechareg FROM inventario it WHERE it.estado = 1
        AND it.activo = 1 AND it.user_log = :il AND it.id_super_empresa = :ids AND it.id NOT IN(SELECT h.id_inventario FROM hoja_vida h) AND it.id_categoria = 1;
        INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, id_super_empresa, estado)
        SELECT it.id, it.id_user, it.id_area, it.user_log, it.id_super_empresa, it.estado FROM inventario it
        WHERE it.estado = 1 AND it.activo = 1  AND it.id_super_empresa = :ids AND it.user_log = :il
        AND it.id NOT IN(SELECT h.id_inventario FROM inventario_log h);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarEvidenciaModel($datos)
    {
        $tabla  = 'evidencias';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT HIGH_PRIORITY INTO " . $tabla . " SELECT * FROM evidencias_temp ev
        WHERE ev.id_log = :il AND ev.id_super_empresa = :ids;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarTemporalModel($datos)
    {
        $tabla  = 'inventario_temp';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0 WHERE user_log = :il AND id_super_empresa = :ids;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function eliminarTemporalModel($datos)
    {
        $tabla  = 'inventario_temp';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE user_log = :il AND id_super_empresa = :ids;
        DELETE FROM evidencias_temp WHERE id_log = :il AND id_super_empresa = :ids";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verificarCodigoModel($codigo)
    {
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE codigo FROM inventario WHERE codigo = :c;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':c', $codigo);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInventarioModel()
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        SQL_NO_CACHE iv.*,
        (SELECT
            e.nombre
            FROM
            estado e
            WHERE e.id IN (iv.estado)) AS estado_nombre,
        (SELECT
            CONCAT (u.nombre, ' ', u.apellido)
            FROM
            usuarios u
            WHERE u.id_user IN (iv.id_user)) AS usuario,
        COUNT(iv.id) AS cantidad,
        (SELECT
            a.nombre
            FROM
            areas a
            WHERE a.id IN (iv.id_area)) AS area_nom
        FROM
        inventario iv
        INNER JOIN usuarios u ON u.id_user = iv.id_user
        LEFT JOIN areas a ON a.id = iv.id_area
        WHERE
        iv.activo = 1
        AND iv.estado NOT IN (2, 6, 5)
        AND iv.confirmado = 1
        GROUP BY iv.descripcion,
        iv.id_area ORDER BY iv.id DESC LIMIT 30;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarInventarioModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        SQL_NO_CACHE iv.*,
        (SELECT
            e.nombre
            FROM
            estado e
            WHERE e.id IN (iv.estado)) AS estado_nombre,
        (SELECT
            CONCAT (u.nombre, ' ', u.apellido)
            FROM
            usuarios u
            WHERE u.id_user IN (iv.id_user)) AS usuario,
        COUNT(iv.id) AS cantidad,
        (SELECT
            a.nombre
            FROM
            areas a
            WHERE a.id IN (iv.id_area)) AS area_nom
        FROM
        inventario iv
        INNER JOIN usuarios u ON u.id_user = iv.id_user
        LEFT JOIN areas a ON a.id = iv.id_area
        WHERE
        CONCAT(u.nombre, ' ', u.apellido, ' ', a.nombre, ' ', iv.descripcion) LIKE '%" . $datos['articulo'] . "%'
        AND
        a.activo = 1
        AND
        iv.activo = 1
        AND iv.estado NOT IN (2, 6, 5)
        AND iv.confirmado = 1
        " . $datos['area'] . " " . $datos['usuario'] . "
        GROUP BY iv.descripcion,
        iv.id_area;
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function cantidadesInventarioModel($articulo)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        iv.descripcion,
        COUNT(iv.id) AS cantidad
        FROM
        inventario iv
        INNER JOIN usuarios u ON u.id_user = iv.id_user
        LEFT JOIN areas a ON a.id = iv.id_area
        WHERE
        iv.descripcion  like '" . $articulo . "%'
        AND
        a.activo = 1
        AND
        iv.activo = 1
        AND iv.estado NOT IN (2, 6, 5)
        AND iv.confirmado = 1;
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarCategoriasModel($super_empresa)
    {
        $tabla  = 'categoria';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE * FROM categoria c WHERE c.activo = 1 AND c.id_super_empresa = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $super_empresa);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarInventarioDetalleModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        iv.*,
        (SELECT e.nombre FROM estado e WHERE e.id IN(iv.estado)) AS estado_nombre,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(iv.id_user)) AS nom_user,
        (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) AS nom_area,
        COUNT(iv.id) as cantidad
        FROM inventario iv
        LEFT JOIN usuarios u ON u.id_user = iv.id_user
        LEFT JOIN areas a ON a.id = iv.id_area
        WHERE iv.activo = 1 AND a.activo = 1
        AND CONCAT(u.nombre, ' ', u.apellido, ' ', u.documento, ' ', a.nombre, ' ', iv.descripcion) LIKE '%" . $datos['articulo'] . "%' " . $datos['area'] . "" . $datos['usuario'] . " GROUP BY iv.descripcion, iv.estado, iv.id_area, iv.id_user order by iv.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarInventarioDescontinuadoDetalleModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        iv.*,
        (SELECT e.nombre FROM estado e WHERE e.id IN(iv.estado)) AS estado_nombre,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(iv.id_user)) AS nom_user,
        (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) AS nom_area
        FROM inventario iv
        LEFT JOIN usuarios u ON u.id_user = iv.id_user
        LEFT JOIN areas a ON a.id = iv.id_area
        WHERE iv.activo = 1 AND a.activo = 1 and iv.estado IN(5)
        AND (iv.descripcion LIKE '%aire acondicionado%' OR iv.descripcion LIKE '%todo en uno%' OR iv.descripcion LIKE '%impresora%' OR iv.descripcion LIKE '%video beam%')
        AND CONCAT(u.nombre, ' ', u.apellido, ' ', u.documento, ' ', a.nombre, ' ', iv.descripcion) LIKE '%" . $datos['articulo'] . "%' " . $datos['area'] . "" . $datos['usuario'] . " order by iv.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInventarioDetalleModel()
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        iv.*,
        (SELECT e.nombre FROM estado e WHERE e.id IN(iv.estado)) AS estado_nombre,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(iv.id_user)) AS nom_user,
        (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) AS nom_area,
        count(iv.id) as cantidad
        FROM inventario iv
        LEFT JOIN usuarios u ON u.id_user = iv.id_user
        LEFT JOIN areas a ON a.id = iv.id_area
        WHERE iv.activo = 1
        group by iv.descripcion, iv.estado
        order by iv.id DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInventarioDescontinuadoDetalleModel()
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        iv.*,
        (SELECT e.nombre FROM estado e WHERE e.id IN(iv.estado)) AS estado_nombre,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(iv.id_user)) AS nom_user,
        (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) AS nom_area
        FROM inventario iv
        LEFT JOIN usuarios u ON u.id_user = iv.id_user
        LEFT JOIN areas a ON a.id = iv.id_area
        WHERE iv.activo = 1
        AND (iv.descripcion LIKE '%aire acondicionado%' OR iv.descripcion LIKE '%todo en uno%' OR iv.descripcion LIKE '%impresora%' OR iv.descripcion LIKE '%video beam%')
        AND iv.estado IN(5)
        ORDER BY iv.id DESC; LIMIT 15;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosTemporalesModel($id_log, $super_empresa)
    {
        $tabla  = 'inventario_temp';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE t.*,
        COUNT(t.id) AS cantidad,
        (SELECT a.nombre FROM areas a WHERE a.id = t.id_area) AS area_nom,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(t.id_user)) AS usuario,
        (SELECT e.nombre FROM estado e WHERE e.id IN(t.estado)) AS estado
        FROM " . $tabla . " t WHERE t.estado = 1 AND t.activo = 0 AND t.user_log = :il AND t.id_super_empresa = 1
        GROUP BY t.descripcion;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':il', $id_log);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosCartaEntregaModel($usuario, $area)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE t.*,
        COUNT(t.id) AS cantidad,
        (SELECT a.nombre FROM areas a WHERE a.id = t.id_area) AS area_nom,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(t.id_user)) AS usuario,
        (SELECT e.nombre FROM estado e WHERE e.id IN(t.estado)) AS estado
        FROM " . $tabla . " t WHERE t.estado IN(1,3,8,9) AND t.activo = 1 " . $usuario . " " . $area . "
        GROUP BY t.descripcion;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarInventarioTempModel($datos)
    {
        $tabla  = 'inventario_temp';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (descripcion, marca, modelo, precio, estado, fecha_compra, id_user, id_area, user_log, id_super_empresa
            , codigo, id_categoria, fechareg) VALUES (:d,:mr,:md,:p,:e,:fc,:iu,:ia,:ul,:ids,:cd,:idc, '" . $datos['fecha_ingreso'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':d', $datos['descripcion']);
            $preparado->bindParam(':mr', $datos['marca']);
            $preparado->bindParam(':md', $datos['modelo']);
            $preparado->bindParam(':p', $datos['precio']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':fc', $datos['fecha_compra']);
            $preparado->bindParam(':iu', $datos['id_user']);
            $preparado->bindParam(':ia', $datos['id_area']);
            $preparado->bindParam(':ul', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['super_empresa']);
            $preparado->bindParam(':cd', $datos['codigo']);
            $preparado->bindParam(':idc', $datos['id_categoria']);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarEvidenciaTempModel($datos)
    {
        $tabla  = 'evidencias_temp';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT HIGH_PRIORITY INTO " . $tabla . " (nombre, id_inventario_temp, id_log, id_super_empresa)
            VALUES (:n, :idt, :il, :ids)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idt', $datos['id_inventario_temp']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function liberarArticuloModel($datos)
    {
        $tabla  = 'inventario_lib';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT HIGH_PRIORITY INTO " . $tabla . " (id_inventario, id_log, id_super_empresa)
            VALUES (:idv, :il, :ids);
            UPDATE inventario SET estado = :e, id_user = NULL WHERE id = :idv;
            INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, estado, id_super_empresa) VALUES (:idv,:idu,:ida,:il,:e,:ids);
            INSERT INTO reportes (id_inventario, estado, id_user, id_log, id_area) VALUES (:idv,:e,:idu,:il,:ida);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindValue(':e', 4);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function descontinuarArticuloModel($datos)
    {
        $tabla  = 'inventario_desc';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT HIGH_PRIORITY INTO " . $tabla . " (id_inventario, id_log)
            VALUES (:idv, :il);
            UPDATE inventario SET estado = :e, observacion = :ob WHERE id = :idv;
            INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, estado) VALUES (:idv,:idu,:ida,:il,:e);
            INSERT INTO reportes (id_inventario, observacion, estado, id_user, id_log, id_area, fechareg) VALUES (:idv,:ob,:e,:idu,:il,:ida, :fr);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':fr', $datos['fechareg']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mantenimientoArticuloModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = :e, observacion = :ob WHERE id = :idv;
            INSERT INTO reportes (id_inventario, observacion, estado, id_user, id_log, tipo_reporte, fechareg, id_area)
            VALUES  (:idv,:ob,:e,:idu,:idl,:tr,:fr,:ida);
            INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, estado, id_super_empresa) VALUES (:idv,:idu,:ida,:idl,:e,:ids);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':tr', $datos['tipo_reporte']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':fr', $datos['fechareg']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function reportarArticuloModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE inventario SET estado = :e, observacion = '" . $datos['observacion'] . "' WHERE descripcion = '" . $datos['nom_inventario_rep'] . "' AND id_area = :ida AND id_user = :idu AND estado NOT IN(2,4,5,6,9,10) ORDER BY id DESC LIMIT " . $datos['cantidad'] . ";";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function articulosReportadosModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        *
            FROM inventario
            WHERE descripcion = '" . $datos['nom_inventario_rep'] . "' AND estado = '" . $datos['estado'] . "' AND id_area = '" . $datos['id_area'] . "' AND id_user = '" . $datos['id_user'] . "' ORDER BY id DESC LIMIT " . $datos['cantidad'] . ";";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function insertarReporteModel($datos)
    {
        $tabla  = 'reportes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO reportes (id_inventario, observacion, estado, id_log, tipo_reporte, id_user, id_area, fechareg)
            VALUES(:idv,:ob,:e,:idl,:tr,:idu,:ida,:fr);

            INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, estado)
            VALUES (:idv,:idu,:ida,:idl,:e);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':tr', $datos['tipo_reporte']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':fr', $datos['fecha']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function trabajoCasaArticuloModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = :e, observacion = :ob WHERE id = :idv;
            INSERT INTO reportes (id_inventario, observacion, estado, id_log, tipo_reporte, id_user, id_area)
            VALUES(:idv,:ob,:e,:idl,:tr,:idu,:ida);
            INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, estado, id_super_empresa)
            VALUES (:idv,:idu,:ida,:idl,:e,:ids);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':tr', $datos['tipo_reporte']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':ids', $datos['super_empresa']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function removerTrabajoCasaArticuloModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = :e, observacion = :ob WHERE id = :idv;
            INSERT INTO reportes (id_inventario, observacion, estado, id_log, tipo_reporte, id_user, id_area)
            VALUES(:idv,:ob,:e,:idl,:tr,:idu,:ida);
            INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, estado, id_super_empresa)
            VALUES (:idv,:idu,:ida,:idl,:e,:ids);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':tr', $datos['tipo_reporte']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':ids', $datos['super_empresa']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function informacionReporteModel($id)
    {
        $tabla  = 'reportes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
            rp.id,
            rp.id_log,
            rp.id_inventario,
            (SELECT iv.descripcion FROM inventario iv WHERE iv.id IN(rp.id_inventario)) AS descripcion,
            (SELECT iv.marca FROM inventario iv WHERE iv.id IN(rp.id_inventario)) AS marca,
            (SELECT iv.modelo FROM inventario iv WHERE iv.id IN(rp.id_inventario)) AS modelo,
            (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(rp.id_user)) AS usuario,
            (SELECT a.nombre FROM areas a WHERE a.id IN(rp.id_area)) AS area,
            (SELECT iv.id_area FROM inventario iv WHERE iv.id IN(rp.id_inventario)) AS id_area,
            (SELECT e.nombre FROM estado e WHERE e.id IN(rp.estado)) AS estado,
            rp.observacion,
            rp.fechareg
            FROM " . $tabla . " rp WHERE rp.id_inventario = :idv ORDER BY rp.id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':idv', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarArticulosLiberadosModel($super_empresa)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE iv.*,
            (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) AS area,
            (SELECT e.nombre FROM estado e WHERE e.id IN(iv.estado)) AS nombre_estado
            FROM " . $tabla . " iv WHERE iv.estado = 4 AND iv.activo = 1 AND iv.id_super_empresa = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $super_empresa);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function reasignarArticuloModel($datos)
    {
        $tabla  = 'inventario_log';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT HIGH_PRIORITY INTO " . $tabla . " (id_inventario, id_user, id_area, id_log, id_super_empresa, estado)
            VALUES (:idv,:idu,:ida,:idl,:ids,:e);
            UPDATE inventario SET id_user = :idu, id_area = :ida, estado = :e, observacion = 'RE-ASIGNADO' WHERE id = :idv;
            INSERT INTO reportes (id_inventario, estado, id_user, id_log, id_resp, id_reporte, fecha_respuesta)
            VALUES (:idv,:e,:idu,:idl,:idr,:idp,:fr);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idv', $datos['id_inventario']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            $preparado->bindValue(':e', 7);
            $preparado->bindParam(':idr', $datos['id_log']);
            $preparado->bindParam(':idp', $datos['id_reporte']);
            $preparado->bindParam(':fr', $datos['fecha_respuesta']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarEquipoComputoModel()
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE iv.*,
            (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
            (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS nom_area,
            (SELECT h.frecuencia_mantenimiento FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia,
            (SELECT h.frecuencia_copia FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia_copia,
            IF((SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY r.fechareg DESC LIMIT 1) IS NULL, iv.fechareg,
                (SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY r.fechareg DESC LIMIT 1)) AS ultimo_mant
            FROM " . $tabla . " iv
            WHERE iv.id_categoria = 1 AND iv.estado NOT IN(5) ORDER BY iv.id DESC LIMIT 30;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarEquipoTodosComputoModel($fecha)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE iv.*,
            (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
            (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS nom_area,
            (SELECT h.frecuencia_mantenimiento FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia,
            (SELECT h.frecuencia_copia FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia_copia,
            IF((SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY r.fechareg DESC LIMIT 1) IS NULL, iv.fechareg,
                (SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY r.fechareg DESC LIMIT 1)) AS ultimo_mant
            FROM " . $tabla . " iv
            WHERE iv.id_categoria = 1 AND iv.estado IN(1,3) AND iv.fechareg <= '" . $fecha . "' ORDER BY iv.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarFechasMantenimientosModel()
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE iv.*,
            (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
            (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area,
            (SELECT h.frecuencia_mantenimiento FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia,
            (SELECT h.frecuencia_copia FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia_copia,
            IF((SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY r.fechareg DESC LIMIT 1) IS NULL, '',
                (SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY r.fechareg DESC LIMIT 1)) AS ultimo_mant
            FROM " . $tabla . " iv
            LEFT JOIN reportes rp ON rp.id_inventario = iv.id
            WHERE iv.id_categoria = 1 AND iv.estado NOT IN(5, 7, 2, 4, 6) AND iv.id_categoria = 1 AND iv.id_user NOT IN(0, '')
            AND iv.descripcion NOT IN('Aire acondicionado', 'Televisor', 'Pagina Web', 'HUELLERO') AND iv.confirmado = 1 GROUP BY YEAR(ultimo_mant), MONTH(ultimo_mant) ORDER BY iv.id, rp.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarFechasMantenimientosTodosModel()
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT r.* FROM reportes r
            WHERE r.id_inventario IN(SELECT iv.id FROM inventario iv WHERE iv.id_categoria = 1 AND iv.estado NOT IN(5, 7, 2, 4, 6) AND iv.id_categoria = 1 AND iv.id_user NOT IN(0, '')
                AND iv.descripcion NOT IN('Aire acondicionado', 'Televisor', 'Pagina Web', 'HUELLERO') AND iv.confirmado = 1) AND r.estado = 6
            GROUP BY YEAR(r.fechareg), MONTH(r.fechareg);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarFechasCopiasSeguridadModel()
    {
        $tabla  = 'copia_seguridad';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
            c.fecha
            FROM copia_seguridad c
            GROUP BY YEAR(c.fecha), MONTH(c.fecha);
            ORDER BY c.fecha ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosEquipoComputoModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE iv.*,
            CONCAT(u.nombre, ' ', u.apellido) AS usuario,
            ar.nombre AS nom_area,
            (SELECT h.frecuencia_mantenimiento FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia,
            (SELECT h.frecuencia_copia FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia_copia,
            IF((SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY r.fechareg DESC LIMIT 1) IS NULL, iv.fechareg,
                (SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY r.fechareg DESC LIMIT 1)) AS ultimo_mant
            FROM inventario iv
            LEFT JOIN usuarios u ON u.id_user = iv.id_user
            LEFT JOIN areas ar ON ar.id = iv.id_area
            WHERE CONCAT(iv.descripcion) LIKE '%" . $datos['buscar'] . "%'
            " . $datos['area'] . "
            " . $datos['usuario'] . "
            AND iv.id_categoria = 1 AND iv.estado NOT IN(5) ORDER BY iv.id DESC;
            ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosArticulosModel($super_empresa)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE iv.*,
            (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
            (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area
            FROM " . $tabla . " iv WHERE iv.id_super_empresa = :ids ORDER BY id DESC LIMIT 100";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':ids', $super_empresa);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosArticuloIdModel($id)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE iv.*,
            (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(iv.id_user)) AS usuario,
            (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) AS area,
            (SELECT ev.nombre FROM evidencias ev WHERE ev.id_inventario = iv.id) AS evidencia,
            (SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.id_reporte IS NULL ORDER BY r.id DESC LIMIT 1) AS fecha_reporte,
            (SELECT (SELECT re.fecha_respuesta FROM reportes re WHERE re.id_reporte = r.id AND re.tipo_reporte = 1 AND re.id_inventario = iv.id)
                FROM reportes r WHERE r.id_reporte IS NULL AND r.id_inventario = iv.id ORDER BY r.id DESC LIMIT 1) AS fecha_respuesta
            FROM " . $tabla . " iv
            WHERE iv.id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosAgrupadosArticulosModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
            iv.*,
            COUNT(iv.id) AS cantidad,
            a.nombre AS area,
            CONCAT(u.nombre, ' ', u.apellido) AS usuario,
            (SELECT e.nombre FROM estado e WHERE e.id = iv.estado) as nom_estado,
            (SELECT rp.fechareg FROM reportes rp WHERE rp.id_inventario = iv.id AND rp.estado = iv.estado ORDER BY rp.id DESC LIMIT 1) AS fecha_reporte
            FROM inventario iv
            LEFT JOIN areas a ON a.id = iv.id_area
            LEFT JOIN usuarios u ON u.id_user = iv.id_user
            WHERE iv.descripcion = '" . $datos['nom_inventario_rep'] . "'
            AND iv.id_area = " . $datos['id_area'] . "
            AND iv.id_user = " . $datos['id_user'] . "
            AND iv.estado = " . $datos['estado'] . "
            GROUP BY iv.descripcion ORDER BY iv.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function historialArticuloModel($id)
    {
        $tabla  = 'reportes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE *,
            r.observacion AS observacion_reporte,
            r.fechareg AS fecha_reporte,
            r.estado AS estado_reporte,
            r.id AS id_reportado,
            (SELECT CONCAT(u.nombre, ' ' , u.apellido) FROM usuarios u WHERE u.id_user = r.id_user) AS usuario,
            (SELECT e.nombre FROM estado e WHERE e.id = r.estado) AS estado_nombre,
            (SELECT a.nombre FROM areas a WHERE a.id = r.id_area) AS area
            FROM " . $tabla . " r
            LEFT JOIN inventario iv ON r.id_inventario = iv.id
            WHERE r.id_inventario = :id ORDER by r.fechareg DESC;
            ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarReporteLiberadoModel($id)
    {
        $tabla  = 'reportes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE * FROM " . $tabla . " WHERE id_inventario = :id AND estado = 4 ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function historialReportesModel($super_empresa)
    {
        $tabla  = 'reportes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE *,
            r.id as id_reporte_inicio,
            r.estado as estado_reporte,
            r.observacion AS observacion_reporte,
            r.fechareg AS fecha_reporte,
            (SELECT CONCAT(u.nombre, ' ' , u.apellido) FROM usuarios u WHERE u.id_user = r.id_user) AS usuario,
            (SELECT e.nombre FROM estado e WHERE e.id = r.estado) AS estado_nombre,
            (SELECT a.nombre FROM areas a WHERE a.id = r.id_area) AS area
            FROM " . $tabla . " r
            LEFT JOIN inventario iv ON r.id_inventario = iv.id
            WHERE iv.id_super_empresa = :id ORDER BY r.fecha_respuesta DESC, r.fechareg DESC LIMIT 20";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $super_empresa);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarHistorialReportesControl($datos)
    {
        $tabla  = 'reportes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE *,
            r.id as id_reporte_inicio,
            r.estado as estado_reporte,
            r.observacion AS observacion_reporte,
            r.fechareg AS fecha_reporte,
            (SELECT CONCAT(u.nombre, ' ' , u.apellido) FROM usuarios u WHERE u.id_user = r.id_user) AS usuario,
            (SELECT e.nombre FROM estado e WHERE e.id = r.estado) AS estado_nombre,
            (SELECT a.nombre FROM areas a WHERE a.id = r.id_area) AS area
            FROM " . $tabla . " r
            LEFT JOIN inventario iv ON r.id_inventario = iv.id
            LEFT JOIN usuarios u ON u.id_user = iv.id_user
            WHERE CONCAT(u.nombre, ' ', u.apellido, ' ', u.documento, ' ', iv.descripcion, ' ', iv.id) LIKE '%" . $datos['buscar'] . "%'
            " . $datos['usuario'] . "
            " . $datos['area'] . "
            ORDER BY r.fecha_respuesta DESC, r.fechareg DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function programarMantenimientoArticuloModel($datos)
    {
        $tabla  = 'hoja_vida';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET frecuencia_mantenimiento = :fm, frecuencia_copia = :fc, fecha_update = NOW() WHERE id_inventario = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id_inventario']);
            $preparado->bindValue(':fm', $datos['frec_mant']);
            $preparado->bindValue(':fc', $datos['frec_copia']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function articulosComputoAreaModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE iv.*,
            (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
            (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area,
            (SELECT h.frecuencia_mantenimiento FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia,
            (SELECT h.frecuencia_copia FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia_copia,
            IF((SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1) IS NULL, iv.fechareg,
                (SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1)) AS ultimo_mant
            FROM " . $tabla . " iv
            LEFT JOIN usuarios u ON u.id_user = iv.id_user
            LEFT JOIN areas ar ON ar.id = iv.id_area
            WHERE CONCAT(iv.descripcion, ' ', u.nombre, ' ', u.apellido, ' ', ar.nombre) LIKE '%" . $datos['buscar'] . "%'
            " . $datos['area'] . "
            " . $datos['usuario'] . "
            AND iv.id_categoria = 1 ORDER BY iv.id DESC;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarArticulosComputoAreaModel()
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE iv.*,
            (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
            (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area,
            (SELECT h.frecuencia_mantenimiento FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia,
            (SELECT h.frecuencia_copia FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia_copia,
            IF((SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1) IS NULL, iv.fechareg,
                (SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1)) AS ultimo_mant
            FROM " . $tabla . " iv WHERE iv.id_categoria = 1 ORDER BY iv.id DESC LIMIT 30;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarArticulosUsuarioModel($id)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
            iv.*,
            COUNT(iv.id) AS cantidad,
            (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
            (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area,
            (SELECT h.frecuencia_mantenimiento FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia,
            (SELECT e.nombre FROM estado e WHERE e.id = iv.estado) AS nombre_estado,
            IF((SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1) IS NULL, iv.fechareg,
                (SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1)) AS ultimo_mant
            FROM inventario iv
            WHERE iv.id_user = :id AND iv.estado NOT IN(5) AND iv.confirmado NOT IN(2)
            GROUP BY iv.descripcion, iv.estado
            ORDER BY iv.descripcion LIMIT 30;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarArticulosBuscarUsuarioModel($id, $buscar)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
            iv.*,
            COUNT(iv.id) AS cantidad,
            (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
            (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area,
            (SELECT h.frecuencia_mantenimiento FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia,
            (SELECT e.nombre FROM estado e WHERE e.id = iv.estado) AS nombre_estado,
            IF((SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1) IS NULL, iv.fechareg,
                (SELECT r.fechareg FROM reportes r WHERE r.id_inventario = iv.id AND r.estado = 6 ORDER BY id DESC LIMIT 1)) AS ultimo_mant
            FROM inventario iv
            WHERE iv.id_user = :id AND iv.estado NOT IN(5) and concat(iv.descripcion , ' ', iv.marca, ' ', iv.modelo) like '%" . $buscar . "%'
            GROUP BY iv.descripcion, iv.estado
            ORDER BY iv.descripcion;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarInventarioModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET descripcion = :d, marca = :mr, modelo = :md, precio = :pr, id_user = :idu, id_area = :ida, id_categoria = :idc, codigo = :c
            WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $datos['id_inventario']);
            $preparado->bindParam(':d', $datos['descripcion']);
            $preparado->bindParam(':mr', $datos['marca']);
            $preparado->bindParam(':md', $datos['modelo']);
            $preparado->bindParam(':pr', $datos['precio']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':idc', $datos['id_categoria']);
            $preparado->bindParam(':c', $datos['codigo']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarAreaUsuarioControl($id)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE id_area FROM " . $tabla . " WHERE id_user = :id GROUP BY id_area;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function agregarMaterialTempModel($datos)
    {
        $tabla  = 'inventario_temp';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT HIGH_PRIORITY INTO " . $tabla . " (descripcion, estado, id_user, id_area, id_categoria, user_log, id_super_empresa, codigo, observacion)
            VALUES (:d, :e, :idu, :ida, :idc, :ul, :ids, :c, :ob);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':d', $datos['descripcion']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':ida', $datos['id_area']);
            $preparado->bindParam(':idc', $datos['id_categoria']);
            $preparado->bindParam(':ul', $datos['id_log']);
            $preparado->bindParam(':ids', $datos['id_super_empresa']);
            $preparado->bindParam(':c', $datos['codigo']);
            $preparado->bindParam(':ob', $datos['observacion']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarMaterialDidacticoModel($id)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE iv.*,
            (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
            (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area,
            (SELECT e.nombre FROM estado e WHERE e.id = iv.estado) AS nombre_estado,
            COUNT(id) AS cantidad
            FROM " . $tabla . " iv WHERE iv.id_super_empresa = :ids AND iv.id_categoria = 6 GROUP BY iv.descripcion, iv.id_user, iv.estado;;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':ids', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarMaterialControl($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT HIGH_PRIORITY INTO inventario SELECT * FROM inventario_temp WHERE id_super_empresa = :ids AND user_log = :idl AND id_categoria = 6;
            INSERT INTO inventario_log (id_inventario, id_user, id_area, id_log, id_super_empresa, estado)
            SELECT it.id, it.id_user, it.id_area, it.user_log, it.id_super_empresa, it.estado FROM inventario it WHERE it.id_super_empresa = :ids
            AND it.user_log = :idl AND it.id_categoria = 6;
            DELETE FROM inventario_temp WHERE id_super_empresa = :ids AND user_log = :idl AND id_categoria = 6;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':idl', $datos['id_log']);
            $preparado->bindValue(':ids', $datos['id_super_empresa']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function trabajoCasaModel($super_empresa)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE iv.*,
            (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
            (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area,
            (SELECT h.frecuencia_mantenimiento FROM hoja_vida h WHERE h.id_inventario = iv.id) AS frecuencia,
            (SELECT e.nombre FROM estado e WHERE e.id = iv.estado) AS nombre_estado
            FROM " . $tabla . " iv WHERE iv.estado = 8 AND iv.id_super_empresa = :ids";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':ids', $super_empresa);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarCantidadesInventarioModel($id)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
            iv.*,
            (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS usuario,
            (SELECT a.nombre FROM areas a WHERE a.id = iv.id_area) AS area_nom,
            COUNT(iv.id) AS cantidad
            FROM " . $tabla . " iv WHERE iv.id_user = :id AND iv.estado NOT IN (2,6,5) GROUP BY iv.descripcion;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function confirmarInventarioModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE inventario SET confirmado = 1, observacion = '' WHERE id_area = :ida AND confirmado NOT IN(2) AND descripcion LIKE '%" . $datos['descripcion'] . "%';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':ida', $datos['id_area']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function confirmarAgregarInventarioModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE inventario SET confirmado = 1, observacion = '' WHERE id_area = :ida AND descripcion LIKE '%" . $datos['descripcion'] . "%';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':ida', $datos['id_area']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function noConfirmarInventarioModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE inventario SET confirmado = 2, observacion = :ob WHERE id_area = :ida AND id_user = :idu AND descripcion = :d;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':ida', $datos['id_area']);
            $preparado->bindValue(':idu', $datos['id_log']);
            $preparado->bindValue(':d', $datos['descripcion']);
            $preparado->bindValue(':ob', $datos['observacion']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarCantidadesModel($id)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE COUNT(id) AS cantidad,
            (SELECT COUNT(id) FROM inventario WHERE id_user = :id AND confirmado IN(1,2) AND estado NOT IN(2,5,6)) AS cantidad_confirmada
            FROM inventario WHERE id_user = :id AND estado NOT IN(2,5,6);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarFirmaUsuarioModel($id)
    {
        $tabla  = 'firmas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE * FROM " . $tabla . " WHERE id_user = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarInventarioNoConfirmadoModel($datos)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
            iv.*,
            (SELECT e.nombre FROM estado e WHERE e.id IN(iv.estado)) AS estado_nombre,
            (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(iv.id_user)) AS usuario,
            COUNT(iv.id) AS cantidad,
            (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) AS area
            FROM " . $tabla . " iv
            LEFT JOIN usuarios u ON u.id_user = iv.id_user
            WHERE iv.activo = 1 AND iv.confirmado = 2 AND iv.estado NOT IN(2,5,6) AND
            CONCAT(u.nombre, ' ', u.apellido, ' ', u.documento, ' ', iv.descripcion, ' ', iv.codigo, ' ', iv.id) LIKE '%" . $datos['buscar'] . "%'
            " . $datos['area'] . " " . $datos['usuario'] . "
            GROUP BY iv.descripcion, iv.id_area;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarInventarioNoConfirmadoModel()
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
            iv.*,
            (SELECT e.nombre FROM estado e WHERE e.id IN(iv.estado)) AS estado_nombre,
            (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(iv.id_user)) AS usuario,
            COUNT(iv.id) AS cantidad,
            (SELECT a.nombre FROM areas a WHERE a.id IN(iv.id_area)) AS area
            FROM " . $tabla . " iv WHERE iv.activo = 1 AND iv.confirmado = 2 AND iv.estado NOT IN(2,5,6) GROUP BY iv.descripcion, iv.id_area LIMIT 30;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function cantidadesSolucionadasModel($datos)
    {
        $tabla  = 'reportes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE COUNT(id) as solucion FROM " . $tabla . " WHERE id_reporte IN(SELECT id FROM " . $tabla . " WHERE estado = " . $datos['tipo'] . ") AND fechareg >= '" . $datos['fecha_inicio'] . " 00:00:00' AND fechareg <= '" . $datos['fecha_fin'] . " 24:59:59' AND id_inventario IN(SELECT iv.id FROM inventario iv WHERE iv.id_categoria IN(1,5));";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function cantidadesPendienteModel($datos)
    {
        $tabla  = 'reportes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE COUNT(r.id) AS cantidad FROM reportes r WHERE r.estado = " . $datos['tipo'] . " AND r.id_inventario IN(SELECT iv.id FROM inventario iv WHERE iv.id_categoria IN(1,5) AND iv.estado NOT IN(5)) AND r.fechareg >= '" . $datos['fecha_inicio'] . " 00:00:00' AND r.fechareg <= '" . $datos['fecha_fin'] . " 24:59:59' AND r.id NOT IN(SELECT p.id_reporte FROM reportes p WHERE p.estado = 3);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function cantidadesGeneralSolucionadasModel($datos)
    {
        $tabla  = 'reportes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
            COUNT(id) AS solucion
            FROM
            reportes
            WHERE id_reporte IN
            (
                SELECT id
                FROM
                reportes
                WHERE estado = " . $datos['tipo'] . ")
            AND fechareg >= '" . $datos['fecha_inicio'] . " 00:00:00'
            AND fechareg <= '" . $datos['fecha_fin'] . " 24:59:59'
            AND id_inventario IN
            (SELECT
            iv.id
            FROM
            inventario iv
            WHERE iv.id_categoria NOT IN (1, 5) AND estado NOT IN(5)
        );";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function cantidadesGeneralPendientesModel($datos)
    {
        $tabla  = 'reportes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE COUNT(r.id) AS cantidad FROM reportes r WHERE r.estado = " . $datos['tipo'] . " AND r.id_inventario IN(SELECT iv.id FROM inventario iv WHERE iv.id_categoria NOT IN(1,5) AND iv.activo = 1 AND iv.estado NOT IN(5)) AND r.fechareg >= '" . $datos['fecha_inicio'] . " 00:00:00' AND r.fechareg <= '" . $datos['fecha_fin'] . " 24:59:59' AND r.id NOT IN(SELECT p.id_reporte FROM reportes p WHERE p.estado = 3);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function agregarInventarioPendienteModel($id, $codigo)
    {
        $tabla  = 'inventario';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT HIGH_PRIORITY INTO inventario (descripcion, marca, modelo, precio, estado, activo, fecha_compra, observacion, id_user, id_area, id_categoria, user_log, confirmado, id_super_empresa, codigo)
        SELECT descripcion, marca, modelo, precio, estado, activo, fecha_compra, observacion, id_user, id_area, id_categoria, user_log, 2, id_super_empresa, " . $codigo . "
        FROM inventario
        WHERE id = " . $id;
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function copiaSeguridadArticuloModel($datos)
    {
        $tabla  = 'copia_seguridad';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
            id_area,
            id_user,
            id_inventario,
            fecha,
            id_log,
            observacion)
        VALUES
        (
            '" . $datos['id_area'] . "',
            '" . $datos['id_user'] . "',
            '" . $datos['id_inventario'] . "',
            '" . $datos['fecha'] . "',
            '" . $datos['id_log'] . "',
            '" . $datos['observacion'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarMantenimientosModel($datos)
    {
        $tabla  = 'reportes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO reportes (id_inventario, observacion, estado, id_area, id_user, id_log, tipo_reporte, fechareg) VALUES (
                '" . $datos['id_inventario'] . "',
                'Manteniento Preventivo',
                '" . $datos['estado'] . "',
                '" . $datos['id_area'] . "',
                '" . $datos['id_user'] . "',
                '" . $datos['id_log'] . "',
                '" . $datos['tipo_reporte'] . "',
                '" . $datos['fechareg'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                $id   = $cnx->ultimoIngreso($tabla);
                $rslt = array('guardar' => true, 'id' => $id);
                return $rslt;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarSolucionMantenimientoControl($datos)
    {
        $tabla  = 'reportes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO reportes (id_inventario, observacion, estado, id_area, id_user, id_log, id_resp, id_reporte, tipo_reporte, fecha_respuesta, fechareg) VALUES (
                    '" . $datos['id_inventario'] . "',
                    'Mantenimiento Preventivo (Solucion)',
                    '" . $datos['estado'] . "',
                    '" . $datos['id_area'] . "',
                    '" . $datos['id_user'] . "',
                    '" . $datos['id_log'] . "',
                    '" . $datos['id_resp'] . "',
                    '" . $datos['id_reporte'] . "',
                    '" . $datos['tipo_reporte'] . "',
                    '" . $datos['fecha_respuesta'] . "',
                    '" . $datos['fechareg'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function comandoSQL()
    {
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SET SQL_BIG_SELECTS=1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

}
