<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloExtra extends conexion
{

    public static function agregarExtraModel($datos)
    {
        $tabla  = 'extra';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (tipo, actividad, cupo, docente, hora_inicio, hora_fin, id_log, grado, dias) VALUES (
        '" . $datos['tipo'] . "',
        '" . $datos['actividad'] . "',
        '" . $datos['cupo'] . "',
        '" . $datos['docente'] . "',
        '" . $datos['hora_inicio'] . "',
        '" . $datos['hora_fin'] . "',
        '" . $datos['id_log'] . "',
        '" . $datos['grado'] . "',
        '" . $datos['dias'] . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodosExtraModel()
    {
        $tabla  = 'extra';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ex.*,
        (SELECT COUNT(exc.id) FROM  extra_inscripcion exc WHERE exc.id_extra = ex.id) AS cupo_disponible,
        (ex.cupo - (SELECT COUNT(exc.id) FROM  extra_inscripcion exc WHERE exc.id_extra = ex.id)) AS cupo_ocupado
        FROM extra ex WHERE activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function datosExtraCurricularModel($id)
    {
        $tabla  = 'extra';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ex.*,
        (SELECT COUNT(exc.id) FROM  extra_inscripcion exc WHERE exc.id_extra = ex.id) AS cupo_disponible,
        (ex.cupo - (SELECT COUNT(exc.id) FROM  extra_inscripcion exc WHERE exc.id_extra = ex.id)) AS cupo_ocupado
        FROM extra ex WHERE activo = 1 AND id = '" . $id . "';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosRegistrosExtraModel()
    {
        $tabla  = 'extra_inscripcion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        exc.*,
        CONCAT(u.nombre, ' ', u.`apellido`) AS nom_usuario,
        (SELECT c.nombre FROM curso c WHERE c.id = u.id_curso) AS nom_curso,
        (SELECT n.nombre FROM nivel n WHERE n.id = u.id_nivel) AS nom_nivel,
        ex.tipo,
        ex.actividad,
        ex.grado,
        ex.dias
        FROM extra_inscripcion exc
        LEFT JOIN usuarios u ON u.id_user = exc.id_user
        LEFT JOIN extra ex ON ex.id = exc.id_extra
        ORDER BY exc.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarDatosRegistrosExtraModel($datos)
    {
        $tabla  = 'extra_inscripcion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        exc.*,
        CONCAT(u.nombre, ' ', u.`apellido`) AS nom_usuario,
        (SELECT c.nombre FROM curso c WHERE c.id = u.id_curso) AS nom_curso,
        (SELECT n.nombre FROM nivel n WHERE n.id = u.id_nivel) AS nom_nivel,
        ex.tipo,
        ex.actividad,
        ex.grado,
        ex.dias
        FROM extra_inscripcion exc
        LEFT JOIN usuarios u ON u.id_user = exc.id_user
        LEFT JOIN extra ex ON ex.id = exc.id_extra
        WHERE CONCAT(u.documento, ' ', u.nombre, ' ', u.apellido) LIKE '%" . $datos['buscar'] . "%'
        " . $datos['id_extra'] . "
        " . $datos['tipo'] . "
        ORDER BY exc.id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosRegistrosExtraLimiteModel()
    {
        $tabla  = 'extra_inscripcion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        exc.*,
        CONCAT(u.nombre, ' ', u.`apellido`) AS nom_usuario,
        (SELECT c.nombre FROM curso c WHERE c.id = u.id_curso) AS nom_curso,
        (SELECT n.nombre FROM nivel n WHERE n.id = u.id_nivel) AS nom_nivel,
        ex.tipo,
        ex.actividad,
        ex.grado,
        ex.dias
        FROM extra_inscripcion exc
        LEFT JOIN usuarios u ON u.id_user = exc.id_user
        LEFT JOIN extra ex ON ex.id = exc.id_extra
        ORDER BY exc.id DESC LIMIT 20;;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registrarUsuarioExtraModel($datos)
    {
        $tabla  = 'extra_inscripcion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_extra, id_user) VALUES (:ide, :idu);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ide', $datos['id_extra']);
            $preparado->bindParam(':idu', $datos['id_user']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function validarRegistroUsuarioExtraModel($usuario, $tipo)
    {
        $tabla  = 'extra_inscripcion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        COUNT(ex.id) AS repetido
        FROM extra_inscripcion ex
        WHERE ex.`id_extra` IN(SELECT ext.id FROM extra ext WHERE ext.tipo = " . $tipo . ") AND ex.`id_user` = " . $usuario . " GROUP BY ex.id_extra;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function correosPadresEstudianteModel($id)
    {
        $tabla  = 'extra_inscripcion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT correo FROM estudiantes_padres WHERE id_estudiante = " . $id . " AND correo NOT IN('');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

}
