<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'extra' . DS . 'ModeloExtra.php';
require_once MODELO_PATH . 'perfil' . DS . 'ModeloPerfil.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreos.php';

class ControlExtra
{

    private static $instancia;

    public static function singleton_extra()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarTodosExtraControl()
    {
        $mostrar = ModeloExtra::mostrarTodosExtraModel();
        return $mostrar;
    }

    public function informacionExtraControl($id)
    {
        $mostrar = ModeloExtra::datosExtraCurricularModel($id);
        return $mostrar;
    }

    public function mostrarDatosRegistrosExtraControl()
    {
        $mostrar = ModeloExtra::mostrarDatosRegistrosExtraModel();
        return $mostrar;
    }

    public function buscarDatosRegistrosExtraControl($datos)
    {

        $tipo     = (!empty($datos['tipo'])) ? ' AND ex.tipo = ' . $datos['tipo'] : '';
        $id_extra = (!empty($datos['actividad'])) ? ' AND exc.id_extra = ' . $datos['actividad'] : '';

        $datos = array('buscar' => $datos['buscar'], 'tipo' => $tipo, 'id_extra' => $id_extra);

        $mostrar = ModeloExtra::buscarDatosRegistrosExtraModel($datos);
        return $mostrar;
    }

    public function mostrarDatosRegistrosExtraLimiteControl()
    {
        $mostrar = ModeloExtra::mostrarDatosRegistrosExtraLimiteModel();
        return $mostrar;
    }

    public function datosExtraCurricularControl($id)
    {

        $datos    = explode(',', $id);
        $array_id = [];

        foreach ($datos as $key) {
            if ($key != '') {
                $id_extra = $key;
            }
            $array_id[] = ModeloExtra::datosExtraCurricularModel($id_extra);
        }
        return $array_id;
    }

    public function agregarExtraControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'tipo'        => $_POST['tipo'],
                'actividad'   => $_POST['actividad'],
                'cupo'        => $_POST['cupo'],
                'docente'     => $_POST['docente'],
                'hora_inicio' => $_POST['hora_inicio'],
                'hora_fin'    => $_POST['hora_fin'],
                'grado'       => $_POST['grado'],
                'dias'        => $_POST['dias'],
            );

            $guardar = ModeloExtra::agregarExtraModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al crear", {color: "red"});
                </script>
                ';
            }

        }
    }

    public function registrarUsuarioExtraControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_extra']) &&
            !empty($_POST['id_extra'])
        ) {

            $datos = array(
                'id_user'  => $_POST['estudiante'],
                'id_extra' => $_POST['id_extra'],
            );

            $extra = ModeloExtra::datosExtraCurricularModel($_POST['id_extra']);

            $validar_registro_usuario = ModeloExtra::validarRegistroUsuarioExtraModel($_POST['estudiante'], $extra['tipo']);

            if ($validar_registro_usuario['repetido'] > 0) {
                echo '
                <script>
                ohSnap("El estudiante ya se encuentra inscrito a una actividad de este horario!", {color: "red"});
                </script>
                ';
                die();
            }

            if ($extra['cupo'] - $extra['cupo_disponible'] > 0) {

                $guardar        = ModeloExtra::registrarUsuarioExtraModel($datos);
                $datos_usuario  = ModeloPerfil::mostrarDatosPerfilModel($_POST['estudiante']);
                $correos_padres = ModeloExtra::correosPadresEstudianteModel($_POST['estudiante']);

                $correos = [];

                foreach ($correos_padres as $correo_enviar) {
                    $correos[] = $correo_enviar['correo'];
                }

                if ($guardar == true) {
                    echo '
                    <script>
                    ohSnap("Registrado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("extra");
                    }
                    </script>
                    ';

                    $tipo        = ($extra['tipo'] == 1) ? 'Extracurricular' : 'Club';
                    $hora_inicio = date('h:i', strtotime($extra['hora_inicio']));
                    $hora_fin    = date('h:i A', strtotime($extra['hora_fin']));

                    $mensaje = '
                    <div>
                    <p style="font-size: 1.2em;">
                    El estudiante <b>' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '</b> Se ha inscrito a la siguiente actividad:
                    </p>
                    <ul  style="font-size: 1.3em;">
                    <li><b>Tipo:</b> ' . $tipo . '</li>
                    <li><b>Actividad:</b> ' . $extra['actividad'] . ' </li>
                    <li><b>Grado:</b> ' . $extra['grado'] . '</li>
                    <li><b>Docente:</b> ' . $extra['docente'] . '</li>
                    <li><b>Dias:</b> ' . $extra['dias'] . '</li>
                    <li><b>Horario:</b> ' . $hora_inicio . ' - ' . $hora_fin . '</li>
                    </ul>
                    ';

                    $datos_correo = array(
                        'asunto'  => 'Inscripcion a actividad Extracurricular',
                        'correo'  => $correos,
                        'user'    => 'Administrador',
                        'mensaje' => $mensaje,
                        'archivo' => array(''),
                    );

                    $enviar_correo = Correo::enviarCorreoModel($datos_correo);

                } else {
                    echo '
                    <script>
                    ohSnap("Error al crear", {color: "red"});
                    </script>
                    ';
                }
            } else {
                echo '
                <script>
                ohSnap("Cupos Agotados", {color: "red", "duration": "1500"});
                </script>
                ';
            }
        }
    }
}
