<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'usuarios' . DS . 'ModeloUsuarios.php';
require_once CONTROL_PATH . 'hash.php';
require_once CONTROL_PATH . 'numeros.php';

class ControlUsuarios
{

    private static $instancia;

    public static function singleton_usuarios()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarUsuariosControl()
    {
        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::mostrarUsuariosModel();
        return $mostrar;
    }

    public function mostrarEstudiantesControl()
    {
        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::mostrarEstudiantesModel();
        return $mostrar;
    }

    public function mostrarEstudiantesAcudienteControl($id)
    {
        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::mostrarEstudiantesAcudienteModel($id);
        return $mostrar;
    }

    public function mostrarAcudientesEstudiantesControl($id)
    {
        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::mostrarAcudientesEstudiantesModel($id);
        return $mostrar;
    }

    public function mostrarEstudiantesPROMControl()
    {
        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::mostrarEstudiantesPROMModel();
        return $mostrar;
    }

    public function cuposEstudiantesControl($id)
    {
        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::cuposEstudiantesModel($id);
        return $mostrar;
    }

    public function buscarUsuariosNivelControl($datos)
    {

        $nivel  = ($datos['nivel'] == '') ? '' : ' AND u.id_nivel = ' . $datos['nivel'];
        $curso  = ($datos['curso'] == '') ? '' : ' AND u.id_curso = ' . $datos['curso'];
        $perfil = ($datos['perfil'] == '') ? '' : ' AND u.perfil = ' . $datos['perfil'];

        $datos = array('nivel' => $nivel, 'curso' => $curso, 'buscar' => $datos['buscar'], 'perfil' => $perfil);

        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::buscarUsuariosNivelModel($datos);
        return $mostrar;
    }

    public function buscarEstudiantesControl($datos)
    {

        $nivel = ($datos['nivel'] == '') ? '' : ' AND u.id_nivel = ' . $datos['nivel'];
        $curso = ($datos['curso'] == '') ? '' : ' AND u.id_curso = ' . $datos['curso'];

        $datos = array('nivel' => $nivel, 'curso' => $curso, 'buscar' => $datos['buscar']);

        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::buscarEstudiantesModel($datos);
        return $mostrar;
    }

    public function mostrarUsuariosDatosControl($id)
    {
        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::mostrarUsuariosDatosModel($id);
        return $mostrar;
    }

    public function mostrarTodosUsuariosControl()
    {
        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::mostrarTodosUsuariosModel();
        return $mostrar;
    }

    public function mostrarTodosUsuariosInventarioControl()
    {
        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::mostrarTodosUsuariosInventarioModel();
        return $mostrar;
    }

    public function mostrarTodosProveedoresControl()
    {
        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::mostrarTodosProveedoresModel();
        return $mostrar;
    }

    public function buscarUsuarioControl($buscar)
    {
        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::buscarUsuarioModel($buscar);
        return $mostrar;
    }

    public function validarFirmaIdControl($id)
    {
        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::validarFirmaModel($id);
        return $mostrar;
    }

    public function mostrarNivelesUsuarioControl()
    {
        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::mostrarNivelesUsuarioModel();
        return $mostrar;
    }

    public function mostrarCursosUsuarioControl()
    {
        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::mostrarCursosUsuarioModel();
        return $mostrar;
    }

    public function mostrarGruposControl()
    {
        $consulta = ModeloUsuarios::comandoSQL();
        $mostrar  = ModeloUsuarios::mostrarGruposModel();
        return $mostrar;
    }

    public function documentoActualizarControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log_inicio_documento']) &&
            !empty($_POST['id_log_inicio_documento'])
        ) {

            $datos = array(
                'id_log'    => $_POST['id_log_inicio_documento'],
                'documento' => $_POST['documento_inicio_edit'],
            );

            $actualizar = ModeloUsuarios::documentoActualizarModel($datos);

            if ($actualizar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("inicio");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Documento no actualizado", {color: "red"});
                </script>
                ';
            }

        }
    }

    public function guardarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $pass      = $_POST['password'];
            $conf_pass = $_POST['conf_password'];

            if ($conf_pass == $pass) {

                $pass_hash = Hash::hashpass($conf_pass);

                $datos = array(
                    'super_empresa' => $_POST['super_empresa'],
                    'id_log'        => $_POST['id_log'],
                    'documento'     => $_POST['documento'],
                    'nombre'        => $_POST['nombre'],
                    'apellido'      => $_POST['apellido'],
                    'telefono'      => $_POST['telefono'],
                    'correo'        => $_POST['correo'],
                    'usuario'       => $_POST['usuario'],
                    'perfil'        => $_POST['perfil'],
                    'asignatura'    => $_POST['asignatura'],
                    'pass'          => $pass_hash,
                    'curso'         => ($_POST['curso'] == '') ? 0 : $_POST['curso'],
                    'nivel'         => $_POST['nivel'],
                    'grupo'         => $_POST['grupo'],
                );

                $guardar = ModeloUsuarios::guardarUsuarioModel($datos);

                if ($guardar['guardar'] == true) {
                    echo '
                    <script>
                    ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina,1050);

                    function recargarPagina(){
                        window.location.replace("index");
                    }
                    </script>
                    ';
                } else {
                    echo '
                    <script>
                    ohSnap("Error al crear usuario", {color: "red"});
                    </script>
                    ';
                }
            } else {
                echo '
                <script>
                ohSnap("Contraseñas no coinciden", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function editarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {
            $pass      = $_POST['pass_editar'];
            $conf_pass = $_POST['conf_pass_editar'];
            $pass_old  = $_POST['pass_old'];

            $pass_hash = ($conf_pass == $pass) ? Hash::hashpass($conf_pass) : $pass_old;
            $grupo = (empty($_POST['grupo'])) ? 0 : $_POST['grupo'];

            $datos = array(
                'id_user'    => $_POST['id_user'],
                'documento'  => $_POST['documento_edit'],
                'nombre'     => $_POST['nombre_edit'],
                'apellido'   => $_POST['apellido_edit'],
                'telefono'   => $_POST['telefono_edit'],
                'perfil'     => $_POST['perfil_edit'],
                'asignatura' => $_POST['asignatura_edit'],
                'pass'       => $pass_hash,
                'correo'     => $_POST['correo_edit'],
                'curso'      => $_POST['curso_edit'],
                'nivel'      => $_POST['nivel_edit'],
                'nivel'      => $_POST['nivel_edit'],
                'grupo'      => $grupo,
            );

            $guardar = ModeloUsuarios::editarUsuarioModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Editado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al editar usuario", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function guardarFirmaUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nom_archivo = '';

            if (isset($_FILES['firma']['name']) && !empty($_FILES['firma']['name'])) {
                $nom_archivo = guardarArchivo($_FILES['firma']);
            }

            $datos = array(
                'nombre'        => $nom_archivo,
                'id_user'       => $_POST['id_log'],
                'user_log'      => $_POST['id_log'],
                'terminos'      => $_POST['terminos'],
                'firma_digital' => 0,
            );

            $guardar = ModeloUsuarios::guardarFirmaUsuarioModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("inicio");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al subir archivo", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function subirFotoCarnetControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nom_archivo = $_POST['foto_carnet'];

            if (isset($_FILES['foto']['name']) && !empty($_FILES['foto']['name'])) {
                $eliminar = eliminarArchivo($nom_archivo);
                if ($eliminar == true) {
                    $nom_archivo = guardarArchivo($_FILES['foto']);
                }
            }

            $datos = array(
                'id_user'   => $_POST['id_user'],
                'id_log'    => $_POST['id_log'],
                'documento' => $_POST['documento'],
                'nombre'    => $_POST['nombre'],
                'apellido'  => $_POST['apellido'],
                'curso'     => $_POST['curso'],
                'foto'      => $nom_archivo,
            );

            $guardar = ModeloUsuarios::subirFotoCarnetModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Subido correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al subir archivo", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function verificarDocumentoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['documento']) &&
            !empty($_POST['documento'])
        ) {
            $documento = $_POST['documento'];
            $consulta  = ModeloUsuarios::comandoSQL();
            $buscar    = ModeloUsuarios::buscarDocumentoModel($documento);

            if ($buscar['id_user'] != "") {
                $rs = 'ok';
            } else {
                $rs = 'no';
            }

            return $rs;
        }
    }

    public function validarFirmaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $id       = $_POST['id'];
            $consulta = ModeloUsuarios::comandoSQL();
            $buscar   = ModeloUsuarios::validarFirmaModel($id);
            return $buscar;
        }
    }

    public function verificarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['usuario']) &&
            !empty($_POST['usuario'])
        ) {
            $usuario  = $_POST['usuario'];
            $consulta = ModeloUsuarios::comandoSQL();
            $buscar   = ModeloUsuarios::verificarUsuarioModel($usuario);

            if ($buscar['id_user'] != "") {
                $rs = 'ok';
            } else {
                $rs = 'no';
            }

            return $rs;
        }
    }

    public function inactivarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {
            $id_user = $_POST['id_user'];
            $fecha   = date('Y-m-d H:i:s');

            $datos    = array('id_user' => $id_user, 'fecha' => $fecha);
            $consulta = ModeloUsuarios::comandoSQL();
            $buscar   = ModeloUsuarios::inactivarUsuarioModel($datos);

            if ($buscar == true) {
                $rs = 'ok';
            } else {
                $rs = 'no';
            }

            return $rs;
        }
    }

    public function activarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {
            $id_user = $_POST['id_user'];
            $fecha   = date('Y-m-d H:i:s');

            $datos    = array('id_user' => $id_user, 'fecha' => $fecha);
            $consulta = ModeloUsuarios::comandoSQL();
            $buscar   = ModeloUsuarios::activarUsuarioModel($datos);

            if ($buscar == true) {
                $rs = 'ok';
            } else {
                $rs = 'no';
            }

            return $rs;
        }
    }

    public function disminuirCupoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {

            $datos = ModeloUsuarios::datosCupoPromModel($_POST['id']);

            $cupo_disponible = $datos['cupo_disponible'];
            $cupo_ocupado    = $datos['cupo_ocupado'];

            if ($cupo_disponible == 0) {

                $rs = 'limite';

            } else {

                $cupo_disponible = $cupo_disponible - 1;
                $cupo_ocupado    = $cupo_ocupado + 1;

                $datos_cupo = array(
                    'cupo_disponible' => $cupo_disponible,
                    'cupo_ocupado'    => $cupo_ocupado,
                    'id_estudiante'   => $_POST['id'],
                );

                $guardar = ModeloUsuarios::disminuirCupoModel($datos_cupo);
                $rs      = ($guardar == true) ? 'ok' : 'no';
            }

            return $rs;
        }
    }

    public function eliminarFotosCarnetControl()
    {

        $datos_usuario = ModeloUsuarios::mostrarUsuariosFotoCarnetModel();

        foreach ($datos_usuario as $usuario) {

            $foto_carnet = $usuario['foto_carnet'];

            if (!empty($foto_carnet)) {
                $ruta = PUBLIC_PATH_ARCH . 'upload' . DS . $foto_carnet;
                if (is_file($ruta)) {
                    unlink($ruta);
                    $eliminar = true;
                } else {
                    $eliminar = false;
                }
            } else {
                $eliminar = true;
            }
        }

        if ($eliminar == true) {
            $mostrar = ModeloUsuarios::eliminarFotosCarnetModel();
            $rs      = ($mostrar == true) ? 'ok' : 'error';
        }

        return $rs;
    }

}
