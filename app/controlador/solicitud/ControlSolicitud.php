<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'solicitud' . DS . 'ModeloSolicitud.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreos.php';
require_once MODELO_PATH . 'perfil' . DS . 'ModeloPerfil.php';
require_once CONTROL_PATH . 'hash.php';
require_once CONTROL_PATH . 'numeros.php';

class ControlSolicitud
{

    private static $instancia;

    public static function singleton_solicitud()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarSolicitudesControl()
    {
        $mostrar = ModeloSolicitud::mostrarSolicitudesModel();
        return $mostrar;
    }

    public function mostrarSolicitudesUsuarioControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarSolicitudesUsuarioModel($id);
        return $mostrar;
    }

    public function mostrarDatosSolicitudIdControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarDatosSolicitudIdModel($id);
        return $mostrar;
    }

    public function mostrarProdcutosSolicitudControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarProdcutosSolicitudModel($id);
        return $mostrar;
    }

    public function mostrarDatosVerificacionControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarDatosVerificacionModel($id);
        return $mostrar;
    }

    //------------------------INICIAL------------------------------//

    public function mostrarDatosSolicitudInicialIdControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarDatosSolicitudInicialIdModel($id);
        return $mostrar;
    }

    public function mostrarProdcutosSolicitudInicialControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarProdcutosSolicitudInicialModel($id);
        return $mostrar;
    }

    public function mostrarDatosVerificacionInicialControl($id)
    {
        $mostrar = ModeloSolicitud::mostrarDatosVerificacionInicialModel($id);
        return $mostrar;
    }

    public function mostrarCotizacionesControl()
    {
        $mostrar = ModeloSolicitud::mostrarCotizacionesModel();
        return $mostrar;
    }

    public function buscarCotizacionesControl($datos)
    {

        $fecha = (!empty($datos['fecha'])) ? ' AND c.fechareg LIKE "%' . $datos['fecha'] . '%"' : '';

        $datos = array('fecha' => $fecha, 'buscar' => $datos['buscar']);

        $mostrar = ModeloSolicitud::buscarCotizacionesModel($datos);
        return $mostrar;
    }

    public function registrarSolicitudControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $datos_solicitud = array(
                'id_log'          => $_POST['id_log'],
                'id_user'         => $_POST['id_user'],
                'fecha_solicitud' => $_POST['fecha_solicitud'],
                'area'            => $_POST['area'],
                'justificacion'   => $_POST['justificacion'],
            );

            $guardar = ModeloSolicitud::registrarSolicitudModel($datos_solicitud);

            if ($guardar['guardar'] == true) {

                $array_producto = array();
                $array_producto = $_POST['producto'];

                $array_cantidad = array();
                $array_cantidad = $_POST['cantidad'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_producto));
                $it->attachIterator(new ArrayIterator($array_cantidad));

                foreach ($it as $a) {
                    $datos_producto = array(
                        'id_log'       => $_POST['id_log'],
                        'id_solicitud' => $guardar['id'],
                        'producto'     => $a[0],
                        'cantidad'     => $a[1],
                    );

                    $guardar_producto = ModeloSolicitud::registrarProdcuctosModel($datos_producto);
                }

                if ($guardar_producto['guardar'] == true) {
                    echo '
                    <script>
                    ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                    setTimeout(recargarPagina, 1050);

                    function recargarPagina()
                    {
                        window.location.replace("index");
                    }
                    </script>
                    ';
                } else {
                    echo '
                    <script>
                    ohSnap("Error de solicitud!", {color:"red", "duration":"1000"});
                    </script>
                    ';
                }
            }
        }
    }

    public function confirmarSolicitudControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $fecha_aplazado = ($_POST['fecha_aplazado'] == '') ? '0000-00-00' : $_POST['fecha_aplazado'];

            $datos_solicitud = array(
                'id_solicitud'     => $_POST['id_solicitud'],
                'estado'           => $_POST['estado'],
                'fecha_solicitado' => $_POST['fecha_solicitado'],
                'fecha_aplazado'   => $fecha_aplazado,
                'observacion'      => $_POST['observacion'],
                'id_log'           => $_POST['id_log'],
                'iva'              => '',
                'id_proveedor'     => $_POST['proveedor'],
            );

            $confirmar = ModeloSolicitud::actualizarEstadoModel($datos_solicitud);

            if ($confirmar == true) {

                $array_producto = array();
                $array_producto = $_POST['id_producto'];

                $array_precio = array();
                $array_precio = $_POST['valor'];

                $array_nom = array();
                $array_nom = $_POST['nom_producto'];

                $array_cantidad = array();
                $array_cantidad = $_POST['cantidad'];

                $array_iva = array();
                $array_iva = $_POST['iva'];

                $it = new MultipleIterator();
                $it->attachIterator(new ArrayIterator($array_producto));
                $it->attachIterator(new ArrayIterator($array_precio));
                $it->attachIterator(new ArrayIterator($array_nom));
                $it->attachIterator(new ArrayIterator($array_cantidad));
                $it->attachIterator(new ArrayIterator($array_iva));

                foreach ($it as $a) {
                    $id_producto  = $a[0];
                    $precio       = $a[1];
                    $nom_producto = $a[2];
                    $cantidad     = $a[3];

                    $datos_precio = array(
                        'id_producto'  => $id_producto,
                        'nom_producto' => $nom_producto,
                        'cantidad'     => $cantidad,
                        'precio'       => str_replace(',', '', $precio),
                        'iva'          => $a[4],
                    );

                    $actualizar_precio = ModeloSolicitud::preciosProductoControl($datos_precio);
                }

                if ($actualizar_precio == true) {

                    echo ' <script>
                    window . open("' . BASE_URL . 'imprimir/solicitud?solicitud=' . base64_encode($_POST['id_solicitud']) . '")
                    </script> ';

                    echo '
                    <script>
                    ohSnap("Guardado correctamente!", {color:"green", "duration":"1000"});
                    setTimeout(recargarPagina, 1050);

                    function recargarPagina()
                    {
                        window . location . replace("listado");
                    }
                    </script>
                    ';
                }
            }
        }
    }

    public function verificarSolicitudControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos_verificacion = array(
                'id_solicitud'        => $_POST['id_solicitud'],
                'id_log'              => $_POST['id_log'],
                'cantidad'            => $_POST['cumple_cant'],
                'calidad'             => $_POST['cumple_calidad'],
                'precios'             => $_POST['cumple_precio'],
                'plazos'              => $_POST['cumple_plazo'],
                'observacion_cant'    => $_POST['observacion_cant'],
                'observacion_calidad' => $_POST['observacion_calidad'],
                'observacion_precio'  => $_POST['observacion_precio'],
                'observacion_plazo'   => $_POST['observacion_plazo'],
                'fecha_verificacion'  => $_POST['fecha_verificacion'],
            );

            $verificacion = ModeloSolicitud::verificarSolicitudModel($datos_verificacion);

            if ($verificacion == true) {
                echo ' <script>
                window . open("' . BASE_URL . 'imprimir/solicitud?solicitud=' . base64_encode($_POST['id_solicitud']) . '")
                </script> ';

                echo '
                <script>
                ohSnap("Guardado correctamente!", {color:"green", "duration":"1000"});
                setTimeout(recargarPagina, 1050);

                function recargarPagina()
                {
                    window . location . replace("listado");
                }
                </script>
                ';
            }
        }
    }

    public function removerProductoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $eliminar = ModeloSolicitud::removerProductoModel($_POST['id']);
            return $eliminar;
        }
    }

    public function agregarProductoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {

            $datos = array(
                'id_solicitud' => $_POST['id'],
                'id_log'       => $_POST['id_log'],
                'producto'     => '',
                'cantidad'     => '',
            );

            $guardar = ModeloSolicitud::registrarProdcuctosModel($datos);
            return $guardar;
        }
    }

    public function anularSolicitudControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_log'       => $_POST['id_log'],
                'id_solicitud' => $_POST['id_solicitud'],
                'estado'       => 0,
                'motivo'       => $_POST['motivo'],
            );

            $guardar = ModeloSolicitud::anularSolicitudModel($datos);

            if ($guardar == true) {

                echo '
                <script>
                ohSnap("Anulada correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina, 1050);

                function recargarPagina()
                {
                    window.location.replace("listado");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al anular!", {color:"red", "duration":"1000"});
                </script>
                ';
            }
        }
    }

    public function subirCotizacionControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nombre_archivo = '';

            if (isset($_FILES['cotizacion']['name']) && !empty($_FILES['cotizacion']['name'])) {
                $nom_arch   = $_FILES['cotizacion']['name'];
                $ext_arch   = pathinfo($nom_arch, PATHINFO_EXTENSION);
                $fecha_arch = date('YmdHis');

                $nombre_archivo = strtolower(md5($_POST['id_log'] . '_' . $_POST['concepto'] . $fecha_arch)) . '.' . $ext_arch;

                $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
                $ruta_img     = $carp_destino . $nombre_archivo;

                if ($ext_arch == 'png' || $ext_arch == 'jpeg') {
                    $compressed = compressImage($_FILES['cotizacion']['tmp_name'], $ruta_img, 70);
                } else {
                    if (is_uploaded_file($_FILES['cotizacion']['tmp_name'])) {
                        move_uploaded_file($_FILES['cotizacion']['tmp_name'], $ruta_img);
                    }
                }
            }

            $datos = array(
                'id_log'      => $_POST['id_log'],
                'concepto'    => $_POST['concepto'],
                'cotizacion'  => $nombre_archivo,
                'observacion' => $_POST['observacion'],
                'fecha'       => $_POST['fecha'],
            );

            $guardar = ModeloSolicitud::subirCotizacionLModel($datos);

            if ($guardar['guardar'] == true) {

                $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($_POST['id_log']);
                $datos_cotizacion = ModeloSolicitud::mostrarDatosCotizacionIdModel($guardar['id']);

                $mensaje = '
                <div>
                <p style="font-size: 1.2em;">
                El usuario <b>' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '</b> ha subido una cotizacion:
                </p>
                <ul  style="font-size: 1.3em;">
                <li><b>Concepto:</b> ' . $datos_cotizacion['concepto'] . '</li>
                <li><b>Fecha:</b> ' . $datos_cotizacion['fecha'] . '</li>
                <li><b>Observacion:</b> ' . $datos_cotizacion['observacion'] . '</li>
                <li><b>Estado de la cotizacion:</b> Aprobada</li>
                </ul>
                </div>
                ';

                $datos_correo = array(
                    'asunto'  => 'Cotizacion de sistemas No. ' . $guardar['id'],
                    'correo'  => array('carolina.otalora@royalschool.edu.co', 'cronograma.sistemas@royalschool.edu.co', 'luz.noriega@royalschool.edu.co'),
                    'user'    => 'Administrador',
                    'mensaje' => $mensaje,
                    'archivo' => array($datos_cotizacion['cotizacion']),
                );

                $enviar_correo = Correo::enviarCorreoModel($datos_correo);

                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina, 1050);

                function recargarPagina()
                {
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al guardar!", {color:"red", "duration":"1000"});
                </script>
                ';
            }
        }
    }

    public function subirFacturaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nombre_archivo = '';

            if (isset($_FILES['factura']['name']) && !empty($_FILES['factura']['name'])) {

                $nom_arch   = $_FILES['factura']['name'];
                $ext_arch   = pathinfo($nom_arch, PATHINFO_EXTENSION);
                $fecha_arch = date('YmdHis');

                $nombre_archivo = strtolower(md5($_POST['id_log'] . '_' . $_POST['id_cotizacion'] . $fecha_arch)) . '.' . $ext_arch;

                $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
                $ruta_img     = $carp_destino . $nombre_archivo;

                if ($ext_arch == 'png' || $ext_arch == 'jpeg') {
                    $compressed = compressImage($_FILES['factura']['tmp_name'], $ruta_img, 70);
                } else {
                    if (is_uploaded_file($_FILES['factura']['tmp_name'])) {
                        move_uploaded_file($_FILES['factura']['tmp_name'], $ruta_img);
                    }
                }
            }

            $datos = array(
                'id_log'        => $_POST['id_log'],
                'id_cotizacion' => $_POST['id_cotizacion'],
                'factura'       => $nombre_archivo,
                'observacion'   => $_POST['observacion'],
            );

            $guardar = ModeloSolicitud::subirFacturaModel($datos);

            if ($guardar == true) {

                $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($_POST['id_log']);
                $datos_cotizacion = ModeloSolicitud::mostrarDatosCotizacionIdModel($_POST['id_cotizacion']);

                $mensaje = '
                <div>
                <p style="font-size: 1.2em;">
                El usuario <b>' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '</b> ha subido una orden de compra relacionanda a la cotizacion <b>No. ' . $_POST['id_cotizacion'] . '</b>:
                </p>
                <ul  style="font-size: 1.3em;">
                <li><b>Concepto:</b> ' . $datos_cotizacion['concepto'] . '</li>
                <li><b>Fecha:</b> ' . $datos_cotizacion['fecha'] . '</li>
                <li><b>Observacion:</b> ' . $datos_cotizacion['observacion'] . '</li>
                <li><b>Estado de la cotizacion:</b> Aprobada</li>
                </ul>
                </div>
                ';

                $datos_correo = array(
                    'asunto'  => 'Factura - Cotizacion de sistemas No. ' . $guardar['id'],
                    'correo'  => array('carolina.otalora@royalschool.edu.co', 'cronograma.sistemas@royalschool.edu.co', 'luz.noriega@royalschool.edu.co'),
                    'user'    => 'Administrador',
                    'mensaje' => $mensaje,
                    'archivo' => array($datos_cotizacion['factura']),
                );

                $enviar_correo = Correo::enviarCorreoModel($datos_correo);

                echo '
                <script>
                ohSnap("Subido correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina, 1050);

                function recargarPagina()
                {
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al guardar!", {color:"red", "duration":"1000"});
                </script>
                ';
            }
        }
    }

    public function subirOrdenControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $nombre_archivo = '';

            if (isset($_FILES['orden']['name']) && !empty($_FILES['orden']['name'])) {
                $nom_arch   = $_FILES['orden']['name'];
                $ext_arch   = pathinfo($nom_arch, PATHINFO_EXTENSION);
                $fecha_arch = date('YmdHis');

                $nombre_archivo = strtolower(md5($_POST['id_log'] . '_' . $_POST['id_cotizacion_compra'] . $fecha_arch)) . '.' . $ext_arch;

                $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
                $ruta_img     = $carp_destino . $nombre_archivo;

                if ($ext_arch == 'png' || $ext_arch == 'jpeg') {
                    $compressed = compressImage($_FILES['orden']['tmp_name'], $ruta_img, 70);
                } else {
                    if (is_uploaded_file($_FILES['orden']['tmp_name'])) {
                        move_uploaded_file($_FILES['orden']['tmp_name'], $ruta_img);
                    }
                }
            }

            $datos = array(
                'id_log'        => $_POST['id_log'],
                'id_cotizacion' => $_POST['id_cotizacion_compra'],
                'orden'         => $nombre_archivo,
                'observacion'   => $_POST['observacion'],
            );

            $guardar = ModeloSolicitud::subirOrdenModel($datos);

            if ($guardar == true) {

                $datos_usuario    = ModeloPerfil::mostrarDatosPerfilModel($_POST['id_log']);
                $datos_cotizacion = ModeloSolicitud::mostrarDatosCotizacionIdModel($_POST['id_cotizacion_compra']);

                $mensaje = '
                <div>
                <p style="font-size: 1.2em;">
                El usuario <b>' . $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'] . '</b> ha subido una orden de compra relacionanda a la cotizacion <b>No. ' . $_POST['id_cotizacion_compra'] . '</b>:
                </p>
                <ul  style="font-size: 1.3em;">
                <li><b>Concepto:</b> ' . $datos_cotizacion['concepto'] . '</li>
                <li><b>Fecha:</b> ' . $datos_cotizacion['fecha'] . '</li>
                <li><b>Observacion:</b> ' . $datos_cotizacion['observacion'] . '</li>
                <li><b>Estado de la cotizacion:</b> Aprobada</li>
                </ul>
                </div>
                ';

                $datos_correo = array(
                    'asunto'  => 'Orden de compra - Cotizacion de sistemas No. ' . $guardar['id'],
                    'correo'  => array('carolina.otalora@royalschool.edu.co', 'cronograma.sistemas@royalschool.edu.co', 'luz.noriega@royalschool.edu.co'),
                    'user'    => 'Administrador',
                    'mensaje' => $mensaje,
                    'archivo' => array($datos_cotizacion['orden_compra']),
                );

                $enviar_correo = Correo::enviarCorreoModel($datos_correo);

                echo '
                <script>
                ohSnap("Subido correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina, 1050);

                function recargarPagina()
                {
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error al guardar!", {color:"red", "duration":"1000"});
                </script>
                ';
            }
        }
    }

}
